CREATE OR REPLACE FUNCTION calccomissaovend_pl(integer, integer)
  RETURNS numeric AS
$BODY$
DECLARE
   pI_NrPedido   Alias For $1;
   pI_cdVendedor Alias For $2;
   vi_cdcomissao         int4;
   vf_vendedor           vendedor.f_vendedor%type;

   cItemPedVenda         RefCursor;
   rItemPedVenda         Record;
   vN_VrLiqItem          func_Lanc.N_VlrTitComissao%Type;
   vComisItem            Float;
   vTotalcomissao        func_Lanc.N_VlrTitComissao%Type;
   vF_TpPgto             PedidoVenda.F_TpPgto%Type;
   vN_VlrEntrada         PedidoVenda.N_VlrEntrada%Type;
   vN_VlrFaturamento     PedidoVenda.N_VlrFaturamento%Type;
   vPercEntrada          ItemComissao.N_Percdesc1%Type; 
   vN_Percdesc1          ItemComissao.N_Percdesc1%Type;
   vN_Percdesc2          ItemComissao.N_Percdesc2%Type;
   vN_Percdesc3          ItemComissao.N_Percdesc3%Type;
   vN_Percdesc4          ItemComissao.N_Percdesc4%Type;
   vN_Percdesc5          ItemComissao.N_Percdesc4%Type;
   vN_Percdesc6          ItemComissao.N_Percdesc4%Type;
   vN_Perccomis1         ItemComissao.N_Perccomis1%Type;
   vN_Perccomis2         ItemComissao.N_Perccomis2%Type;
   vN_Perccomis3         ItemComissao.N_Perccomis3%Type;
   vN_Perccomis4         ItemComissao.N_Perccomis4%Type;
   vN_Perccomis5         ItemComissao.N_Perccomis4%Type;
   vN_Perccomis6         ItemComissao.N_Perccomis4%Type;
   vN_PercDescPedido     PedidoVenda.N_PercDescPedido%Type;
   vN_VrItemComis        Func_Lanc.N_VlrTitComissao%Type;
   vi_media_atraso       PedidoVenda.i_media_atraso%Type;   
   vI_CdItemComissao     ItemComissao.I_CdItemComissao%Type;   
Begin

   Select F_TpPgto, N_VlrEntrada, N_VlrFaturamento, N_PercDescPedido, i_media_atraso
     Into vF_TpPgto, vN_VlrEntrada, vN_VlrFaturamento, vN_PercDescPedido,  vi_media_atraso
     From PedidoVenda
    Where I_NrPedido = pI_NrPedido;
    

        Select Comissao.I_CdComissao,
               Vendedor.f_vendedor
               into vi_cdcomissao,vf_vendedor
          From PedVenda_Vendedor, Vendedor, Comissao
         Where PedVenda_Vendedor.I_CdVendedor = Vendedor.I_CdVendedor
           And 2 = Comissao.I_CdComissao
           And I_NrPedido = pI_NrPedido
           And Vendedor.i_cdvendedor = pI_cdVendedor;

-- raise exception 'Comi - %  % %',vi_cdcomissao, pI_NrPedido,pI_cdVendedor;

     vN_VrItemComis := 0;
     vTotalcomissao := 0;
     
     Open cItemPedVenda For
           Select Produto.I_CdProduto,
                  Produto.I_CdGrupo,
                  Produto.I_CdLinha,
                  ItemPedVenda.N_PrcUnitario,
                  ItemPedVenda.n_prcvenda,
                  ItemPedVenda.n_qtdeseparada,
                  ItemPedVenda.N_PercDesconto,
                  PedidoVenda.I_Cdlista_Preco,
                  PedidoVenda.i_cdlista_precoprom
             From ItemPedVenda, Produto, PedidoVenda
            Where ItemPedVenda.I_CdProduto = Produto.I_CdProduto
              And ItemPedVenda.i_nrpedido = PedidoVenda.i_nrpedido
              And ItemPedVenda.I_NrPedido  = pI_NrPedido
              And ItemPedVenda.F_RetiraComissao = 'N';

      Fetch cItemPedVenda Into rItemPedVenda;
      While Found loop
	 vI_CdItemComissao := 0;
	 
         /*Comissao por Produto com Lista Promocional*/
	 Select I_CdItemComissao
           into vI_CdItemComissao
           from ItemComissao 
	  where I_CdComissao = vI_CdComissao
            and I_CdProduto = rItemPedVenda.I_CdProduto 
             And I_CdLista_Preco = rItemPedVenda.i_cdlista_precoprom;

         /*Comissao por Produto com Lista Principal */
         If Coalesce(vI_CdItemComissao,0) = 0 then
	    Select I_CdItemComissao 
              into vI_CdItemComissao
              from ItemComissao 
	     where I_CdComissao = vI_CdComissao
               and I_CdProduto = rItemPedVenda.I_CdProduto 
               And I_CdLista_Preco = rItemPedVenda.I_CdLista_Preco
             Order By I_CdLista_Preco;
         End If;    

	/*Comissao por Produto Sem Lista*/
         If Coalesce(vI_CdItemComissao,0) = 0 then
	    Select I_CdItemComissao 
              into vI_CdItemComissao
              from ItemComissao 
	     where I_CdComissao = vI_CdComissao
               and I_CdProduto = rItemPedVenda.I_CdProduto 
               And (I_CdLista_Preco is Null)
            Order By I_CdLista_Preco;
         End If;    

         /*Comissao por Grupo Com Lista Promocional*/   
         If Coalesce(vI_CdItemComissao,0) = 0 Then
            Select I_CdItemComissao 
              into vI_CdItemComissao
              from ItemComissao 
	     where I_CdComissao = vI_CdComissao
               and I_CdGrupo = rItemPedVenda.I_CdGrupo         
               And I_CdLista_Preco = rItemPedVenda.i_cdlista_precoprom;
         End If;  
         
         /*Comissao por Grupo Com Lista Principal*/   
         If Coalesce(vI_CdItemComissao,0) = 0 Then
            Select I_CdItemComissao 
              into vI_CdItemComissao
              from ItemComissao 
	     where I_CdComissao = vI_CdComissao
               and I_CdGrupo = rItemPedVenda.I_CdGrupo         
               And I_CdLista_Preco = rItemPedVenda.I_CdLista_Preco
             Order By I_CdLista_Preco;
         End If;  

         /*Comissao por Grupo Sem Lista*/   
         If Coalesce(vI_CdItemComissao,0) = 0 Then
            Select I_CdItemComissao 
              into vI_CdItemComissao
              from ItemComissao 
	     where I_CdComissao = vI_CdComissao
               and I_CdGrupo = rItemPedVenda.I_CdGrupo         
               And I_CdLista_Preco is Null
	     Order By I_CdLista_Preco;
         End If;  

         /*Comissao por Linha Com Lista Promocional*/   
         If Coalesce(vI_CdItemComissao,0) = 0 Then
            Select I_CdItemComissao 
              into vI_CdItemComissao
              from ItemComissao 
	     where I_CdComissao = vI_CdComissao
               and I_CdLinha   = rItemPedVenda.I_CdLinha
               And I_CdLista_Preco = rItemPedVenda.i_cdlista_precoprom;
         End If;  
         
         /*Comissao por Linha Com Lista Principal*/   
         If Coalesce(vI_CdItemComissao,0) = 0 Then
            Select I_CdItemComissao 
              into vI_CdItemComissao
              from ItemComissao 
	     where I_CdComissao = vI_CdComissao
               and I_CdLinha   = rItemPedVenda.I_CdLinha
               And I_CdLista_Preco = rItemPedVenda.I_CdLista_Preco                
	     Order By I_CdLista_Preco;
         End If;  

	/*Comissao por Linha Sem Lista*/   
         If Coalesce(vI_CdItemComissao,0) = 0 Then
            Select I_CdItemComissao 
              into vI_CdItemComissao
              from ItemComissao 
	     where I_CdComissao = vI_CdComissao
               and I_CdLinha   = rItemPedVenda.I_CdLinha
               And I_CdLista_Preco is Null
	     Order By I_CdLista_Preco;
         End If;  
 
        /*Comissao Geral Com Lista Promocional*/   
         If Coalesce(vI_CdItemComissao,0) = 0 Then
            Select i.I_CdItemComissao
              into vI_CdItemComissao
              from ItemComissao i
              join Lista_preco p on p.i_cdlista_preco = i.i_cdlista_preco
              join preco pp on p.i_cdlista_preco = pp.i_cdlista_preco
	     where i.I_CdComissao = vI_CdComissao
               and i.F_Tipo = 'G'
               and pp.I_CdProduto = rItemPedVenda.i_cdproduto
               And p.I_CdLista_Preco = rItemPedVenda.i_cdlista_precoprom;
         End If;  

         /*Comissao Geral Com Lista Principal */   
         If Coalesce(vI_CdItemComissao,0) = 0 Then
            Select i.I_CdItemComissao
              into vI_CdItemComissao
              from ItemComissao i
              join Lista_preco p on p.i_cdlista_preco = i.i_cdlista_preco
              join preco pp on p.i_cdlista_preco = pp.i_cdlista_preco
	     where i.I_CdComissao = vI_CdComissao
               and i.F_Tipo = 'G'
               and pp.I_CdProduto = rItemPedVenda.i_cdproduto
               And i.I_CdLista_Preco = rItemPedVenda.I_CdLista_Preco
	     Order By p.I_CdLista_Preco;
         End If;  

	/*Comissao Geral Sem Lista*/   
         If Coalesce(vI_CdItemComissao,0) = 0 Then
            Select I_CdItemComissao 
              into vI_CdItemComissao
              from ItemComissao 
	     where I_CdComissao = vI_CdComissao
               and F_Tipo = 'G'
               And I_CdLista_Preco is Null
	     Order By I_CdLista_Preco;
         End If;  

         Select 
                -- N_PercComisAvista,
                N_Percdesc1,
                N_Percdesc2,
                N_Percdesc3,
                N_Percdesc4,
                N_Percdesc5,
                N_Percdesc6,
                N_Perccomis1,
                N_Perccomis2,
                N_Perccomis3,
                N_Perccomis4,
                N_Perccomis5,
                N_Perccomis6
           Into 
               --vN_PercComisAvista,
                vN_Percdesc1,
                vN_Percdesc2,
                vN_Percdesc3,
                vN_Percdesc4,
                vN_Percdesc5,
                vN_Percdesc6,
                vN_Perccomis1,
                vN_Perccomis2,
                vN_Perccomis3,
                vN_Perccomis4,
                vN_Perccomis5,
                vN_Perccomis6
           From ItemComissao
           Where I_CdItemComissao = vI_CdItemComissao;



        vN_vrLiqItem := (rItemPedVenda.n_qtdeseparada * rItemPedVenda.n_prcvenda) ;
        vN_vrLiqItem := round(vN_vrLiqItem,2);
        
        vComisItem   := 0;
        

	 /*Comissao por margem de lucro*/
         if (trim(RetParametro('USAMARGEMLUCRO')) = 'S') and (vf_vendedor  = 'R') Then
            vN_vrLiqItem := (rItemPedVenda.n_qtdeseparada * (rItemPedVenda.n_prcvenda - rItemPedVenda.N_PrcUnitario ) ) ;
           vN_vrLiqItem := round(vN_vrLiqItem,2);
         End if;
		          
             
           If rItemPedVenda.N_PercDesconto <= vN_PercDesc1 and vComisItem = 0 Then
              vComisItem = vN_VrLiqItem * (vN_PercComis1 / 100.00);
           End If;

           If rItemPedVenda.N_PercDesconto <= vN_PercDesc2 and vComisItem = 0 Then
              vComisItem = vN_VrLiqItem * (vN_PercComis2 / 100.00);
           End If;

           If rItemPedVenda.N_PercDesconto <= vN_PercDesc3 and vComisItem = 0 Then
               vComisItem = vN_VrLiqItem * (vN_PercComis3 / 100.00);
           End If;

           If rItemPedVenda.N_PercDesconto <= vN_PercDesc4 and vComisItem = 0 Then
              vComisItem = vN_VrLiqItem * (vN_PercComis4 / 100.00);
           End If;

           If rItemPedVenda.N_PercDesconto <= vN_PercDesc5 and vComisItem = 0 Then
              vComisItem = vN_VrLiqItem * (vN_PercComis5 / 100.00);
           End If;

           If rItemPedVenda.N_PercDesconto <= vN_PercDesc6 and vComisItem = 0 Then
              vComisItem = vN_VrLiqItem * (vN_PercComis6 / 100.00);
           End If;
       --  End If;
 
         vTotalcomissao := vTotalComissao + vComisItem;
         Fetch cItemPedVenda Into rItemPedVenda;
         
      End Loop;

      Close cItemPedVenda;
      vTotalcomissao :=   round((vTotalcomissao * (1 - (vN_PercDescPedido/100.00))) ,2);
      
     if cast(RetParametro('MediaAltaDivComissao') as integer) > 0 then
           if (vi_media_atraso > cast(RetParametro('MediaAltaDivComissao') as integer)) and (vf_tppgto = 'P') Then
              vTotalcomissao := vTotalcomissao/2;
           end if;    
         end if;


   Return vTotalcomissao;

END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE;
CREATE OR REPLACE FUNCTION CriaTituloRecebimento(
				char,
				PedidoVenda.I_CdContaFin%Type,
				Cliente.I_CdParceiro%Type,
				TimeStamp,
				numeric,
				Titulo.C_Documento%Type,
				Titulo.I_CdVendedor%Type,
                                Titulo.N_VrLiquido%Type, 
                                Titulo.i_cdempresa%Type
                                ) RETURNS Titulo.i_cdTitulo%Type AS
$BODY$
/* Procedure       : Gera os boletos a partir de um Pedido de Venda. */
/* Desenvolvido por: Ronair De Moura Fran�a  em 17/11/2005 */
/* Alterado por    :                         em __/__/____ */
/* Usada em:	GeraBoleto(),			*/
DECLARE
   pF_Tipo         Alias For $1;
   pI_CdContaFin   Alias For $2;
   pI_CdParceiro   Alias For $3;
   pD_Vencimento   Alias For $4;
   pN_VrTitulo     Alias For $5;
   pC_Documento    Alias For $6;
   pI_CdVendedor   Alias For $7;
   pN_VrLiquido    Alias For $8;
   pi_cdempresa    Alias For $9;
   vI_CdTipoTitulo TipoTitulo.I_CdTipoTitulo%Type;
   vi_cdTitulo     Titulo.i_cdTitulo%Type;
   vI_CdContaFin   Titulo.I_CdContaFin%Type; 
   vN_VrTitulo     numeric;
   vN_VrLiquido    numeric;
   valor numeric;
   b boolean;
Begin
  
--  b :=  insparametro('I_CdUsuarioAtual','40007')  ;

  vN_VrTitulo  := round( pN_VrTitulo * 100)/100;  
  vN_VrLiquido := round( pN_VrLiquido * 100)/100;
  vI_CdContaFin := pI_CdContaFin;

	
        If pF_Tipo = 'B' Then
            vI_CdTipoTitulo := cast(RetParametro('I_CdTpTitVendaBnc') as integer);
        Else
            vI_CdTipoTitulo := cast(RetParametro('I_CdTpTitVendaCart') as integer);
        End If;
--raise exception 'valor % ', ;
      
     Insert Into 
         Titulo
         (I_CdContaFin,
         I_CdParceiro,
         I_CdTipoTitulo,
         I_CdFuncCadastro,
         D_Emissao,
         D_Vencimento,
         D_Lancamento,
         D_Mov,
         N_VrTitulo,
         F_TpMov,
         N_Moradiaria,
         C_Documento,
         I_CdVendedor,
         N_VrLiquido, 
         I_CDEMPRESA)
      Values
        (vI_CdContaFin,
         pI_CdParceiro,
         vI_CdTipoTitulo,
         cast(RetParametro('I_CdFuncionarioAtual') as integer),
         Now(),
         pD_Vencimento,
         Now(),
         Now(),
         vN_VrTitulo,
         'C',
         Round((Cast(RetParametro('PercMoraDiaria') as Numeric) * pN_VrTitulo) / 3000.00,2),
         pC_Documento,
         pI_CdVendedor,
         vN_VrLiquido,
         pI_cdempresa);

         vi_cdTitulo := cast(RetParametro('I_CdTitulo') as integer);

       Insert 
       into ContaGer_Titulo 
         (i_cdtitulo,
          i_cdplanoconta,
          n_valor,
          f_tpmov)
      values
	 (vi_cdTitulo,
         cast(RetParametro('PlanoContaVenda') as integer),
         vN_VrTitulo,
         'C');
  
      update Titulo
         set f_validado = 'S'
       where i_cdTitulo = vi_cdTitulo;   

    Return vi_cdtitulo;
    
END;$BODY$
LANGUAGE 'plpgsql' VOLATILE;

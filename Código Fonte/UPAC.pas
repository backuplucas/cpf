unit UPAC;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DBXpress, FMTBcd, Provider, SqlExpr, DB, DBClient, StdCtrls,
  Buttons, Mask, DBCtrls, Menus, Grids, DBGrids, ExtCtrls, ComCtrls,
  RpDefine, RpCon, RpConDS, RpRenderCanvas, RpRenderPrinter, RpRave,
  RpRenderText, RpRenderRTF, RpRenderHTML, RpRender, RpRenderPDF, RpBase,
  RpSystem;

type
  TForm1 = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    BitBtn1: TBitBtn;
    eOrcamento: TEdit;
    Corcamento: TClientDataSet;
    dOrcamento: TDataSource;
    Sorcamento: TSQLDataSet;
    Porcamento: TDataSetProvider;
    SQLConnection: TSQLConnection;
    scriatitulo: TSQLDataSet;
    dcriatitulo: TDataSetProvider;
    criatitulo: TClientDataSet;
    PopupMenu1: TPopupMenu;
    Limpa1: TMenuItem;
    Procura1: TMenuItem;
    Gera1: TMenuItem;
    Sair1: TMenuItem;
    Panel2: TPanel;
    sTitulo: TSQLDataSet;
    pTitulo: TDataSetProvider;
    cTitulo: TClientDataSet;
    dtitulo: TDataSource;
    eDesconto: TEdit;
    lperc: TLabel;
    ldesconto: TLabel;
    eValor: TEdit;
    Label8: TLabel;
    cvendedor: TClientDataSet;
    dvendedor: TDataSetProvider;
    svendedor: TSQLDataSet;
    scriaparametro: TSQLDataSet;
    sLote: TSQLDataSet;
    dLote: TDataSetProvider;
    cLote: TClientDataSet;
    sLoteRec: TSQLDataSet;
    dLoteRec: TDataSetProvider;
    cLoteRec: TClientDataSet;
    sfunclanc: TSQLDataSet;
    dfunclanc: TDataSetProvider;
    cfunclanc: TClientDataSet;
    sitem: TSQLDataSet;
    ditem: TDataSetProvider;
    citem: TClientDataSet;
    supdateitem: TSQLDataSet;
    dupdateitem: TDataSetProvider;
    cupdateitem: TClientDataSet;
    scalpedido: TSQLDataSet;
    dcalcpedido: TDataSetProvider;
    ccalpedido: TClientDataSet;
    Configuracao: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    DBGrid1: TDBGrid;
    conf: TMemo;
    bSalvaConfg: TBitBtn;
    slocalest: TSQLDataSet;
    dlocest: TDataSetProvider;
    clocalest: TClientDataSet;
    dcriaparametro: TDataSetProvider;
    ccriaparametro: TClientDataSet;
    DBMemo1: TDBMemo;
    sorcamentovenda: TSQLDataSet;
    dorcamentovenda: TDataSetProvider;
    corcamentovenda: TClientDataSet;
    BitBtn2: TBitBtn;
    bretornaped: TBitBtn;
    eOutras: TEdit;
    Label10: TLabel;
    sInsertDesc: TSQLDataSet;
    cInsertDesc: TClientDataSet;
    pInsertDesc: TDataSetProvider;
    SpeedButton1: TSpeedButton;
    cDeleta: TClientDataSet;
    pDeleta: TDataSetProvider;
    sDeleta: TSQLDataSet;
    Imprimir1: TMenuItem;
    cParam: TClientDataSet;
    pParam: TDataSetProvider;
    sParam: TSQLDataSet;
    cMovTitulo: TClientDataSet;
    dMovTitulo: TDataSetProvider;
    sMovTitulo: TSQLDataSet;
    procedure BitBtn1Click(Sender: TObject);
    procedure P1Exit(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SpeedButton1Click(Sender: TObject);
    procedure Limpa1Click(Sender: TObject);
    procedure Procura1Click(Sender: TObject);
    procedure Sair1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ConfiguracaoChanging(Sender: TObject;
      var AllowChange: Boolean);
    procedure bretornapedClick(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure eOrcamentoEnter(Sender: TObject);
    procedure bSalvaConfgClick(Sender: TObject);
  private
    { Private declarations }
  public
    procedure Busca;
    procedure GeraTitulos;
//    procedure geracomissao(vtitulo:integer; vlrtitulo :double; vtitdocumento: string);
    procedure AcertaPed;
    procedure excluititulos;
    procedure descancelapedido;
    procedure corrigeItens;

  end;

var
  Form1: TForm1;
  parcelas : TStrings;
  vlrdesconto, vLote : integer;
  valor : Double;
implementation

uses Math;

{$R *.dfm}

procedure TForm1.FormCreate(Sender: TObject);
 var iDiskSize: int64;
     path : String;
begin

  path := copy(ExtractFilePath(Application.ExeName),1,1);
  iDiskSize := SysUtils.DiskSize(byte(path[1]) - $40);


  //if iDiskSize > 11000000000 then
  //Begin
  //  ShowMessage('Espa�o insuficiente no disco!');
  //  Application.Terminate;
  //end;
  parcelas := TStringList.Create;
  If FileExists('pac.ini') Then
    conf.Lines.LoadFromFile('pac.ini')
  else
    ShowMessage('Arquivo de inicializa��o n�o foi encontrado!' );

end;

procedure TForm1.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  parcelas.Free;
end;

procedure TForm1.Busca;
var prazo: String;
    totparc,pr : double;
    posicao : integer;
    vparcela : string;
begin

  Corcamento.Close;
  Corcamento.Params.ParamByName('i_cdorcamento').AsInteger := StrToInt(eOrcamento.text);
  Corcamento.Open;

  if eDesconto.Visible then
    vlrdesconto := StrToInt(eDesconto.Text)
  else
    vlrdesconto := 100;

  if Corcamento.RecordCount > 0 then
  begin
    evalor.Text :=  FloatToStr(Corcamento.fieldbyname('n_totliquido').AsFloat * (1 - (vlrdesconto/100)));

    cTitulo.Close;
    cTitulo.CommandText := ' Select i_cdtitulo, c_documento, d_emissao,d_vencimento, '+
                           ' n_vrtitulo, f_status, i_cdcontafin  '+
                           ' from titulo '+
                           ' where i_nrpedido is null '+
                           ' and i_cdfatura is null '+
                           ' and c_documento like :c_documento  '+
                           ' and i_cdparceiro = :i_cdparceiro ';
    cTitulo.Params.ParamByName('c_documento').AsString := eOrcamento.text + '%';
    cTitulo.Params.ParamByName('i_cdparceiro').AsInteger := Corcamento.fieldbyname('i_cdparccliente').AsInteger;
    cTitulo.Open;

  end;
end;

procedure TForm1.BitBtn1Click(Sender: TObject);
 var i:integer;
begin

  if not Corcamento.Active then exit;

  for i := 1 to length(eDesconto.text) do
  if not (eDesconto.text[i] in ['0'..'9']) then
  begin
      ShowMessage('caracter inv�lido no desconto');
      exit;
  end;

  for i := 1 to length(eOutras.text) do
  if not (eOutras.text[i] in ['0'..'9',',']) then
  begin
      ShowMessage('caracter inv�lido em outras despesas!');
      exit;
  end;

  If (vlrdesconto <= 0) or (vlrdesconto > 100) then
  begin
      ShowMessage('Desconto n�o pode ser nenor que 1 ou maior que 100%');
      exit;
  end;

  if eDesconto.Visible then
    vlrdesconto := StrToInt(eDesconto.Text)
  else
    vlrdesconto := 100;


  if Corcamento.FieldByName('f_status').AsString = 'C' then
  begin
    MessageDlg('j� cancelado!', mtError,  [mbOk], 0);
    exit;
  end;
               {
  if Corcamentof_faturou.AsString = 'S' then
  begin
    MessageDlg('Aten��o, j� foi faturado!', mtError,  [mbOk], 0);
    exit;
  end;        }

   if MessageDlg('Confirma a Gera��o?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
  begin
    GeraTitulos;

    cTitulo.Close;
    cTitulo.CommandText := ' Select i_cdtitulo, c_documento, d_emissao,d_vencimento, '+
                           ' n_vrtitulo, f_status, i_cdcontafin  '+
                           ' from titulo '+
                           ' where i_nrpedido is null '+
                           ' and i_cdfatura is null '+
                           ' and c_documento like :c_documento  '+
                           ' and i_cdparceiro = :i_cdparceiro ';
    cTitulo.Params.ParamByName('c_documento').AsString := eOrcamento.text + '%';
    cTitulo.Params.ParamByName('i_cdparceiro').AsInteger := Corcamento.fieldbyname('i_cdparccliente').AsInteger;
    cTitulo.Open;

    MessageDlg('concluido!', mtInformation,  [mbOk], 0);
  end;

  Corcamento.Close;

end;

procedure TForm1.P1Exit(Sender: TObject);
begin
   IF Tedit(Sender).text = '' then BitBtn1.SetFocus;
end;

procedure TForm1.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
   if key = VK_RETURN then
   begin
      // 2
      if Shift = [ssShift]  then
         SelectNext(ActiveControl, False, True)
      // 1
      else
         SelectNext(ActiveControl, True, True);
   end;

end;

procedure TForm1.SpeedButton1Click(Sender: TObject);
begin
  Busca;

end;

procedure TForm1.Limpa1Click(Sender: TObject);
begin
  Corcamento.Close;
  cTitulo.Close;
  eOrcamento.SetFocus;
  eOrcamento.Text := '';
  evalor.text := '0,00';
end;

procedure TForm1.Procura1Click(Sender: TObject);
begin
  Busca;
end;

procedure TForm1.Sair1Click(Sender: TObject);
begin
  Close;
end;

procedure TForm1.GeraTitulos;
var vpt, totparcelas : integer;
    vlrparcelas, vlrultparce, valorboleto, vlrreal, vlrcomissao : double;
    vcdvendedor,vCdContaFin, vCdFormapgto : integer;
    vTipo : string;
    vi_cdtitulo : integer;
    TD : TTransactionDesc;

begin

 // valor := ( Corcamenton_vlrseparado.AsFloat  - Corcamenton_vlrdesconto.AsFloat) * (vlrdesconto/100);
  vlrreal := Corcamento.fieldbyname('n_totliquido').AsFloat;
  valor   := (Corcamento.fieldbyname('n_totliquido').AsFloat - StrToFloatDef(eValor.Text,0)) + StrToFloat(eOutras.Text);


  if conf.Lines.Values['tipoconta'] =  '' then
  begin
    MessageDlg('Tipoconta n�o foi informado no cpfv.ini!', mtError,  [mbOk], 0);
    exit;
  end;

  if conf.Lines.Values['contafin'] = '' then
  begin
    MessageDlg('contafin n�o foi informado no cpfv.ini!', mtError,  [mbOk], 0);
    exit;
  end;

  if conf.Lines.Values['formapagto'] = '' then
  begin
    MessageDlg('formapagto n�o foi informado no cpfv.ini!', mtError,  [mbOk], 0);
    exit;
  end;

  vtipo := conf.Lines.Values['tipoconta'];
  vCdContaFin := StrToInt(conf.Lines.Values['contafin']);
  vCdFormapgto := StrToInt(conf.Lines.Values['formapagto']);
  cvendedor.Close;
  cvendedor.CommandText := 'select i_cdvendedor from vendedor where i_cdparceiro = :i_cdparceiro ';
  cvendedor.Params.ParamByName('i_cdparceiro').AsInteger := Corcamento.fieldbyname('i_cdparcvendedor').AsInteger;
  cvendedor.Open;


  if not SQLConnection.InTransaction then
  begin
    TD.TransactionID := 1;
    TD.IsolationLevel := xilREADCOMMITTED;
    SQLConnection.StartTransaction(TD);
    try
        criatitulo.Close;
        criatitulo.CommandText := 'select CriaTituloRecebimento(:F_Tipo,:I_CdContaFin,:I_CdParceiro,:Data,cast(:N_VrTitulo as numeric),:c_Documento,:i_CdVendedor,cast(:N_VrLiquido as numeric),:I_CDEmpresa) as id_titulo' ;
        criatitulo.Params.ParamByName('F_Tipo').AsString  := vTipo;
        criatitulo.Params.ParamByName('I_CdContaFin').AsInteger := vCdContaFin;
        criatitulo.Params.ParamByName('I_CdParceiro').AsInteger := Corcamento.fieldbyname('i_cdparccliente').AsInteger;
        criatitulo.Params.ParamByName('Data').AsDate := Date;
        criatitulo.Params.ParamByname('N_VrTitulo').AsFloat := valor;
        criatitulo.Params.ParamByName('C_Documento').AsString  := Corcamento.fieldbyname('i_cdorcamento').AsString + '_01/01';
        criatitulo.Params.ParamByName('I_CdVendedor').AsInteger := cvendedor.fieldbyname('i_cdvendedor').AsInteger;
        criatitulo.Params.ParamByName('N_VrLiquido').AsFloat := valor;
        criatitulo.Params.ParamByName('I_CDEmpresa').AsFloat := Corcamento.fieldbyname('i_cdempresa').AsInteger;
        criatitulo.open;
        vi_cdtitulo := criatitulo.Fieldbyname('id_titulo').asinteger;

        cLoteRec.Close;
        cLoteRec.Params.ParamByName('d_lancamento').AsDate := Now();
        cLoteRec.Params.ParamByName('i_cdvendedor').AsInteger := cvendedor.fieldbyname('i_cdvendedor').AsInteger;
        cLoteRec.Params.ParamByName('c_documento').AsString := Corcamento.fieldbyname('i_cdorcamento').AsString + '_01/01';
        cLoteRec.Params.ParamByName('i_cdparceiro').AsInteger:=  Corcamento.fieldbyname('i_cdparccliente').AsInteger;
        cLoteRec.Params.ParamByName('i_cdcaixa').AsInteger := vCdContaFin;
        cLoteRec.Params.ParamByName('i_cdbanco').AsInteger := vCdContaFin;
        cLoteRec.Execute;

        cLote.Close;
        cLote.CommandText := ' select max(i_cdloterecebimento) as i_cdloterecebimento from loterecebimento ';
        cLote.Open;

        vLote := cLote.fieldbyname('i_cdloterecebimento').AsInteger;

        cMovTitulo.Close;
        cMovTitulo.CommandText :=  ' insert into movtitulo (i_cdloterecebimento,f_status,d_movtitulo,i_cdtitulo,n_valpago,n_vrdesconto,i_cdcontafin,c_documento,c_obs,i_cdformapagto,f_fechado) ' +
                                   ' values (:i_cdloterecebimento,:f_status,:d_movtitulo,:i_cdtitulo,:n_valpago,:n_vrdesconto,:i_cdcontafin,:c_documento,:c_obs,:i_cdformapagto,:f_fechado) ';
        cMovTitulo.Params.ParamByName('i_cdloterecebimento').AsInteger := vLote;
        cMovTitulo.Params.ParamByName('f_status').AsString := 'Q';
        cMovTitulo.Params.ParamByName('d_movtitulo').AsDate := Now();
        cMovTitulo.Params.ParamByName('i_cdtitulo').AsInteger := vi_cdtitulo;
        cMovTitulo.Params.ParamByName('n_valpago').AsFloat := valor;
        cMovTitulo.Params.ParamByName('n_vrdesconto').AsFloat := 0;
        cMovTitulo.Params.ParamByName('i_cdcontafin').AsInteger := vCdContaFin;
        cMovTitulo.Params.ParamByName('c_documento').AsString := Corcamento.fieldbyname('i_cdorcamento').AsString + '_01/01';
        cMovTitulo.Params.ParamByName('c_obs').AsString := DBMemo1.Text;
        cMovTitulo.Params.ParamByName('i_cdformapagto').AsInteger := vCdFormapgto;
        cMovTitulo.Params.ParamByName('f_fechado').AsString := 'N';
        cMovTitulo.Execute;

        cMovTitulo.Close;
        cMovTitulo.CommandText :=  ' update movtitulo set f_fechado = ''S'' ' +
                                   ' where i_cdtitulo = :i_cdtitulo ';
        cMovTitulo.Params.ParamByName('i_cdtitulo').AsInteger := vi_cdtitulo;
        cMovTitulo.Execute;

        cLote.Close;
        cLote.CommandText := ' update loterecebimento set f_fechado = ''S'' where i_cdloterecebimento = :i_cdloterecebimento ';
        cLote.Params.ParamByName('i_cdloterecebimento').AsInteger := vLote;
        cLote.Execute;
        AcertaPed;
        SQLConnection.Commit(TD);
    except
      on E: Exception do
      begin
         SQLConnection.Rollback(TD);
         raise;
      end;
    end;
  end;

end;

procedure TForm1.AcertaPed;
 var novoprc, novodes, prctot : double;
     vprazo : string;
     vpt : integer;
begin

   //altera orcamento
    Corcamentovenda.Close;
    Corcamentovenda.CommandText :=
              ' Update orcamento set             '+
              ' F_LIBERADO_PAGTO = ''N'' ,         '+
              '    c_observacao =  :c_observacao  '+
              ' Where i_cdorcamento  = :i_cdorcamento    ';
    corcamentovenda.Params.ParamByName('c_observacao').AsString := DBMemo1.Text;
    Corcamentovenda.Params.ParamByName('i_cdorcamento').AsInteger := Corcamento.fieldbyname('i_cdorcamento').AsInteger;
    Corcamentovenda.execute;



  if vlrdesconto = 100 then
  begin
    //com desconto = 100  cancela o or�amento

    //cancela o or�amento
    Corcamentovenda.Close;
    Corcamentovenda.CommandText :=
              ' Update orcamento  set             '+
              '    f_status = ''C''          '+
              ' Where i_cdorcamento  = :i_cdorcamento   ';
    Corcamentovenda.Params.ParamByName('i_cdorcamento').AsInteger := Corcamento.fieldbyname('i_cdorcamento').AsInteger;
    Corcamentovenda.execute;

    //busca local para baixar estoque
    clocalest.Close;
    clocalest.CommandText :=
              ' select i_cdlocal_estoque        '+
              '   from local_estoque            '+
              ' Where i_cdempresa  = :i_cdempresa  ';
    clocalest.Params.ParamByName('i_cdempresa').AsInteger := Corcamento.fieldbyname('i_cdempresa').AsInteger;
    clocalest.Open;

    //baixa estoque
    citem.Close;
    citem.CommandText :=
              '     Select                        '+
              '      i_cdproduto,                 '+
              '      n_quantidade                 '+
              ' From Item_orcamento               '+
              ' Where i_cdorcamento  = :i_cdorcamento   ';
    citem.Params.ParamByName('i_cdorcamento').AsInteger := Corcamento.fieldbyname('i_cdempresa').AsInteger;
    citem.Open;

    while not citem.Eof do
    begin
      cupdateitem.Close;
      cupdateitem.CommandText :=
              ' Update estoque set                 '+
              '    n_qtdestoque = n_qtdestoque - :n_qtdestoque    '+
              ' Where i_Cdproduto = :i_Cdproduto   '+
              '   and i_cdlocal_estoque = :i_cdlocal_estoque ';
      cupdateitem.Params.ParamByName('n_qtdestoque').AsFloat        := citem.FieldByName('n_quantidade').AsInteger;
      cupdateitem.Params.ParamByName('i_Cdproduto').AsInteger       := citem.FieldByName('i_cdproduto').AsInteger;
      cupdateitem.Params.ParamByName('i_cdlocal_estoque').AsInteger := clocalest.FieldByName('i_cdlocal_estoque').AsInteger;
      cupdateitem.execute;

      citem.Next;
    end;

  end
  else
  begin //com desconto corrige valores do item


      citem.Close;
      citem.CommandText :=
                '     Select                              '+
                '      i_cditem_orcamento,                '+
                '      n_precovenda,                      '+
                '      n_percdesconto                     '+
                ' From Item_orcamento                     '+
                ' Where i_cdorcamento  = :i_cdorcamento   ';
      citem.Params.ParamByName('i_cdorcamento').AsInteger := Corcamento.fieldbyname('i_cdorcamento').AsInteger;
      citem.Open;

      while not citem.Eof do
      begin
        cInsertDesc.Close;
        cInsertDesc.Params.ParamByName('i_cddesc_comv').AsInteger := citem.FieldByName('i_cditem_orcamento').AsInteger;
        cInsertDesc.Params.ParamByName('n_valor').AsFloat         := citem.FieldByName('n_percdesconto').AsFloat;
        cInsertDesc.Execute;

        novoprc := citem.FieldByName('n_precovenda').AsFloat * ((100-vlrdesconto)/100);
        novodes := 100 - ((100 - citem.FieldByName('n_percdesconto').AsFloat) * ((100-vlrdesconto)/100));

        cupdateitem.Close;
        cupdateitem.CommandText :=
                ' Update Item_orcamento set                 '+
                '    n_precovenda   = :n_precovenda,            '+
                '    n_percdesconto = :n_percdesconto     '+
                ' Where Item_orcamento.i_cditem_orcamento  = :i_cditem_orcamento   ';
        cupdateitem.Params.ParamByName('n_precovenda').AsFloat := novoprc;
        cupdateitem.Params.ParamByName('n_percdesconto').AsFloat := 0;
        cupdateitem.Params.ParamByName('i_cditem_orcamento').AsInteger := citem.FieldByName('i_cditem_orcamento').AsInteger;
        cupdateitem.execute;

        citem.Next;
      end;
  end;

  Corcamentovenda.Close;
  Corcamentovenda.CommandText :=
              ' Update orcamento set             '+
              ' F_LIBERADO_PAGTO = ''S'' '+
              ' Where i_cdorcamento  = :i_cdorcamento    ';
   Corcamentovenda.Params.ParamByName('i_cdorcamento').AsInteger := Corcamento.fieldbyname('i_cdorcamento').AsInteger;
   Corcamentovenda.execute;
end;

procedure TForm1.eOrcamentoEnter(Sender: TObject);
begin
  if not SQLConnection.Connected then
  begin
   if not FileExists('pac.ini') or (conf.Lines.Count = 0) Then
     close;
   SQLConnection.Connected := False;
   SQLConnection.Params := conf.Lines;
   SQLConnection.Connected := True;

   ccriaparametro.Close;
   ccriaparametro.CommandText := 'select criaparametro() ';
   ccriaparametro.Active := true;

   ccriaparametro.Close;
   ccriaparametro.CommandText := 'select insparametro(''I_CdUsuarioAtual'',:usuario)';
   ccriaparametro.Params.ParamByName('usuario').AsString :=  conf.Lines.Values['usuario'];
   ccriaparametro.Active := true;


   if conf.Lines.Values['Desconto'] <> '' then
      eDesconto.Text := conf.Lines.Values['Desconto'];
  end;
end;

procedure TForm1.bSalvaConfgClick(Sender: TObject);
begin
  conf.Lines.SaveToFile('pac.ini');

end;

procedure TForm1.ConfiguracaoChanging(Sender: TObject;
  var AllowChange: Boolean);
begin
   bSalvaConfg.Visible := (Configuracao.ActivePage = TabSheet1);
end;

procedure TForm1.bretornapedClick(Sender: TObject);
begin
  //verifica se algum titulo esta pago total  ou parcial
  if not  Corcamento.active then
  begin
    ShowMessage('Nenhuma venda selecionada!' );
    exit;
  end;

  {cTitulo.First;
  while not  cTitulo.Eof do
  begin
    if cTitulo.FieldByName('n_vracerto').AsFloat > 0 then
    begin
      ShowMessage('O titulo ' + ctitulo.FieldByName('i_cdtitulo').AsString + ' j� foi movimentado!, este n�o pode ser excluido!' );
      exit;
    end;
    cTitulo.Next;
  end;}

  if (Corcamento.FieldByName('f_status').AsString = 'F') then
  begin
    MessageDlg('O venda est� finalizada!', mtConfirmation, [mbOK], 0);
    exit;
  end;

  if (Corcamento.FieldByName('f_status').AsString = 'C') then
  begin
    if MessageDlg('O venda est� cancelado!,Corrige o estoque e exclui titulos?', mtConfirmation, [mbYes, mbNo], 0) = mrNo then
      exit;
    descancelapedido;
  end
  else
  begin
    if MessageDlg('Deseja exclui os titulos e acertar os itens da venda?', mtConfirmation, [mbYes, mbNo], 0) = mrNo then
      exit;
    corrigeItens;
  end;
  SpeedButton1Click(sender);
end;

procedure TForm1.descancelapedido;
var  TD : TTransactionDesc;
begin
  if not SQLConnection.InTransaction then
  begin
    TD.TransactionID := 1;
    TD.IsolationLevel := xilREADCOMMITTED;
    SQLConnection.StartTransaction(TD);
    try

      excluititulos;

      //busca local para baixar estoque
      clocalest.Close;
      clocalest.CommandText :=
                ' select i_cdlocal_estoque        '+
                '   from local_estoque            '+
                ' Where i_cdempresa  = :i_cdempresa  ';
      clocalest.Params.ParamByName('i_cdempresa').AsInteger := Corcamento.fieldbyname('i_cdempresa').AsInteger;
      clocalest.Open;

      //baixa estoque
      citem.Close;
      citem.CommandText :=
              '     Select                        '+
              '      i_cdproduto,                 '+
              '      n_quantidade                 '+
              ' From Item_orcamento               '+
              ' Where i_cdorcamento  = :i_cdorcamento   ';
      citem.Params.ParamByName('i_cdorcamento').AsInteger := Corcamento.fieldbyname('i_cdorcamento').AsInteger;
      citem.Open;

     while not citem.Eof do
     begin
        cupdateitem.Close;
        cupdateitem.CommandText :=
                ' Update estoque set                 '+
                '    n_qtdestoque = n_qtdestoque + :n_qtdestoque    '+
                ' Where i_Cdproduto = :i_Cdproduto   '+
                '   and i_cdlocal_estoque = :i_cdlocal_estoque ';
        cupdateitem.Params.ParamByName('n_qtdestoque').AsFloat        := citem.FieldByName('n_quantidade').AsInteger;
        cupdateitem.Params.ParamByName('i_Cdproduto').AsInteger       := citem.FieldByName('i_cdproduto').AsInteger;
        cupdateitem.Params.ParamByName('i_cdlocal_estoque').AsInteger := clocalest.FieldByName('i_cdlocal_estoque').AsInteger;
        cupdateitem.execute;

        citem.Next;

      end;
      Corcamentovenda.Close;
      Corcamentovenda.CommandText :=
              ' Update orcamento  set             '+
              '    f_status = ''O''          '+
              ' Where i_cdorcamento  = :i_cdorcamento   ';
      Corcamentovenda.Params.ParamByName('i_cdorcamento').AsInteger := Corcamento.fieldbyname('i_cdorcamento').AsInteger;
      Corcamentovenda.execute;

      SQLConnection.Commit(TD);
    except
      on E: Exception do
      begin
        SQLConnection.Rollback(TD);
        raise;
      end;
    end;
  end;
end;


procedure TForm1.corrigeItens;
var     novoprc, novodes,vdesc : double;
        TD : TTransactionDesc;
begin
    if not SQLConnection.InTransaction then
    begin
      TD.TransactionID := 1;
      TD.IsolationLevel := xilREADCOMMITTED;
      SQLConnection.StartTransaction(TD);
      try
        excluititulos;

        Corcamentovenda.Close;
        Corcamentovenda.CommandText :=
              ' Update orcamento set             '+
              ' F_LIBERADO_PAGTO = ''N'' '+
              ' Where i_cdorcamento  = :i_cdorcamento    ';
        Corcamentovenda.Params.ParamByName('i_cdorcamento').AsInteger := Corcamento.fieldbyname('i_cdorcamento').AsInteger;
        Corcamentovenda.execute;


        citem.Close;
        citem.CommandText :=
                  ' Select                               '+
                  '      io.i_cdItem_orcamento,            '+
                  '      io.n_precovenda,                  '+
                  '      io.N_PercDesconto,              '+
                  '      dc.n_valor                      '+
                  ' From Item_orcamento io                 '+
                  ' join desc_comv dc on dc.i_cddesc_comv = io.i_cdItem_orcamento '+
                  ' Where io.I_cdorcamento  = :I_cdorcamento   ';
        citem.Params.ParamByName('I_cdorcamento').AsInteger := Corcamento.fieldbyname('i_cdorcamento').AsInteger;
        citem.Open;

        while not citem.Eof do
        begin
          vdesc   := 100 - (100 - StrToFloatDef(eDesconto.Text,0))/((100 - citem.FieldByname('n_valor').AsFloat)/100);
          novoprc := citem.FieldByName('n_precovenda').AsFloat /((100-vdesc)/100);
          novodes := citem.FieldByname('n_valor').AsFloat;

          cDeleta.Close;
          cDeleta.Params.ParamByName('i_cddesc_comv').AsInteger := citem.fieldbyname('i_cdItem_orcamento').AsInteger;
          cDeleta.Execute;

          cupdateitem.Close;
          cupdateitem.CommandText :=
                  ' Update Item_orcamento set                 '+
                  '    n_precovenda = :n_precovenda,            '+
                  '    n_percDesconto = :n_percDesconto     '+
                  ' Where Item_orcamento.i_cdItem_orcamento  = :i_cdItem_orcamento   ';
          cupdateitem.Params.ParamByName('n_precovenda').AsFloat := novoprc;
          cupdateitem.Params.ParamByName('n_percDesconto').AsFloat := novodes;
          cupdateitem.Params.ParamByName('i_cdItem_orcamento').AsInteger := citem.FieldByName('i_cdItem_orcamento').AsInteger;
          cupdateitem.execute;

          citem.Next;
        end;

        Corcamentovenda.Close;
        Corcamentovenda.CommandText :=
              ' Update orcamento set             '+
              ' F_LIBERADO_PAGTO = ''S'' '+
              ' Where i_cdorcamento  = :i_cdorcamento    ';
        Corcamentovenda.Params.ParamByName('i_cdorcamento').AsInteger := Corcamento.fieldbyname('i_cdorcamento').AsInteger;
        Corcamentovenda.execute;

        SQLConnection.Commit(TD);
      except
        on E: Exception do
        begin
          SQLConnection.Rollback(TD);
          raise;
        end;
      end;
    end;

end;

procedure TForm1.excluititulos;
var vExcluiLote, vExcluiMov : integer;
begin

  cTitulo.First;
  while not  cTitulo.Eof do
  begin
        cMovTitulo.Close;
        cMovTitulo.CommandText := 'select i_cdloterecebimento,i_cdmovfinanceira from movtitulo where i_cdtitulo = :i_cdtitulo ';
        cMovTitulo.Params.ParamByName('i_cdtitulo').AsInteger  := cTitulo.FieldByName('i_cdtitulo').AsInteger;
        cMovTitulo.Open;

        vExcluiLote := cMovTitulo.fieldbyname('i_cdloterecebimento').AsInteger;

        vExcluiMov  := cMovTitulo.fieldbyname('i_cdmovfinanceira').AsInteger;


   criatitulo.Close;
   criatitulo.CommandText := ' Delete from movtitulo '+
                             ' where i_cdtitulo = :i_cdtitulo ';
   criatitulo.Params.ParamByName('i_cdtitulo').AsInteger  := cTitulo.FieldByName('i_cdtitulo').AsInteger;
   criatitulo.Execute;

    cLote.Close;
    cLote.CommandText := 'delete from loterecebimento where i_cdloterecebimento = :i_cdloterecebimento';
    cLote.Params.ParamByName('i_cdloterecebimento').AsInteger := vExcluiLote;
    cLote.Execute;

    cLote.Close;
    cLote.CommandText := 'delete from movfinanceira where i_cdmovfinanceira = :i_cdmovfinanceira';
    cLote.Params.ParamByName('i_cdmovfinanceira').AsInteger := vExcluiMov;
    cLote.Execute;



   criatitulo.Close;
   criatitulo.CommandText := ' Delete from contager_titulo '+
                             ' where i_cdtitulo = :i_cdtitulo ';
   criatitulo.Params.ParamByName('i_cdtitulo').AsInteger  := cTitulo.FieldByName('i_cdtitulo').AsInteger;
   criatitulo.Execute;

   criatitulo.Close;
   criatitulo.CommandText := ' update Titulo '+
                             ' set n_vracerto = 0 , f_validado = ''N'' '+
                             ' where i_cdtitulo = :i_cdtitulo ';
   criatitulo.Params.ParamByName('i_cdtitulo').AsInteger  := cTitulo.FieldByName('i_cdtitulo').AsInteger;
   criatitulo.Execute;

   criatitulo.Close;
   criatitulo.CommandText := ' Delete from Titulo '+
                             ' where i_cdtitulo = :i_cdtitulo ';
   criatitulo.Params.ParamByName('i_cdtitulo').AsInteger  := cTitulo.FieldByName('i_cdtitulo').AsInteger;
   criatitulo.Execute;

   cTitulo.Next;
  end;
end;


procedure TForm1.BitBtn3Click(Sender: TObject);
var vReport : string;
begin
  {
  if eOrcamento.Text <> '' then
  begin
    rcItemPedido.Close;
    rcItemPedido.Params.ParamByName('pedido').AsInteger := StrToInt(eOrcamento.Text);
    rcItemPedido.Open;

    cParam.Close;
    cParam.Params.ParamByName('param').AsString := 'C_MASCARAPRODUTO';
    cParam.Open;

    rcItemPedido.FieldByName('c_codigo').EditMask := cParam.fieldbyname('c_valor').AsString;

    rcPedVenda.Close;
    rcPedVenda.Params.ParamByName('pedido').AsInteger := StrToInt(eOrcamento.Text);
    rcPedVenda.Open;

    rcPedVenda.FieldByName('c_prazo').EditMask := '000/000/000/000/000/000/000/;0;*';

    rcCliente.Close;
    rcCliente.Params.ParamByName('cliente').AsInteger := rcPedVenda.fieldbyname('i_cdcliente').AsInteger;
    rcCliente.Open;

    if rcCliente.FieldByName('c_cpf').AsString <> '' then
      rcCliente.FieldByName('c_cpf').EditMask := '000.000.000-00;0;*'
    else
      rcCliente.FieldByName('c_cnpj').EditMask := '00.000.000/0000-00;0;*';


    rcTitulo.Close;
    rcTitulo.Params.ParamByName('c_documento').AsString := eOrcamento.text + '%';
    rcTitulo.Params.ParamByName('i_cdparceiro').AsInteger := StrToInt(rcPedVenda.fieldbyname('i_cdcliente').AsString + '1');
    rcTitulo.Params.ParamByName('i_cdempresa').AsInteger := rcPedVenda.fieldbyname('i_cdempresa').AsInteger;
    rcTitulo.Open;

    RvSystem1.DefaultDest := rdPreview;
    RvSystem1.SystemSetups := RvSystem1.SystemSetups - [ssAllowSetup];
    IF conf.Lines.Values['RptRec'] <> '' THEN
      vReport := 'RptRec_'+ conf.Lines.Values['RptRec']
    else
      vReport := 'RptRec';
    if MessageBox(0, Pchar('Deseja exibir a observa��o do pedido?'), 'Confirma��o', MB_YESNO+MB_DEFBUTTON2+MB_ICONQUESTION+MB_TASKMODAL ) = IDYES then
      RvProject1.SetParam('pObs','Obs.: ' + Trim(rcPedVenda.fieldbyname('c_observacao').AsString))
    else
      RvProject1.SetParam('pObs','');

    RvProject1.SelectReport(vReport,True);           
    RvProject1.Execute;
  end
  else
    MessageDlg('Digite o c�digo para impress�o!', mtError,[mbOK] ,0);
    }
end;

end.











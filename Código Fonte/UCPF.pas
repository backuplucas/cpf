unit UCPF;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, FMTBcd, Provider, SqlExpr, DB, DBClient, StdCtrls,
  Buttons, Mask, DBCtrls, Menus, Grids, DBGrids, ExtCtrls, ComCtrls,
  RpDefine, RpCon, RpConDS, RpRenderCanvas, RpRenderPrinter, RpRave,
  RpRenderText, RpRenderRTF, RpRenderHTML, RpRender, RpRenderPDF, RpBase,
  RpSystem, {U_BematechDll, rotinastef,} ToolWin, Registry,// U_Funcoes,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf,
  FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async,
  FireDAC.Phys, FireDAC.Phys.PG, FireDAC.Phys.PGDef, FireDAC.VCLUI.Wait,
  FireDAC.Comp.Client, FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf,
  FireDAC.DApt, FireDAC.Comp.DataSet, frxExportXML, frxExportText, frxExportRTF,
  frxClass, frxExportPDF, frxExportImage, frxDBSet, frxExportCSV, frxDesgn,
  frxExportBaseDialog, frxExportBIFF, SLClientDataSet; //, SLClientDataSet;

type
  TForm1 = class(TForm)
    dsPedido: TDataSource;
    dspPedido: TDataSetProvider;
    dspCriaTitulo: TDataSetProvider;
    cdsCriaTitulo: TClientDataSet;
    PopupMenu1: TPopupMenu;
    Limpa1: TMenuItem;
    Procura1: TMenuItem;
    Gera1: TMenuItem;
    Sair1: TMenuItem;
    dspTitulo: TDataSetProvider;
    cdsTitulo: TClientDataSet;
    dsTitulo: TDataSource;
    cdsVendedor: TClientDataSet;
    dspVendedor: TDataSetProvider;
    dspValorBoleto: TDataSetProvider;
    cdsValorPedido: TClientDataSet;
    dspTotComissao: TDataSetProvider;
    cdsTotComissao: TClientDataSet;
    dspFuncLanc: TDataSetProvider;
    cdsFuncLanc: TClientDataSet;
    dspItemPed: TDataSetProvider;
    cdsItemPed: TClientDataSet;
    dspUpdateItem: TDataSetProvider;
    cdsUpdateItem: TClientDataSet;
    dspCalcPedido: TDataSetProvider;
    cdsCalPedido: TClientDataSet;
    dspCriaParametro: TDataSetProvider;
    cdsCriaParametro: TClientDataSet;
    dspPedidoVenda: TDataSetProvider;
    cdsPedidoVenda: TClientDataSet;
    cdsInsertDesc: TClientDataSet;
    dspInsertDesc: TDataSetProvider;
    cdsDeleta: TClientDataSet;
    dspDeleta: TDataSetProvider;
    dspRdTitulo: TDataSetProvider;
    RvTitulo: TRvDataSetConnection;
    dspRdCliente: TDataSetProvider;
    RvCliente: TRvDataSetConnection;
    cdsRcItemPedido: TClientDataSet;
    dspItemPedido: TDataSetProvider;
    RvItemPedido: TRvDataSetConnection;
    cdsRcPedVenda: TClientDataSet;
    dspRdPedVenda: TDataSetProvider;
    RvPedVenda: TRvDataSetConnection;
    cdsRcTitulo: TClientDataSet;
    cdsRcCliente: TClientDataSet;
    Imprimir1: TMenuItem;
    cdsParam: TClientDataSet;
    dspParam: TDataSetProvider;
    RvProject1: TRvProject;
    RvSystem1: TRvSystem;
    RvRenderPDF1: TRvRenderPDF;
    cdsPedido: TClientDataSet;
    cdsPedidoi_nrpedido: TIntegerField;
    cdsPedidod_faturamento: TSQLTimeStampField;
    cdsPedidoi_cdcliente: TIntegerField;
    cdsPedidoi_cdempresa: TSmallintField;
    dspHistoricoCNAB: TDataSetProvider;
    cdsHistoricoCNAB: TClientDataSet;
    cdsDiasVerificacao: TClientDataSet;
    dspDiasVerificacao: TDataSetProvider;
    cdsRcTipoVend: TClientDataSet;
    dspRdTipoVend: TDataSetProvider;
    rptipovend: TRvDataSetConnection;
    cdsFechaGer: TClientDataSet;
    dspFechaGer: TDataSetProvider;
    cdsVerificaMes: TClientDataSet;
    dspVerificaMes: TDataSetProvider;
    cdsDespesas: TClientDataSet;
    dspDespesas: TDataSetProvider;
    rvDespesas: TRvDataSetConnection;
    RvProject2: TRvProject;
    RvSystem2: TRvSystem;
    cdsEmpresa: TClientDataSet;
    dspEmpresa: TDataSetProvider;
    cdsRcOrcamento: TClientDataSet;
    dspRdOrcamento: TDataSetProvider;
    cdsRcOpRequisicao: TClientDataSet;
    dspRdOpRequisicao: TDataSetProvider;
    cdsBuscaTitEnt: TClientDataSet;
    dspBuscaTitEnt: TDataSetProvider;
    cdsPedidoi_cdlocal_estoque: TIntegerField;
    cdsUpdate: TClientDataSet;
    dspUpdate: TDataSetProvider;
    cdsPedidoi_fatura: TIntegerField;
    cdsInsertLog: TClientDataSet;
    dspInsertLog: TDataSetProvider;
    cdsEstoque: TClientDataSet;
    dspEstoque: TDataSetProvider;
    cdsBuscaFormaPagto: TClientDataSet;
    dspRBuscaFormaPagto: TDataSetProvider;
    cdsPedidoi_cdformapagto: TIntegerField;
    pcCpf: TPageControl;
    ts1: TTabSheet;
    ts2: TTabSheet;
    Panel2: TPanel;
    Configuracao: TPageControl;
    TabSheet3: TTabSheet;
    DBGrid1: TDBGrid;
    TabSheet4: TTabSheet;
    eConfiguracao: TMemo;
    TabSheet5: TTabSheet;
    Label11: TLabel;
    Label12: TLabel;
    lData: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    eDataGenFinal: TEdit;
    bFechaGer: TBitBtn;
    bRelGenrencial: TBitBtn;
    eAno: TEdit;
    eDataGenInicial: TMaskEdit;
    ComboBoxEmp: TComboBox;
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label6: TLabel;
    Label4: TLabel;
    lperc: TLabel;
    ldesconto: TLabel;
    Label8: TLabel;
    Label5: TLabel;
    Label7: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    SpeedButton1: TSpeedButton;
    Label16: TLabel;
    Label17: TLabel;
    bGeraComplemento: TBitBtn;
    ePedido: TEdit;
    DBEdit4: TDBEdit;
    DBCheckBox1: TDBCheckBox;
    DBCheckBox3: TDBCheckBox;
    DBEdit1: TDBEdit;
    P1: TEdit;
    P2: TEdit;
    P3: TEdit;
    P4: TEdit;
    P5: TEdit;
    P6: TEdit;
    P7: TEdit;
    P8: TEdit;
    P9: TEdit;
    P10: TEdit;
    P11: TEdit;
    P12: TEdit;
    eParcelas: TEdit;
    eDesconto: TEdit;
    eValor: TEdit;
    DBCheckBox2: TDBCheckBox;
    bSalvaConfg: TBitBtn;
    bDesconto: TBitBtn;
    C1: TCheckBox;
    C12: TCheckBox;
    C11: TCheckBox;
    C10: TCheckBox;
    C9: TCheckBox;
    C8: TCheckBox;
    C7: TCheckBox;
    C6: TCheckBox;
    C5: TCheckBox;
    C4: TCheckBox;
    C3: TCheckBox;
    C2: TCheckBox;
    cbapaga: TCheckBox;
    DBMemo1: TDBMemo;
    bLimpaTela: TBitBtn;
    bRetornaPed: TBitBtn;
    eOutras: TEdit;
    bImprimirRecibo: TBitBtn;
    cbGeraEntrada: TCheckBox;
    eEntrada: TEdit;
    DBEdit2: TDBEdit;
    Label19: TLabel;
    eDescVarejo: TEdit;
    Label18: TLabel;
    Label20: TLabel;
    dtpLancamento: TDateTimePicker;
    btn5: TSpeedButton;
    BitBtn4: TBitBtn;
    BitBtn5: TBitBtn;
    dbg2: TDBGrid;
    Label22: TLabel;
    eTotTitMarc: TEdit;
    Label21: TLabel;
    eTotOrcamento: TEdit;
    SpeedButton3: TSpeedButton;
    dsVendaVarejo: TDataSource;
    cdsVendaVarejo: TClientDataSet;
    cdsVendaVarejomarcado: TStringField;
    cdsVendaVarejod_lancamento: TDateField;
    cdsVendaVarejon_totliquido: TFMTBCDField;
    cdsVendaVarejoi_cdfatura: TIntegerField;
    cdsVendaVarejoi_cdorcamento: TIntegerField;
    cdsVendaVarejoi_cdparccliente: TIntegerField;
    dspVendaVarejo: TDataSetProvider;
    dspItemOrcamento: TDataSetProvider;
    cdsItemOrcamento: TClientDataSet;
    cdsCriaTitCanc: TClientDataSet;
    dspCriaTitCanc: TDataSetProvider;
    FDPhysPgDriverLink1: TFDPhysPgDriverLink;
    SQLConnection: TFDConnection;
    qryCriaParametro: TFDQuery;
    qryVerificaMes: TFDQuery;
    qryDiasVerificacao: TFDQuery;
    qryVendaVarejo: TFDQuery;
    qryBuscaFormaPagto: TFDQuery;
    qryCriaTitCanc: TFDQuery;
    qryItemOrcamento: TFDQuery;
    qryUpdate: TFDQuery;
    qryBuscaTitEnt: TFDQuery;
    qryEstoque: TFDQuery;
    qryInsertLog: TFDQuery;
    qryOpRequisicao: TFDQuery;
    qryUpdateItem: TFDQuery;
    qryItemPed: TFDQuery;
    qryFuncLanc: TFDQuery;
    qryInsertDesc: TFDQuery;
    qryPedidoVenda: TFDQuery;
    qryCalPedido: TFDQuery;
    qryTotComissao: TFDQuery;
    qryTitulo: TFDQuery;
    qryCriaTitulo: TFDQuery;
    qryPedido: TFDQuery;
    qryValorBoleto: TFDQuery;
    qryVendedor: TFDQuery;
    qryDeleta: TFDQuery;
    qryFechaGerencial: TFDQuery;
    qryTipoVend: TFDQuery;
    qryOrcamento: TFDQuery;
    qryEmpresa: TFDQuery;
    qryDespesa: TFDQuery;
    qryItemPedido: TFDQuery;
    qryRsTitulo: TFDQuery;
    qryCliente: TFDQuery;
    qryHistoricoCNAB: TFDQuery;
    qryParam: TFDQuery;
    qryPedVenda: TFDQuery;
    cdsPedidoc_prazo: TWideStringField;
    cdsPedidof_tppgto: TWideStringField;
    cdsPedidon_vlrentrada: TBCDField;
    cdsPedidon_vlrseparado: TBCDField;
    cdsPedidoc_observacao: TWideStringField;
    cdsPedidon_percdescpedido: TBCDField;
    cdsPedidon_vlrdesconto: TBCDField;
    cdsPedidon_vlrfaturamento: TBCDField;
    cdsPedidon_vlrsuframa: TBCDField;
    cdsPedidof_cobraboleto: TWideStringField;
    cdsPedidof_faturou: TWideStringField;
    cdsPedidof_cancelado: TWideStringField;
    qSQL: TFDQuery;
    qCMD: TFDQuery;
    qCItemPedido: TFDQuery;
    qRKit: TFDQuery;
    qMovimentaEstoque: TFDQuery;
    cdsRcItemPedidoi_qtvolumes: TSmallintField;
    cdsRcItemPedidon_despacess: TBCDField;
    cdsRcItemPedidoc_codigo: TWideStringField;
    cdsRcItemPedidoc_descricao: TWideStringField;
    cdsRcItemPedidoi_cditempedvenda: TIntegerField;
    cdsRcItemPedidoi_cdproduto: TIntegerField;
    cdsRcItemPedidoi_ctitem: TSmallintField;
    cdsRcItemPedidoc_cdunidade: TWideStringField;
    cdsRcItemPedidon_prcunitario: TBCDField;
    cdsRcItemPedidon_qtdeseparada: TBCDField;
    cdsRcItemPedidon_quantidade: TBCDField;
    cdsRcItemPedidon_percdesconto: TBCDField;
    cdsRcItemPedidoprecodesc: TBCDField;
    cdsRcItemPedidototal: TBCDField;
    cdsRcPedVendac_cnpj: TWideStringField;
    cdsRcPedVendanome_emp: TWideStringField;
    cdsRcPedVendaend_emp: TWideStringField;
    cdsRcPedVendaf_tpfrete: TWideStringField;
    cdsRcPedVendai_cdformapagto: TIntegerField;
    cdsRcPedVendac_descricao: TWideStringField;
    cdsRcPedVendai_cdempresa: TSmallintField;
    cdsRcPedVendac_observacao: TWideStringField;
    cdsRcPedVendai_nrpedido: TIntegerField;
    cdsRcPedVendad_cadastro: TSQLTimeStampField;
    cdsRcPedVendad_pedido: TSQLTimeStampField;
    cdsRcPedVendac_prazo: TWideStringField;
    cdsRcPedVendan_vlrdigitado: TBCDField;
    cdsRcPedVendai_cdcliente: TIntegerField;
    cdsRcPedVendai_cdtransp: TIntegerField;
    cdsRcPedVendai_cdlista_preco: TIntegerField;
    cdsRcPedVendai_cdlista_precoprom: TIntegerField;
    cdsRcPedVendai_qtvolumes: TSmallintField;
    cdsRcPedVendai_cdvendedor: TIntegerField;
    cdsRcPedVendan_vlrfaturamento: TBCDField;
    cdsRcPedVendaf_liberado: TWideStringField;
    cdsRcPedVendai_cdarea: TIntegerField;
    cdsRcPedVendarepresentante: TWideStringField;
    cdsRcClientei_cdcliente: TIntegerField;
    cdsRcClientec_nome: TWideStringField;
    cdsRcClientec_endereco: TWideStringField;
    cdsRcClientec_nomecidade: TWideStringField;
    cdsRcClientec_uf: TWideStringField;
    cdsRcClientec_bairro: TWideStringField;
    cdsRcClientec_inscrestadual: TWideStringField;
    cdsRcClientec_rgorgao: TWideStringField;
    cdsRcClientec_cep: TWideStringField;
    cdsRcClientec_cnpj: TWideStringField;
    cdsRcClientec_cpf: TWideStringField;
    cdsRcClientec_telefone: TWideStringField;
    cdsRcClientec_nomefantas: TWideStringField;
    cdsRcClientec_contato: TWideStringField;
    cdsRcClientei_cdcontafin: TIntegerField;
    cdsRcClientec_descricao: TWideStringField;
    cdsRcClientec_cdsuframa: TWideStringField;
    cdsRcClientec_classe: TWideStringField;
    cdsRcClientei_mediaatraso: TSmallintField;
    Label23: TLabel;
    eIPI: TEdit;
    cdsRcItemPedidon_valoripi: TBCDField;
    cdsRcPedVendan_vlripi: TBCDField;
    Label24: TLabel;
    eFrete: TEdit;
    cdsPedidon_vlripi: TBCDField;
    cdsRcPedVendan_vlrsuframa: TBCDField;
    eSuframa: TEdit;
    Label25: TLabel;
    cdsRcPedVendan_custofrete: TBCDField;
    cdsRcPedVendan_despacess: TBCDField;
    cdsRcPedVendadespesas: TBCDField;
    lblTipoFrete: TLabel;
    cdsRcPedVendan_vlricmssubst: TBCDField;
    eImpostos: TEdit;
    lblSubst: TLabel;
    cdsRcPedVendac_nome_transportadora: TWideStringField;
    cdsRcPedVendac_nome_vendedor: TWideStringField;
    cdsRcPedVendan_vlrdesconto: TBCDField;
    cdsRcPedVendan_credcliente: TBCDField;
    cdsRcPedVendan_percdescpedido: TBCDField;
    cdsRcPedVendan_vlrseparado: TBCDField;
    cdsRcTituloi_cdtitulo: TIntegerField;
    cdsRcTituloc_documento: TWideStringField;
    cdsRcTitulod_vencimento: TSQLTimeStampField;
    cdsRcTitulon_vrtitulo: TBCDField;
    cdsRcTituloc_descricao: TWideStringField;
    cdsRcTitulon_valorboleto: TBCDField;
    cdsRcItemPedidof_cobraboleto: TWideStringField;
    frxCSVExport1: TfrxCSVExport;
    frxDBPedVenda: TfrxDBDataset;
    frxGIFExport1: TfrxGIFExport;
    frxJPEGExport1: TfrxJPEGExport;
    frxPDFExport1: TfrxPDFExport;
    frxReport: TfrxReport;
    frxRTFExport1: TfrxRTFExport;
    frxSimpleTextExport1: TfrxSimpleTextExport;
    frxXMLExport1: TfrxXMLExport;
    frxDBCliente: TfrxDBDataset;
    frxDBDespesas: TfrxDBDataset;
    frxDBTipoVend: TfrxDBDataset;
    frxDBTitulo: TfrxDBDataset;
    frxDBItemPedido: TfrxDBDataset;
    cbRelAntigo: TCheckBox;
    lbTipoDoc: TLabel;
    qTipoDoc: TFDQuery;
    dsTipoDoc: TDataSource;
    qTipoDoci_cdtipotitulo: TIntegerField;
    qTipoDocc_sigla: TWideStringField;
    eTipoDoc: TDBLookupComboBox;
    qTipoDocc_descricao: TWideStringField;
    cdsMovimentaEstoque: TClientDataSet;
    dspMovimentaEstoque: TDataSetProvider;
    cdsPedidon_freteincluso: TBCDField;
    cdsPedidon_despesasadd: TBCDField;
    sdsAtualizaTitulo: TFDQuery;
    dspAtualizaTitulo: TDataSetProvider;
    cdsAtualizaTitulo: TSLClientDataSet;
    sdsContaFin: TFDQuery;
    dspContaFin: TDataSetProvider;
    cdsContaFin: TSLClientDataSet;
    chkProtestaTitulo: TCheckBox;
    procedure bGeraComplementoClick(Sender: TObject);
    procedure P1Exit(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SpeedButton1Click(Sender: TObject);
    procedure Limpa1Click(Sender: TObject);
    procedure Procura1Click(Sender: TObject);
    procedure Sair1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ePedidoEnter(Sender: TObject);
    procedure bSalvaConfgClick(Sender: TObject);
    procedure bDescontoClick(Sender: TObject);
    procedure bRetornaPedClick(Sender: TObject);
    procedure bImprimirReciboClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure bFechaGerClick(Sender: TObject);
    procedure ConfiguracaoChange(Sender: TObject);
    procedure eDataGenInicialExit(Sender: TObject);
    procedure bRelGenrencialClick(Sender: TObject);
    procedure eEntradaExit(Sender: TObject);
    procedure btn5Click(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
//    procedure BitBtn5Click(Sender: TObject);
    procedure dbg2DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure dbg2CellClick(Column: TColumn);
    procedure SpeedButton3Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure bImprimirReciboMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
  private
    { Private declarations }
    vClickMouse: Boolean;
    vFileReport: string;

//    ECF: TEcf;
//    Parametros : U_BematechDll.TParametros;
    vb_CupomAberto, ErroTef, vAtualiza: Boolean;
    vTotOrcCupom: Double;

    function ComissaoBaseLucro: Double;
    function BuscaNovosValoreSPED: Double;
    procedure ExcluiDescCom;
    function DespesasAcessorias: Double;
//    procedure GeraCupom;
//    procedure FinalizaCupom;
    procedure BloquearAcaoUsuario(Bloquea: Boolean);
    function AjustString(s: string; Tamanho: Integer; Carac_subs: string; Alig_Right: Boolean = True): string;
    procedure EnableCTRLALTDEL(YesNo: Boolean);
    function FunctionDetect(LibName, FuncName: string; var LibPointer: Pointer): Boolean;
    procedure BuscaVendaVarejo;
//    procedure CriaTituloCancelado(vlrTit : Double; idparceiro: Integer; cDoc : string);
//    procedure AtualizaOrcCupom(idOrcamento : Integer);
//    procedure CriaParametro;
    procedure SomaValores;
//    procedure LimpaFatura(idFatura: Integer);
    procedure Conectar;
    procedure AutoMontarDesmontarKit(numero_pedido: Integer; acao: string);
    procedure BaixarCaixas(numero_pedido: Integer; acao: string);
    procedure ValidarMediaAtrasoCliente;
  public
    procedure Busca;
    procedure GeraTitulos;
    procedure GeraComissao(vtitulo: Integer; vlrtitulo: Double; vtitdocumento: string; vlrml: Double);
    procedure AcertaPed;
    procedure ExcluiTitulos;
    procedure DesCancelaPedido;
    procedure CorrigeItens;
    procedure VerificaUltimoFechamento;
    function  BuscaSQLMes(mes, ano: string): string;
    procedure FechaRequisicao;
    procedure AbreRequisicao;
    procedure AtualizarArquivo(DirServer: string; Arquivo: string);
    function Coalesce(vValue,vIfNull: Variant): Variant;
    procedure AtualizaTituloProtesto(pTitulo: Integer);


  end;

var
  Form1: TForm1;
  parcelas: TStrings;
  vlrdesconto: Integer;
  vCria_Comissao_PremioLiqui : Boolean;
  vTotalParcela: Integer;
  vExecuta_ExcluiDescCom: Boolean;
  vDias_Verificacao: Integer;
  vMargemLucro: Boolean;
  vComissao_Liquidez, ProgPedVenda, vMascaraProduto: string;
  vPermissao_100: Boolean;
  vClick: Boolean = True;

implementation

uses Math, DateUtils, StrUtils;

{$R *.dfm}

procedure TForm1.FormCreate(Sender: TObject);
var
  path: string;
//  iDiskSize: Int64;
begin
//  SQLConnection.Connected := False;
  Configuracao.TabIndex := 0;
  path := Copy(ExtractFilePath(Application.ExeName),1,1);
//  iDiskSize := SysUtils.DiskSize(byte(path[1]) - $40);
  dtpLancamento.Date := Now();
  parcelas := TStringList.Create;

  if FileExists(ExtractFilePath(Application.ExeName) + 'cpf.ini') then
    eConfiguracao.Lines.LoadFromFile('cpf.ini')
  else
    ShowMessage('Arquivo de inicializa��o n�o foi encontrado!');

  vExecuta_ExcluiDescCom := False;
  vMargemLucro := False;

  if StrToIntDef(eConfiguracao.Lines.Values['Dias_Verificacao'], 0) > 0 then
    vDias_Verificacao := StrToInt(eConfiguracao.Lines.values['Dias_Verificacao'])
  else
    vDias_Verificacao := 180;

  vComissao_Liquidez := '';

  if eConfiguracao.Lines.Values['ApagaParcelas'] = 'N' then
    cbapaga.Checked := False
  else
    cbapaga.Checked := True;

  if eConfiguracao.Lines.values['Permite_100'] = 'N' then
    vPermissao_100 := False
  else
    vPermissao_100 := True;

  if not vPermissao_100 then
    bDesconto.Enabled := False;

  if eConfiguracao.Lines.values['IMPRIMECUPOM'] = 'S' then begin
    pcCpf.ActivePageIndex := 1;
    ts2.TabVisible := True;
    ts1.TabVisible := False;
  end
  else begin
    pcCpf.ActivePageIndex := 0;
    ts2.TabVisible := False;
    ts1.TabVisible := True;
  end;

  chkProtestaTitulo.checked := (eConfiguracao.Lines.Values['ProtestaBoleto'] = 'S');

end;

procedure TForm1.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  parcelas.Free;
end;

procedure TForm1.Busca;
var
  prazo: string;
  totparc, pr: Double;
  posicao: Integer;
  vparcela: string;
  vValorIPI: Double;
  tipo_frete: string;
  aux_frete: Double;
begin
  evalor.Text := '0,00';

  aux_frete := 0;
  tipo_frete := '';

  cdsPedido.Close;
  cdsPedido.Params.ParamByName('i_nrpedido').AsInteger := StrToInt(ePedido.Text);
  cdsPedido.Open;

  cdsRcPedVenda.Close;
  cdsRcPedVenda.Params.ParamByName('pedido').AsInteger := StrToInt(ePedido.Text);
  cdsRcPedVenda.Open;

  tipo_frete := cdsRcPedVendaf_tpfrete.AsString;

  if tipo_frete = 'Incluso' then begin
    aux_frete := cdsRcPedVendan_custofrete.AsFloat;
    lblTipoFrete.Caption := '(Frete Incluso)';
  end;

  if tipo_frete = 'Pago' then begin
    aux_frete := 0;
    lblTipoFrete.Caption := '(Frete Pago)';
  end;

  qCMD.Close;
  qCMD.SQL.Clear;
  qCMD.SQL.Add('select cast(sum(n_valoripi) as numeric(12,4)) as total_ipi from itempedvenda where i_nrpedido = :i_nrpedido');
  qCMD.ParamByName('i_nrpedido').AsInteger := StrToInt(ePedido.Text);
  qCMD.Open;

  vValorIPI := qCMD.FieldByName('total_ipi').AsFloat;

  eEntrada.Text := '0,00';
  eParcelas.Text := '';
  P1.Text := '';
  P2.Text := '';
  P3.Text := '';
  P4.Text := '';
  P5.Text := '';
  P6.Text := '';
  P7.Text := '';
  P8.Text := '';
  P9.Text := '';
  P10.Text := '';
  P11.Text := '';
  P12.Text := '';

  if eDesconto.Visible then
    vlrdesconto := StrToInt(eDesconto.Text)
  else
    vlrdesconto := 100;

  if cdsPedido.RecordCount > 0 then begin
    cdsTitulo.Close;
    cdsTitulo.CommandText :=
      ' select ' +
      '   i_cdtitulo, ' +
      '   c_documento, ' +
      '   d_emissao, ' +
      '   d_vencimento, ' +
      '   n_vracerto, ' +
      '   n_vrtitulo, ' +
      '   f_status ' +
      ' from ' +
      '   titulo ' +
      ' where ' +
      '   i_nrpedido is null ' +
      ' and c_documento like :c_documento ' +
      ' and i_cdparceiro = :i_cdparceiro ' +
      ' and i_cdempresa = :i_cdempresa ';

    if cdsPedidoi_fatura.AsInteger > 0 then
      cdsTitulo.Params.ParamByName('c_documento').AsString := Trim(cdsPedidoi_fatura.AsString) + '-%'
    else
      cdsTitulo.Params.ParamByName('c_documento').AsString := Trim(ePedido.Text) + '-%';

    cdsTitulo.Params.ParamByName('i_cdparceiro').AsInteger := StrToInt(cdsPedidoi_cdcliente.AsString + '1');
    cdsTitulo.Params.ParamByName('i_cdempresa').AsInteger := cdsPedidoi_cdempresa.AsInteger;
    cdsTitulo.Open;

//    cdsVendedor.Close;
//    cdsVendedor.CommandText :=
//      ' select ' +
//      '   pv.I_CdVendedor, ' +
//      '   v.f_vendedor, ' +
//      '   v.i_cdparceiro, ' +
//      '   v.f_vendedor ' +
//      ' from ' +
//      '   PedVenda_Vendedor pv ' +
//      ' join vendedor  v on v.i_cdvendedor = pv.i_cdvendedor ' +
//      ' where ' +
//      '   I_NrPedido = :I_NrPedido';
//    cdsVendedor.Params.ParamByName('i_nrpedido').AsInteger := cdsPedidoi_nrpedido.AsInteger;
//    cdsVendedor.Open;

    cdsVendedor.Close;
    cdsVendedor.CommandText :=
      ' select ' +
      '   av.I_CdVendedor, ' +
      '   v.f_vendedor, ' +
      '   v.i_cdparceiro, ' +
      '   v.f_vendedor ' +
      ' from ' +
      '   pedidovenda pv ' +
      ' join cliente cli on cli.i_cdcliente = pv.i_cdcliente '  +
      ' join area_vendedor  av on av.i_cdarea = cli.i_cdarea ' +
      ' join vendedor  v on v.i_cdvendedor = av.i_cdvendedor ' +
      ' where ' +
      '   I_NrPedido = :I_NrPedido and ( f_vendedor = ''R'' )';
    cdsVendedor.Params.ParamByName('i_nrpedido').AsInteger := cdsPedidoi_nrpedido.AsInteger;
    cdsVendedor.Open;

    if not (cdsVendedor.RecordCount > 0) then
    begin
      MessageDlg('Representante da Area do Pedido n�o informado.'+
      #13+
      'Favor voltar ao Age, ir na �rea de Venda e adicionar o Representante da �rea do Cliente deste Pedido. ', mtWarning, [mbOK], 0);
    end;

    eValor.Text := StringReplace(eValor.Text, '.', '', [rfReplaceAll]);

    if (eValor.Text = '0,00') or (StrToFloat(eValor.Text) = 0) then begin
      eValor.Text := FormatFloat('#,##0.00',
        (
          cdsPedidon_vlrseparado.AsFloat -
          cdsPedidon_vlrdesconto.AsFloat +
          vValorIPI +
          aux_frete
         // - cdsRcPedVendan_vlrsuframa.AsFloat
        ) * (vlrdesconto / 100)
      );
    end;

    eIPI.Text := FormatFloat('#,##0.00', vValorIPI);
    eFrete.Text := FormatFloat('#,##0.00', cdsRcPedVendan_custofrete.AsFloat);

//  Busca parcelamento
    prazo := cdsPedidoc_prazo.AsString;
    totparc := Length(cdsPedidoc_prazo.AsString) / 3;

    if totparc > 0 then begin
      //parcela 1
      posicao := 1;
      vparcela := Copy(prazo, posicao, 3);
      eParcelas.Text := vparcela;
      C1.Checked := (totparc = 1);

      if vparcela <> '' then begin
        if (vlrdesconto = 100) then
          C1.Checked := True;

        pr := StrToInt(vparcela);
        vparcela := FloatToStr(pr);

        while Length(vparcela) < 3 do
          vparcela := '0' + vparcela;

        p1.Text := vparcela;
      end;

      //parcela 2
      posicao := posicao + 3;
      vparcela := Copy(prazo, posicao, 3);
      C2.Checked := (totparc = 2);

      if vparcela <> '' then begin
        if (vlrdesconto = 100) then
          C2.Checked := True;

        eParcelas.Text := eParcelas.Text + ' / ' + vparcela;
        pr := StrToInt(vparcela);
        vparcela := FloatToStr(pr);

        while Length(vparcela) < 3 do
          vparcela := '0' + vparcela;

        p2.Text := vparcela;
      end;

      //parcela 3
      posicao := posicao + 3;
      vparcela := Copy(prazo, posicao, 3);
      C3.Checked := ((totparc >= 3) and (totparc <= 4));

      if vparcela <> '' then begin
        if (vlrdesconto = 100) then
          C3.Checked := True;

        eParcelas.Text := eParcelas.Text + ' / ' + vparcela;
        pr := StrToInt(vparcela);
        vparcela := FloatToStr(pr);

        while Length(vparcela) < 3 do
          vparcela := '0' + vparcela;

        p3.Text := vparcela;
      end;

      //parcela 4
      posicao := posicao + 3;
      vparcela := Copy(prazo, posicao, 3);
      C4.Checked := ((totparc >= 4) and (totparc <= 6));

      if vparcela <> '' then begin
        if (vlrdesconto = 100) then
          C4.Checked := True;

        eParcelas.Text := eParcelas.Text + ' / ' + vparcela;
        pr := StrToInt(vparcela);
        vparcela := FloatToStr(pr);

        while Length(vparcela) < 3 do
          vparcela := '0' + vparcela;

        p4.Text := vparcela;
      end;

      //parcela 5
      posicao := posicao + 3;
      vparcela := Copy(prazo, posicao, 3);
      C5.Checked := ((totparc >= 5) and (totparc <= 8));

      if vparcela <> '' then begin
        if (vlrdesconto = 100) then
          C5.Checked := True;

        eParcelas.Text := eParcelas.Text + ' / ' + vparcela;
        pr := StrToInt(vparcela);
        vparcela := FloatToStr(pr);

        while Length(vparcela) < 3 do
          vparcela := '0' + vparcela;

        p5.Text := vparcela;
      end;

      //parcela 6
      posicao := posicao + 3;
      vparcela := Copy(prazo, posicao, 3);
      C6.Checked := ((totparc >= 6) and (totparc <= 10));

      if vparcela <> '' then begin
        if (vlrdesconto = 100) then
          C6.Checked := True;

        eParcelas.Text := eParcelas.Text + ' / ' + vparcela;
        pr := StrToInt(vparcela);
        vparcela := FloatToStr(pr);

        while Length(vparcela) < 3 do
          vparcela := '0' + vparcela;

        p6.Text := vparcela;
      end;

      //parcela 7
      posicao := posicao + 3;
      vparcela := Copy(prazo, posicao, 3);
      C7.Checked := ((totparc >= 7) and (totparc <= 12));

      if vparcela <> '' then begin
        if (vlrdesconto = 100) then
          C7.Checked := True;

        eParcelas.Text := eParcelas.Text + ' / ' + vparcela;
        pr := StrToInt(vparcela);
        vparcela := FloatToStr(pr);

        while Length(vparcela) < 3 do
          vparcela := '0' + vparcela;

        p7.Text := vparcela;
      end;

      //parcela 8
      posicao := posicao + 3;
      vparcela := Copy(prazo, posicao, 3);
      C8.Checked := ((totparc >= 8) and (totparc <= 12));

      if vparcela <> '' then begin
        if (vlrdesconto = 100) then
         C8.Checked := True;

        eParcelas.Text := eParcelas.Text + ' / ' + vparcela;
        pr := StrToInt(vparcela);
        vparcela := FloatToStr(pr);

        while Length(vparcela) < 3 do
          vparcela := '0' + vparcela;

        p8.Text := vparcela;
      end;

      //parcela 9
      posicao := posicao + 3;
      vparcela := Copy(prazo, posicao, 3);
      C9.Checked := ((totparc >= 9) and (totparc <= 12));

      if vparcela <> '' then begin
        if (vlrdesconto = 100) then
          C9.Checked := True;

        eParcelas.Text := eParcelas.Text + ' / ' + vparcela;
        pr := StrToInt(vparcela);
        vparcela := FloatToStr(pr);

        while Length(vparcela) < 3 do
          vparcela := '0' + vparcela;

        p9.Text := vparcela;
      end;

      //parcela 10
      posicao := posicao + 3;
      vparcela := Copy(prazo, posicao, 3);
      C10.Checked := ((totparc >= 10) and (totparc <= 12));

      if vparcela <> '' then begin
        if (vlrdesconto = 100) then
          C10.Checked := True;

        eParcelas.Text := eParcelas.Text + ' / ' + vparcela;
        pr := StrToInt(vparcela);
        vparcela := FloatToStr(pr);

        while Length(vparcela) < 3 do
          vparcela := '0' + vparcela;

        p10.Text := vparcela;
      end;

      //parcela 11
      posicao := posicao + 3;
      vparcela := Copy(prazo, posicao, 3);
      C11.Checked := ((totparc >= 11) and (totparc <= 12));

      if vparcela <> '' then begin
        if (vlrdesconto = 100) then
         C11.Checked := True;

        eParcelas.Text := eParcelas.Text + ' / ' + vparcela;
        pr := StrToInt(vparcela);
        vparcela := FloatToStr(pr);

        while Length(vparcela) < 3 do
          vparcela := '0' + vparcela;

        p11.Text := vparcela;
      end;

      //parcela 12
      posicao := posicao + 3;
      vparcela := Copy(prazo, posicao, 3);
      C12.Checked := ((totparc >= 12) and (totparc <= 12));

      if vparcela <> '' then begin
        if (vlrdesconto = 100) then
          C12.Checked := True;

        eParcelas.Text := eParcelas.Text + ' / ' + vparcela;
        pr := StrToInt(vparcela);
        vparcela := FloatToStr(pr);

        while Length(vparcela) < 3 do
          vparcela := '0' + vparcela;

        p12.Text := vparcela;
      end;
    end;
  end;

  eOutras.Text := FormatFloat('0.00', cdsRcPedVendan_despacess.AsFloat);
  eImpostos.Text := FormatFloat('0.00', cdsRcPedVendan_vlricmssubst.AsFloat);
  eSuframa.Text := FormatFloat('0.00', cdsRcPedVendan_vlrsuframa.AsFloat);

  if ProgPedVenda = '' then begin
    cdsUpdate.Close;
    cdsUpdate.CommandText := 'select RetParametro(''PROGPEDVENDA'') as progpedvenda';
    cdsUpdate.Open;

    ProgPedVenda := cdsUpdate.FieldByName('progpedvenda').AsString;
  end;
end;

procedure TForm1.bGeraComplementoClick(Sender: TObject);
var
  i: Integer;
  vpt: Integer;
  totparcelas: Integer;

  vRegClass: string;
  vProdCustoMaior: string;
  vPrc_Custo_Produto: string;
  _AutoMontagemKit: string;
  vS_ICdTitulo: string;
begin
  vS_ICdTitulo := '';
  if not cdsPedido.Active then
    Exit;

  if cdsTitulo.RecordCount > 0  then
  begin
    ShowMessage('Titulos j� gerados!');
    Exit;
    end;


  for i := 1 to Length(eDesconto.Text) do begin
    if not (eDesconto.Text[i] in ['0'..'9']) then begin
      ShowMessage('Car�cter inv�lido no desconto');
      Exit;
    end;
  end;

  for i := 1 to Length(eOutras.Text) do begin
    if not (eOutras.Text[i] in ['0'..'9',',']) then begin
      ShowMessage('Car�cter inv�lido em outras despesas!');
      Exit;
    end;
  end;

  if (vlrdesconto <= 0) or (vlrdesconto > 100) then begin
    ShowMessage('Desconto n�o pode ser menor que 1 ou maior que 100%');
    Exit;
  end;

  if cbGeraEntrada.Visible and cbGeraEntrada.Checked and (cdsPedidon_vlrentrada.AsFloat = 0 ) then begin
    ShowMessage('O valor da entrada tem que ser maior que zero!');
    Exit;
  end;

  if eDesconto.Visible then
    vlrdesconto := StrToInt(eDesconto.Text)
  else
    vlrdesconto := 100;

  if cdsPedidof_cancelado.AsString = 'S' then begin
    MessageDlg('J� cancelado!', mtError, [mbOk], 0);
    Exit;
  end;

  if cdsPedidof_faturou.AsString = 'S' then begin
    MessageDlg('Aten��o, j� foi faturado!', mtError, [mbOk], 0);
    Exit;
  end;

  if not(vPermissao_100) and (vlrdesconto = 100) then begin
    MessageDlg('Desconto n�o permitido!', mtError, [mbOK], 0);
    Exit;
  end;

  if (StrToFloatDef(eEntrada.Text,0) > 0) then begin
    if (StrToFloatDef(eEntrada.Text,0) >= ((cdsPedidon_vlrseparado.AsFloat - cdsPedidon_vlrdesconto.AsFloat) * (vlrdesconto/100))) then begin
      MessageDlg('Valor da Entrada deve ser menor que o Valor Gerado!', mtError, [mbOK], 0);
      Exit;
    end;

    if (StrToFloatDef(eEntrada.Text,0) > cdsPedidon_vlrentrada.AsFloat) then begin
      MessageDlg('Valor da Entrada n�o pode ser maior que Entrada do Pedido!', mtError, [mbOK], 0);
      Exit;
    end;
  end;

  ValidarMediaAtrasoCliente;

  cdsUpdate.Close;
  cdsUpdate.CommandText := ' select calculavalorespedido(:I_NrPedido) ';
  cdsUpdate.Params.ParamByName('I_NrPedido').AsInteger := cdsPedidoi_nrpedido.AsInteger;
  cdsUpdate.Open;

  cdsUpdate.Close;
  cdsUpdate.CommandText := 'select RetParametro(''PROGPEDVENDA'') as progpedvenda';
  cdsUpdate.Open;

  ProgPedVenda := cdsUpdate.FieldByName('progpedvenda').AsString;

  if cdsUpdate.FieldByName('progpedvenda').AsString = 'CADPEDIDOVENDABALCAO' then begin
    if cdsPedidoi_fatura.AsInteger = 0 then begin
      cdsUpdate.Close;
      cdsUpdate.CommandText := 'update pedidovenda set i_fatura = (select max(i_fatura) + 1 from pedidovenda) where i_nrpedido = :NrPedido';
      cdsUpdate.Params.ParamByName('NrPedido').AsInteger := cdsPedidoi_nrpedido.AsInteger;
      cdsUpdate.Execute;

      cdsUpdate.Close;
      cdsUpdate.CommandText := 'select i_fatura from pedidovenda where i_nrpedido = :pedido ';
      cdsUpdate.Params.ParamByName('pedido').AsInteger := cdsPedidoi_nrpedido.AsInteger;
      cdsUpdate.Open;
    end
    else begin
      cdsUpdate.Close;
      cdsUpdate.CommandText := 'select i_fatura from pedidovenda where i_nrpedido = :pedido ';
      cdsUpdate.Params.ParamByName('pedido').AsInteger := cdsPedidoi_nrpedido.AsInteger;
      cdsUpdate.Open;
    end;
  end;

  vpt := 1;
  totparcelas := 0;

  while (TEdit(FindComponent('P' + IntToStr(vpt))).Text <> '') and (vpt <= 12) and (totparcelas = 0 ) do begin
    if (TCheckBox(FindComponent('C' + IntToStr(vpt))).Checked) then
      Inc(totparcelas);
    Inc(vpt);
  end;

  if totparcelas = 0 then begin
    MessageDlg('Aten��o, Marque as parcelas a serem geradas!', mtError, [mbOk], 0);
    Exit;
  end;

  vPrc_Custo_Produto := eConfiguracao.Lines.values['PRC_CUSTO_PRODUTO'];

  cdsItemPed.Close;
  cdsItemPed.CommandText :=
    ' select ' +
    '   case ''' + vPrc_Custo_Produto + ''' when ''L'' then ' +
    '     prc.n_precocusto ' +
    '   else ' +
    '     prod.n_ultimoprecocompra ' +
    '   end as n_precocusto, ' +
    '   ipv.n_prcvenda, ' +
    '   ipv.n_quantidade, ' +
    '   prod.c_codigo ' +
    ' from ' +
    '   itempedvenda ipv ' +
    ' join produto prod using(i_cdproduto) ' +
    ' join preco prc on prc.i_cdproduto = prod.i_cdproduto and prc.i_cdlista_preco = ipv.i_cdlista_preco ' +
    ' where ' +
    '   ipv.i_nrpedido = :I_NrPedido ' +
    ' and ipv.f_bonifica = ''N'' ';
  cdsItemPed.Params.parambyname('I_NrPedido').asinteger := StrToInt(ePedido.Text);
  cdsItemPed.Open;

  if cdsItemPed.RecordCount > 0 then begin
    vProdCustoMaior := '';
    cdsItemPed.First;

    while not cdsItemPed.Eof do begin
      if
        (StrToIntDef(eConfiguracao.Lines.values['Margem_Min_Lucro'],0) > 0)
        and
        (cdsItemPed.FieldByName('n_precocusto').AsFloat * (1 + ((StrToIntDef(eConfiguracao.Lines.values['Margem_Min_Lucro'],0)/100))) > (cdsItemPed.FieldByName('n_prcvenda').AsFloat))
      then begin
        vProdCustoMaior := vProdCustoMaior + cdsItemPed.FieldByName('c_codigo').AsString + ', ';
        cdsItemPed.Next;
      end
      else if (cdsItemPed.FieldByName('n_precocusto').AsFloat > (cdsItemPed.FieldByName('n_prcvenda').AsFloat * (1 - (vlrdesconto/100)))) and (vlrdesconto <> 100) then begin
        MessageDlg('Existe(m) produto(s) no pedido que ficar�(�o) com pre�o de venda menor que pre�o de custo.', mtInformation, [mbOK], 0);
        cdsItemPed.Last;
        vMargemLucro := True;
      end
      else
        cdsItemPed.Next;
    end;

    if vProdCustoMaior <> '' then begin
      MessageDlg(
        'Produto(s) ' + vProdCustoMaior + ' com pre�o de custo maior que pre�o de venda.' + #13 +
        'Altere o pre�o de venda do(s) produto(s) para realizar esta venda. ',
        mtError,
        [mbOK],
        0
      );

      Abort;
    end;
  end;

  if MessageDlg('Confirma a Gera��o?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then begin
    GeraTitulos;

    cdsTitulo.Close;
    cdsTitulo.CommandText :=
      ' select ' +
      '   i_cdtitulo, ' +
      '   c_documento, ' +
      '   d_emissao, ' +
      '   d_vencimento, ' +
      '   n_vracerto, ' +
      '   n_vrtitulo, ' +
      '   f_status ' +
      ' from ' +
      '   titulo ' +
      ' where ' +
      '   i_nrpedido is null ' +
      ' and c_documento like :c_documento ' +
      ' and i_cdparceiro = :i_cdparceiro ' +
      ' and i_cdempresa = :i_cdempresa ';

    if cdsPedidoi_fatura.AsInteger > 0 then
      cdsTitulo.Params.ParamByName('c_documento').AsString := Trim(cdsPedidoi_fatura.AsString) + '%';

    cdsTitulo.Params.ParamByName('i_cdparceiro').AsInteger := StrToInt(cdsPedidoi_cdcliente.AsString + '1');
    cdsTitulo.Params.ParamByName('i_cdempresa').AsInteger := cdsPedidoi_cdempresa.AsInteger;
    cdsTitulo.Open;

    if cdsTitulo.RecordCount <= 0 then begin
      cdsTitulo.Close;
      cdsTitulo.Params.ParamByName('c_documento').AsString := Trim(ePedido.Text) + '%';
      cdsTitulo.Params.ParamByName('i_cdparceiro').AsInteger := StrToInt(cdsPedidoi_cdcliente.AsString + '1');
      cdsTitulo.Params.ParamByName('i_cdempresa').AsInteger := cdsPedidoi_cdempresa.AsInteger;
      cdsTitulo.Open;
    end;

    MessageDlg('Conclu�do com sucesso!', mtInformation, [mbOk], 0);
  end;

  // Corre��o para solicita��o 10043
  qSQL.Close;
  qSQL.SQL.Clear;
  qSQL.SQL.Add('select');
  qSQL.SQL.Add('  c_valor');
  qSQL.SQL.Add('from');
  qSQL.SQL.Add('  parametro');
  qSQL.SQL.Add('where');
  qSQL.SQL.Add('  c_nome = ''AUTOMONTAGEMKIT'' ');
  qSQL.Open;

  if qSQL.RecordCount > 0 then begin
    _AutoMontagemKit := qSQL.FieldByName('c_valor').AsString;

    if _AutoMontagemKit = 'S' then
      AutoMontarDesmontarKit(StrToInt(ePedido.Text), 'M');
  end;

  BaixarCaixas(StrToInt(ePedido.Text), 'B');
  //

  cdsUpdate.Close;
  cdsUpdate.CommandText := ' select calculavalorespedido(:I_NrPedido) ';
  cdsUpdate.Params.ParamByName('I_NrPedido').AsInteger := cdsPedidoi_nrpedido.AsInteger;
  cdsUpdate.Open;

  //Pedido passado 100% o pedido ficar� marcado como Conferido.
  if not(eDesconto.Visible) then
  begin
    cdsUpdate.Close;
    cdsUpdate.CommandText := 'update pedidovenda set f_conferido = ''S'' where i_nrpedido =:I_NrPedido ';
    cdsUpdate.Params.ParamByName('I_NrPedido').AsInteger := cdsPedidoi_nrpedido.AsInteger;
    cdsUpdate.Execute;
  end;

  cdsPedido.Close;

  // Solic. 11166
  if (eConfiguracao.Lines.Values['ATUALIZARDATAULTCOMPRACLIENTE'] = 'S') and (eDesconto.Text = '100') then begin
    try
      qSQL.Close;
      qSQL.SQL.Clear;
      qSQL.SQL.Add('update cliente set');
      qSQL.SQL.Add('  d_ultcompra = now()');
      qSQL.SQL.Add('where');
      qSQL.SQL.Add('  i_cdcliente in (');
      qSQL.SQL.Add('    select');
      qSQL.SQL.Add('      i_cdcliente');
      qSQL.SQL.Add('    from');
      qSQL.SQL.Add('      pedidovenda');
      qSQL.SQL.Add('    where');
      qSQL.SQL.Add('      i_nrpedido = :i_nrpedido');
      qSQL.SQL.Add('  )');
      qSQL.ParamByName('i_nrpedido').AsInteger := StrToInt(ePedido.Text);
      qSQL.ExecSQL;
    except
      on e: Exception do begin
        ShowMessage('Erro ao atualizar data de �ltima compra do cliente.! ' + e.Message);
        Abort;
      end;
    end;
  end;
  //

  Busca;

  // Solic. 11166
  if eTipoDoc.KeyValue > 0 then begin
    cdsTitulo.DisableControls;
    cdsTitulo.First;
    while not(cdsTitulo.Eof) do begin
      if cdsTitulo.RecNo = 1 then
        vS_ICdTitulo := cdsTitulo.FieldByName('i_cdtitulo').AsString
      else
        vS_ICdTitulo := vS_ICdTitulo + ', ' + cdsTitulo.FieldByName('i_cdtitulo').AsString;
      cdsTitulo.Next;
    end;
    cdsTitulo.First;
    cdsTitulo.EnableControls;

    qSQL.Close;
    qSQL.SQL.Clear;
    qSQL.SQL.Add('update titulo set');
    qSQL.SQL.Add('  i_cdtipotitulo = ' + IntToStr(eTipoDoc.KeyValue));
    qSQL.SQL.Add('where i_cdtitulo in ( ' + vS_ICdTitulo + ' )');
    qSQL.ExecSQL;
  end;
  //
end;

procedure TForm1.P1Exit(Sender: TObject);
begin
  if TEdit(Sender).Text = '' then
    bGeraComplemento.SetFocus;
end;

procedure TForm1.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_RETURN then begin
    if Shift = [ssShift] then // 2
      SelectNext(ActiveControl, False, True)
    else // 1
      SelectNext(ActiveControl, True, True);
  end;
end;

procedure TForm1.SpeedButton1Click(Sender: TObject);
begin
  if (StrToIntDef(eConfiguracao.Lines.values['Margem_Min_Lucro'],0) > 0) and (eConfiguracao.Lines.Values['MARGEMLUCRO'] = 'S') then begin
    MessageDlg('N�o pode existir 2 par�metros alimentados "Margem_Min_Lucro" e "MARGEMLUCRO".', mtInformation, [mbOK], 0);
    Exit;
  end;

  if
    (StrToIntDef(eConfiguracao.Lines.values['Margem_Min_Lucro'],0) > 0) and
    (eConfiguracao.Lines.Values['PRC_CUSTO_PRODUTO'] <> 'L') and
    (eConfiguracao.Lines.Values['PRC_CUSTO_PRODUTO'] <> 'P')
  then begin
    MessageDlg('Falta informar o par�metro "PRC_CUSTO_PRODUTO"!', mtInformation, [mbOK], 0);
    Exit;
  end;

  Busca;
end;

procedure TForm1.Limpa1Click(Sender: TObject);
begin
  cdsPedido.Close;
  cdsTitulo.Close;

  ePedido.SetFocus;

  eEntrada.Text := '0,00';
  ePedido.Text := '';
  eParcelas.Text := '';
  evalor.Text := '0,00';
  eOutras.Text := '0,00';
  eImpostos.Text := '0,00';
  eFrete.Text := '0,00';
  eIPI.Text := '0,00';


  c1.Checked := False;
  c2.Checked := False;
  c3.Checked := False;
  c4.Checked := False;
  c5.Checked := False;
  c6.Checked := False;
  c7.Checked := False;
  c8.Checked := False;
  c9.Checked := False;
  c10.Checked := False;
  c11.Checked := False;
  c12.Checked := False;

  P1.Text := '';
  P2.Text := '';
  P3.Text := '';
  P4.Text := '';
  P5.Text := '';
  P6.Text := '';
  P7.Text := '';
  P8.Text := '';
  P9.Text := '';
  P10.Text := '';
  P11.Text := '';
  P12.Text := '';
end;

procedure TForm1.Procura1Click(Sender: TObject);
begin
  Busca;
end;

procedure TForm1.Sair1Click(Sender: TObject);
begin
  Close;
end;

procedure TForm1.GeraTitulos;
var
  vpt: Integer;
  totparcelas: Integer;
  vi_cdtitulo: Integer;
  vcdvendedor: Integer;
  vCdContaFin: Integer;

  valor: Double;
  vlrml: Double;
  vlrreal: Double;
  vlrEntrada: Double;
  vlrparcelas: Double;
  vlrultparce: Double;
  valorboleto: Double;
  vlrcomissao: Double;

  vTipo: string;
  vFormaPagto: string;
begin
  valor := BuscaNovosValoreSPED;

  if StrToFloatDef(eEntrada.Text,0) > 0 then
    valor := valor - StrToFloatDef(eEntrada.Text, 0);

  vlrreal := valor;
  valor := valor; // ?????

  if cbGeraEntrada.Visible and cbGeraEntrada.Checked and (cdsPedidon_vlrentrada.AsFloat > 0) then begin
    vlrreal := vlrreal - cdsPedidon_vlrentrada.AsFloat;
    valor := valor - cdsPedidon_vlrentrada.AsFloat;
  end;

  cdsVendedor.First;
  while (cdsVendedor.FieldByName('f_vendedor').AsString <> 'R') and
        not(cdsVendedor.Eof) do
    cdsVendedor.Next;

  vcdvendedor := cdsVendedor.FieldByName('i_cdvendedor').AsInteger;

  if cdsvendedor.Eof then begin
    MessageDlg('Vendedor n�o foi informado no Pedido!', mtError, [mbOk], 0);
    Abort;
  end;

  //caso nao informe o tipoconta este ser� inforamdo o tipoconta da forma de pagamento do pedido
  if eConfiguracao.Lines.Values['tipoconta'] <> '' then
    vtipo := eConfiguracao.Lines.Values['tipoconta'];

  if eConfiguracao.Lines.Values['contafin'] = '' then begin
    MessageDlg('contafin foi informado no cpf.ini!', mtError, [mbOk], 0);
    Abort;
  end;

  vCdContaFin := StrToInt(eConfiguracao.Lines.Values['contafin']);

  parcelas.Clear;
  vpt := 1;

  while (TEdit(FindComponent('P' + IntToStr(vpt))).Text <> '') and (vpt <= 12) do begin
    if (TCheckBox(FindComponent('C' + IntToStr(vpt))).Checked) then
      parcelas.Add(TEdit(FindComponent('P' + IntToStr(vpt))).Text);
    Inc(vpt);
  end;

  totparcelas := parcelas.Count;
  vTotalParcela := totparcelas;
  vlrparcelas := (valor / totparcelas);
  vlrultparce := valor - (vlrparcelas * (totparcelas - 1));

  if (totparcelas > 0) and (vlrreal > 0) then
    vlrcomissao := vlrreal / totparcelas
  else
    vlrcomissao := 0;

  if eConfiguracao.Lines.Values['MARGEMLUCRO'] = 'S' then begin
    cdsValorPedido.Close;
    cdsValorPedido.CommandText :=
      ' select ' +
      '   sum(Round(ipv.n_qtdeseparada * ipv.n_prcunitario * (ipv.n_percdesconto - pv.n_reducaomargemlucro)/100,4)) as vlrml ' +
      ' from ' +
      '   pedidovenda pv ' +
      ' join itempedvenda ipv using(i_nrpedido) ' +
      ' where ' +
      '   ipv.i_nrpedido = :Pedido ' +
      ' and ipv.f_bonifica = ''N'' ';
    cdsValorPedido.Params.ParamByName('Pedido').AsInteger := cdsPedidoi_nrpedido.AsInteger;
    cdsValorPedido.Open;

    vlrml := (cdsValorPedido.FieldByName('vlrml').AsFloat / vlrreal * vlrcomissao) * (1-((100 - vlrdesconto)/100));
  end
  else
    vlrml := 0;

  if cdsPedidof_cobraboleto.Value = 'S' then begin
    cdsValorPedido.Close;
    cdsValorPedido.CommandText := ' select N_ValorBoleto from ContaFinanceira where I_CdContaFin = :I_CdContaFin ';
    cdsValorPedido.Params.ParamByName('i_cdcontafin').AsInteger := vCdContaFin;
    cdsValorPedido.Open;

    valorboleto := cdsValorPedido.FieldByName('n_valorboleto').AsFloat;
  end
  else
    valorboleto := 0;

  if not SQLConnection.InTransaction then begin
    SQLConnection.StartTransaction;

    try
      cdsBuscaFormaPagto.Close;
      cdsBuscaFormaPagto.CommandText := ' select c_descricao, i_cdtipotitulo from formapagto where i_cdformapagto = :FormaPagto ';
      cdsBuscaFormaPagto.Params.ParamByName('FormaPagto').AsInteger := cdsPedidoi_cdformapagto.AsInteger;
      cdsBuscaFormaPagto.Open;

      vFormaPagto := cdsBuscaFormaPagto.FieldByName('c_descricao').AsString;

      if vTipo = '' then
        vTipo := cdsBuscaFormaPagto.FieldByName('i_cdtipotitulo').AsString;

      if vTipo = '' then
        vTipo := 'B';

      if (cbGeraEntrada.Visible and cbGeraEntrada.Checked and (cdsPedidon_vlrentrada.AsFloat > 0 )) or (StrToFloatDef(eEntrada.Text,0) > 0) then begin
        if StrToFloatDef(eEntrada.Text,0) > 0 then
          vlrEntrada := StrToFloatDef(eEntrada.Text,0)
        else
          vlrEntrada := cdsPedidon_vlrentrada.AsFloat;

        cdsCriaTitulo.Close;
        cdsCriaTitulo.CommandText := 'select CriaTituloRecebimento(';

        if vTipo = 'B' then
          cdsCriaTitulo.CommandText := cdsCriaTitulo.CommandText + QuotedStr(vTipo) + '::varchar, '
        else
          cdsCriaTitulo.CommandText := cdsCriaTitulo.CommandText + vTipo + '::varchar, ';

        cdsCriaTitulo.CommandText := cdsCriaTitulo.CommandText +
          IntToStr(vCdContaFin) + ', ' +
          Trim(cdsPedidoi_cdcliente.AsString + '1') + ', ' +
          'date(' + QuotedStr(DateToStr(Now)) + '), ' +
          'cast' + '(' + StringReplace(FloatToStr(vlrEntrada), ',', '.', [rfReplaceAll]) + ' as numeric) ' + ', ';

        if ProgPedVenda = 'CADPEDIDOVENDABALCAO' then begin
          if (cdsUpdate.FieldByName('i_fatura').AsInteger > 0) then begin
            cdsCriaTitulo.CommandText := cdsCriaTitulo.CommandText +
              QuotedStr(cdsUpdate.FieldByName('i_fatura').AsString + '-0');
          end
          else begin
            cdsCriaTitulo.CommandText := cdsCriaTitulo.CommandText +
              QuotedStr(cdsPedidoi_nrpedido.AsString + '-0');
          end;
        end
        else begin
          cdsCriaTitulo.CommandText := cdsCriaTitulo.CommandText +
            QuotedStr(cdsPedidoi_nrpedido.AsString + '-0');
        end;

        cdsCriaTitulo.CommandText :=
          cdsCriaTitulo.CommandText +
          '::character varying, ' +
          IntToStr(vcdvendedor) + ', ' +
          'cast' + '(' + StringReplace(FloatToStr(vlrEntrada), ',', '.', [rfReplaceAll]) + ' as numeric) ' + ', ' +
          IntToStr(cdsPedidoi_cdempresa.AsInteger) +
          '::smallint) as id_titulo';

        cdsCriaTitulo.Open;

        vi_cdtitulo := cdsCriaTitulo.FieldByName('id_titulo').AsInteger;

        //Caso tenha o parametro Dias_Protesto e esteja maior que zero ser� atualizado o titulo com o dia para protesto
        AtualizaTituloProtesto(vi_cdtitulo);


        if ProgPedVenda = 'CADPEDIDOVENDABALCAO' then begin
          if cdsUpdate.FieldByName('i_fatura').AsInteger > 0 then
            GeraComissao(vi_cdtitulo, vlrEntrada, cdsUpdate.FieldByName('i_fatura').AsString + '-0', vlrml)
          else
            GeraComissao(vi_cdtitulo, vlrEntrada, cdsPedidoi_nrpedido.AsString + '-0', vlrml);
        end
        else
          GeraComissao(vi_cdtitulo, vlrEntrada, cdsPedidoi_nrpedido.AsString + '-0', vlrml);
      end;

      for vpt := 1 to totparcelas do begin
        if vpt = totparcelas then
          vlrparcelas := vlrultparce;

        cdsCriaTitulo.Close;
        cdsCriaTitulo.CommandText := 'select CriaTituloRecebimento( ';

        if vTipo = 'B' then
          cdsCriaTitulo.CommandText := cdsCriaTitulo.CommandText + QuotedStr(vTipo) + '::varchar, '
        else
          cdsCriaTitulo.CommandText := cdsCriaTitulo.CommandText + vTipo + '::varchar, ';

        cdsCriaTitulo.CommandText := cdsCriaTitulo.CommandText +
          IntToStr(vCdContaFin) + ', ' +
          Trim(cdsPedidoi_cdcliente.AsString + '1') + ', ' +
          'date(' + QuotedStr(FormatDateTime('yyyy-mm-dd', (Date + StrToInt(parcelas.Strings[vpt-1])))) + '), ' +
          'cast' + '(' + StringReplace(FloatToStr((vlrparcelas+valorboleto)), ',', '.', [rfReplaceAll]) + ' as numeric) ' + ', ';

        if ProgPedVenda = 'CADPEDIDOVENDABALCAO' then begin
          if (cdsUpdate.FieldByName('i_fatura').AsInteger > 0) then begin
            cdsCriaTitulo.CommandText := cdsCriaTitulo.CommandText +
              QuotedStr(cdsUpdate.FieldByName('i_fatura').AsString + '-' + IntToStr(vpt));
          end
          else begin
            cdsCriaTitulo.CommandText := cdsCriaTitulo.CommandText +
              QuotedStr(cdsPedidoi_nrpedido.AsString + '-' + IntToStr(vpt));
          end;
        end
        else begin
          cdsCriaTitulo.CommandText := cdsCriaTitulo.CommandText +
            QuotedStr(cdsPedidoi_nrpedido.AsString + '-' + IntToStr(vpt));
        end;

        cdsCriaTitulo.CommandText :=
          cdsCriaTitulo.CommandText +
          '::character varying, ' +
          IntToStr(vcdvendedor) + ', ' +
          'cast' + '(' + StringReplace(FloatToStr(vlrparcelas), ',', '.', [rfReplaceAll]) + ' as numeric) ' + ', ' +
          IntToStr(cdsPedidoi_cdempresa.AsInteger) +
        '::smallint) as id_titulo';

        cdsCriaTitulo.Open;

        vi_cdtitulo := cdsCriaTitulo.FieldByName('id_titulo').AsInteger;

        //Caso tenha o parametro Dias_Protesto e esteja maior que zero ser� atualizado o titulo com o dia para protesto
        AtualizaTituloProtesto(vi_cdtitulo);

        if ProgPedVenda = 'CADPEDIDOVENDABALCAO' then begin
          if cdsUpdate.FieldByName('i_fatura').AsInteger > 0 then
            GeraComissao(vi_cdtitulo, vlrcomissao, cdsPedidoi_nrpedido.AsString + '-' + IntToStr(vpt), vlrml)
          else
            GeraComissao(vi_cdtitulo, vlrcomissao, cdsPedidoi_nrpedido.AsString + '-' + IntToStr(vpt), vlrml);
        end
        else
          GeraComissao(vi_cdtitulo, vlrcomissao, cdsPedidoi_nrpedido.AsString + '-' + IntToStr(vpt), vlrml);
      end;

      AcertaPed;
      SQLConnection.Commit;
    except
      on E: Exception do begin
        SQLConnection.Rollback;
        raise;
        Abort;
      end;
    end;
  end;
end;

procedure TForm1.GeraComissao(vtitulo: Integer; vlrtitulo: Double; vtitdocumento: string; vlrml: Double);
var
  sql: string;
  vlrcom: Double;
//  percsuframa: Double;
begin
  cdsVendedor.First;
  while not cdsVendedor.Eof do begin
    cdsTotComissao.Close;
    cdsTotComissao.CommandText :=
      ' select ' +
      '   cast(sum(buscacomissaoitem(pv.i_nrpedido, ipv.i_cditempedvenda, pvv.i_cdvendedor, ''D'')) as numeric(12,4)) as total ' +
      ' from ' +
      '   pedidovenda pv ' +
      ' join itempedvenda ipv using(i_nrpedido) ' +
      ' join pedvenda_vendedor pvv on pvv.i_nrpedido = pv.i_nrpedido ' +
      ' where pv.i_nrpedido = :Pedido ' +
      ' and pvv.i_cdvendedor = :Vendedor ' +
      ' and ipv.f_bonifica = ''N'' ' +
      ' and ipv.f_bonifica = ''N'' ';
    cdsTotComissao.Params.ParamByName('PEDIDO').AsInteger := cdsPedidoi_nrpedido.AsInteger;
    cdsTotComissao.Params.ParamByName('VENDEDOR').AsInteger := cdsVendedor.FieldByName('i_cdvendedor').AsInteger;
    cdsTotComissao.Open;

    vlrcom := cdsTotComissao.FieldByName('total').AsFloat;

    vlrcom := ((vlrcom * vlrtitulo)/(cdsPedidon_vlrseparado.AsFloat * (1-(cdsPedidon_percdescpedido.AsFloat/100))));
    //percsuframa := (cdsPedidon_vlrsuframa.AsFloat * 100) / (cdsPedidon_vlrseparado.AsFloat * (1-(cdsPedidon_percdescpedido.AsFloat/100)));

    if vlrcom > 0 then begin
      sql :=
        ' Insert Into Func_Lanc( ' +
        '   i_cdtitulocomissao,  ' +
        '   i_cdtipo_func_lanc,  ' +
        '   i_cdparceiro,        ' +
        '   c_descricao,         ' +
        '   n_valor,             ' +
        '   f_tpmov,             ' +
        '   d_lancamento,        ';

      if vComissao_Liquidez = 'N' then
        sql := sql + ' d_vencimento, ';

      sql := sql +
        '   n_vlrtitcomissao     ' +
        '   ) Values (           ' +
        '   :I_CdTitulo,         ' +
        '   cast(RetParametro(''TipoFuncLancComissao'') as Integer), ' +
        '   :I_CdParceiro,       ' +
        '   :C_documento,        ' +
        '   :N_Valor,            ' +
        '   ''C'',               ' +
        '   Now(),               ';

      if vComissao_Liquidez = 'N' then
        sql := sql + ' Now(), ';
      sql := sql + ' :vN_VlrTitComissao); ';

      cdsFuncLanc.Close;
      cdsFuncLanc.CommandText := sql;
      cdsFuncLanc.Params.ParamByName('I_CdTitulo').AsInteger := vtitulo;
      cdsFuncLanc.Params.ParamByName('I_CdParceiro').AsInteger := cdsVendedor.FieldByName('i_cdparceiro').AsInteger;
      cdsFuncLanc.Params.ParamByName('C_documento').AsString := 'C' + vtitdocumento;
      cdsFuncLanc.Params.ParamByName('N_Valor').AsFloat := vlrcom;

      if eConfiguracao.Lines.Values['MARGEMLUCRO'] = 'S' then
        cdsFuncLanc.Params.ParamByName('vN_VlrTitComissao').AsFloat := vlrml //  * (1-(percsuframa/100))
      else
        cdsFuncLanc.Params.ParamByName('vN_VlrTitComissao').AsFloat := vlrtitulo ;//* (1-(percsuframa/100));
      cdsFuncLanc.Execute;

      if vCria_Comissao_PremioLiqui and (cdsVendedor.FieldByName('f_vendedor').AsString = 'R') then begin
        cdsTotComissao.Close;
        cdsTotComissao.CommandText :=
          ' select ' +
          '   cast(sum(buscacomissaoitem(pv.i_nrpedido,ipv.i_cditempedvenda,pvv.i_cdvendedor,''L'')) as numeric(12,4)) as total ' + // Premio de Liquidez
          ' from ' +
          ' pedidovenda pv ' +
          ' join itempedvenda ipv using(i_nrpedido) ' +
          ' join pedvenda_vendedor pvv on pvv.i_nrpedido = pv.i_nrpedido ' +
          ' where pv.i_nrpedido = :Pedido ' +
          ' and pvv.i_cdvendedor = :Vendedor ' +
          ' and ipv.f_bonifica = ''N'' ';
        cdsTotComissao.Params.ParamByName('PEDIDO').AsInteger := cdsPedidoi_nrpedido.AsInteger;
        cdsTotComissao.Params.ParamByName('VENDEDOR').AsInteger := cdsVendedor.FieldByName('i_cdvendedor').AsInteger;
        cdsTotComissao.Open;

        vlrcom := cdsTotComissao.FieldByName('total').AsFloat;

        vlrcom := ((vlrcom * vlrtitulo)/(cdsPedidon_vlrseparado.AsFloat * (1-(cdsPedidon_percdescpedido.AsFloat/100))));

        if vlrcom > 0 then begin
          cdsFuncLanc.Close;
          cdsFuncLanc.CommandText :=
            ' Insert Into Func_Lanc( ' +
            '   i_cdtitulocomissao,  ' +
            '   i_cdtipo_func_lanc,  ' +
            '   i_cdparceiro,        ' +
            '   c_descricao,         ' +
            '   n_valor,             ' +
            '   f_tpmov,             ' +
            '   d_lancamento,        ' +
            '   n_vlrtitcomissao     ' +
            ' ) Values (             ' +
            '   :I_CdTitulo,         ' +
            '   cast(RetParametro(''TipoFuncLancPremio'') as Integer), ' +
            '   :I_CdParceiro,       ' +
            '   :C_documento,        ' +
            '   :N_Valor,            ' +
            '   ''C'',               ' +
            '   Now(),               ' +
            '   :vN_VlrTitComissao   ' +
            ' );';

          cdsFuncLanc.Params.ParamByName('I_CdTitulo').AsInteger := vtitulo;
          cdsFuncLanc.Params.ParamByName('I_CdParceiro').AsInteger := cdsVendedor.FieldByName('i_cdparceiro').AsInteger;
          cdsFuncLanc.Params.ParamByName('C_documento').AsString := 'P' + vtitdocumento;
          cdsFuncLanc.Params.ParamByName('N_Valor').AsFloat := vlrcom;

          if eConfiguracao.Lines.Values['MARGEMLUCRO'] = 'S' then
            cdsFuncLanc.Params.ParamByName('vN_VlrTitComissao').AsFloat := vlrml //* (1-(percsuframa/100))
          else
            cdsFuncLanc.Params.ParamByName('vN_VlrTitComissao').AsFloat := vlrtitulo;// * (1-(percsuframa/100));
          cdsFuncLanc.Execute;
        end;
      end;
    end;

    cdsVendedor.Next;
  end;
end;

procedure TForm1.AcertaPed;
var
  novoprc: Double;
  novodes: Double;
  novounit: Double;
  vlrfatura: Double;

  sql: string;
  vprazo: string;
  vtotal: string;
  vPrc_Custo_Produto: string;

  vpt: Integer;
  vUsuario: Integer;
  vAcrescimo: Boolean;
begin
  // Altera pedido
  cdsPedidoVenda.Close;
  cdsPedidoVenda.CommandText :=
    ' update pedidovenda set          ' +
    '   c_observacao = :c_observacao, ' +
    '   c_prazo      = :c_prazo       ' +
    ' where i_nrpedido = :i_nrpedido ';
  cdsPedidoVenda.Params.ParamByName('c_observacao').AsString := cdsPedidoc_observacao.AsString;

  if cbapaga.Checked and eDesconto.Visible then begin
    vprazo := '';
    vpt := 1;

    while (TEdit(FindComponent('P' + IntToStr(vpt))).Text <> '') and  (vpt <= 12) do begin
      if not (TCheckBox(FindComponent('C' + IntToStr(vpt))).Checked) then
        vprazo := vprazo  + TEdit(FindComponent('P' + IntToStr(vpt))).Text;
      Inc(vpt);
    end;

    cdsPedidoVenda.Params.ParamByName('c_prazo').AsString := vprazo;
  end
  else
    cdsPedidoVenda.Params.ParamByName('c_prazo').AsString := cdsPedidoc_prazo.AsString;

  cdsPedidoVenda.Params.ParamByName('i_nrpedido').AsInteger := cdsPedidoi_nrpedido.AsInteger;
  cdsPedidoVenda.Execute;


  if StrToFloatDef(eEntrada.Text, 0) > 0 then begin
    cdsPedidoVenda.Close;
    cdsPedidoVenda.CommandText := ' update pedidovenda set n_vlrentrada = n_vlrentrada - :n_vlrentrada where i_nrpedido = :i_nrpedido ';
    cdsPedidoVenda.Params.ParamByName('n_vlrentrada').AsFloat := StrToFloatDef(eEntrada.Text,0);
    cdsPedidoVenda.Params.ParamByName('i_nrpedido').AsInteger := cdsPedidoi_nrpedido.AsInteger;
    cdsPedidoVenda.Execute;
  end;

  if vlrdesconto = 100 then begin
    // Com desconto = 100% cancela o pedido
    // Retira a substituicao
    cdsPedidoVenda.Close;
    cdsPedidoVenda.CommandText :=
      ' update pedidovenda set ' +
      '   n_vlrfaturamento = n_vlrfaturamento - n_vlricmssubst ' +
      ' where i_nrpedido = :i_nrpedido ';
    cdsPedidoVenda.Params.ParamByName('i_nrpedido').AsInteger := cdsPedidoi_nrpedido.AsInteger;
    cdsPedidoVenda.Execute;

    cdsPedidoVenda.Close;
    cdsPedidoVenda.CommandText :=
      ' update pedidovenda set ' +
      '   n_basecalcicmssubst = 0, ' +
      '   n_vlricmssubst = 0 ' +
      ' where i_nrpedido = :i_nrpedido ';
    cdsPedidoVenda.Params.ParamByName('i_nrpedido').AsInteger := cdsPedidoi_nrpedido.AsInteger;
    cdsPedidoVenda.Execute;

    // Cancela pedido
    cdsCriaParametro.Close;
    cdsCriaParametro.CommandText := 'select insparametro(''LIMPASEPARACAO'', ''N'')';
    cdsCriaParametro.Active := True;

    cdsPedidoVenda.Close;
    cdsPedidoVenda.CommandText :=
      ' update pedidovenda set ' +
      '   f_cancelado = ''S'' ' +
      ' where i_nrpedido = :i_nrpedido ';
    cdsPedidoVenda.Params.ParamByName('i_nrpedido').AsInteger := cdsPedidoi_nrpedido.AsInteger;
    cdsPedidoVenda.Execute;

    cdsCriaParametro.Close;
    cdsCriaParametro.CommandText := 'select insparametro(''LIMPASEPARACAO'', ''S'')';
    cdsCriaParametro.Active := True;

    //baixa estoque
    cdsItemPed.Close;
    cdsItemPed.CommandText :=
      ' select ' +
      '   i_cdproduto,    ' +
      '   n_qtdeseparada, ' +
      '   n_prcunitario   ' +
      ' from              ' +
      '   ItemPedVenda    ' +
      ' where             ' +
      '   I_NrPedido = :I_NrPedido and ' +
      '   i_nrrequisicao is null ';
    cdsItemPed.Params.ParamByName('I_NrPedido').AsInteger := cdsPedidoi_nrpedido.AsInteger;
    cdsItemPed.Open;

    while not cdsItemPed.Eof do begin
      cdsUpdateItem.Close;
      cdsUpdateItem.CommandText :=
        ' update estoque set ' +
        '   n_qtdestoque = n_qtdestoque - :n_qtdestoque ' +
        ' where i_Cdproduto = :i_Cdproduto ' +
        '   and i_cdlocal_estoque = :i_cdlocal_estoque ' +
        '   and f_controla_estoque = ''S''';
      cdsUpdateItem.Params.ParamByName('n_qtdestoque').AsFloat := cdsItemPed.FieldByName('n_qtdeseparada').AsFloat;
      cdsUpdateItem.Params.ParamByName('i_Cdproduto').AsInteger := cdsItemPed.FieldByName('i_cdproduto').AsInteger;
      cdsUpdateItem.Params.ParamByName('i_cdlocal_estoque').AsInteger := cdsPedidoi_cdlocal_estoque.AsInteger;
      cdsUpdateItem.Execute;

      cdsEstoque.Close;
      cdsEstoque.CommandText:=
        ' select ' +
        '   * ' +
        ' from ' +
        '   estoque ' +
        ' where ' +
        '   i_Cdproduto = :i_Cdproduto ' +
        ' and i_cdlocal_estoque = :i_cdlocal_estoque ' +
        ' and f_controla_estoque = ''S''';
      cdsEstoque.Params.ParamByName('i_Cdproduto').AsInteger := cdsItemPed.FieldByName('i_cdproduto').AsInteger;
      cdsEstoque.Params.ParamByName('i_cdlocal_estoque').AsInteger := cdsPedidoi_cdlocal_estoque.AsInteger;
      cdsEstoque.Open;

      vUsuario := StrToInt(eConfiguracao.Lines.Values['usuario']);
      vtotal := (cdsEstoque.FieldByName('n_qtdestoque').AsVariant - cdsItemPed.FieldByName('n_qtdeseparada').AsVariant);

      cdsInsertLog.Close;
      cdsInsertLog.CommandText:=
        ' insert into log.log(i_cdusuario, c_log, d_data, c_tabela, i_idtabela, f_acao) ' +
        '	values(:usuario, ''CPF - quantidade retirada do estoque ' +
        cdsItemPed.FieldByName('n_qtdeseparada').AsString +
        ' quantidade atual em estoque ' +
        vtotal +
        ' '' , :data, ''ESTOQUE'', :i_Cdproduto, ''I'')';
      cdsInsertLog.Params.ParamByName('usuario').AsInteger := vUsuario;
      cdsInsertLog.Params.ParamByName('i_Cdproduto').AsInteger := cdsItemPed.FieldByName('i_cdproduto').AsInteger;
      cdsInsertLog.Params.ParamByName('data').AsDate := Now();
      cdsInsertLog.Execute;

      cdsItemPed.Next;
    end;

    FechaRequisicao;
  end
  else begin //com desconto corrige valores do item
    if UpperCase(eConfiguracao.Lines.Values['TIPODESCONTO']) = 'PEDIDO' then begin
      cdsPedidoVenda.Close;
      cdsPedidoVenda.CommandText :=
        ' update pedidovenda set ' +
        '   n_percdescpedido = :n_percdescpedido, ' +
        '   n_vlrdesconto = :n_vlrdesconto ' +
        ' where i_nrpedido = :i_nrpedido';
      cdsPedidoVenda.Params.ParamByName('n_percdescpedido').AsFloat := 100 - ((100 - cdsPedido.FieldByName('n_percdescpedido').AsFloat) * ((100 - vlrdesconto)/100));
      cdsPedidoVenda.Params.ParamByName('n_vlrdesconto').AsFloat := cdsPedido.FieldByName('n_vlrseparado').AsFloat * ((100 - vlrdesconto)/100);
      cdsPedidoVenda.Params.ParamByName('i_nrpedido').AsInteger := cdsPedidoi_nrpedido.AsInteger;
      cdsPedidoVenda.Execute;
    end
    else begin
      if eConfiguracao.Lines.Values['MARGEMLUCRO'] = 'S' then
        sql := ' insert into desc_com (i_cddesc_com, n_valor2) values (:i_cddesc_com, :n_valor2) '
      else
        sql := ' insert into desc_com (i_cddesc_com, n_valor) values (:i_cddesc_com, :n_valor) ';

      // Calculo, quando possui desconto no pedido e param. Margem_Min_Lucro
      cdsRcPedVenda.Close;
      cdsRcPedVenda.Params.ParamByName('pedido').AsInteger := StrToInt(ePedido.Text);
      cdsRcPedVenda.Open;

      if (StrToIntDef(eConfiguracao.Lines.Values['Margem_Min_Lucro'],0) > 0) and (cdsRcPedVenda.FieldByName('n_percdescpedido').AsFloat > 0) then begin
        // Recuperando o valor total da fatura
        vlrfatura := cdsPedido.FieldByName('n_vlrfaturamento').AsFloat  - cdsRcPedVenda.FieldByName('n_custofrete').AsFloat - cdsRcPedVenda.FieldByName('n_despacess').AsFloat;
        vlrfatura := (vlrfatura * 100) / (100 - (cdsRcPedVenda.FieldByName('n_percdescpedido').AsFloat));

        if vMargemLucro = True then
          vlrfatura := vlrfatura - BuscaNovosValoreSPED;

        cdsPedidoVenda.Close;
        cdsPedidoVenda.CommandText :=
          ' update pedidovenda set ' +
          '   n_percdescpedido = :n_percdescpedido, ' +
          '   n_vlrdesconto = :n_vlrdesconto ' +
          ' where i_nrpedido = :i_nrpedido ';

        if vMargemLucro = True then
          cdsPedidoVenda.Params.ParamByName('n_percdescpedido').AsFloat := (100 * cdsPedido.FieldByName('n_vlrdesconto').AsFloat) / vlrfatura
        else
          cdsPedidoVenda.Params.ParamByName('n_percdescpedido').AsFloat := 100 * (cdsPedido.FieldByName('n_vlrdesconto').AsFloat / (vlrfatura * (100 - vlrdesconto)/100));

        cdsPedidoVenda.Params.ParamByName('n_vlrdesconto').AsFloat := cdsPedido.FieldByName('n_vlrdesconto').AsFloat;
        cdsPedidoVenda.Params.ParamByName('i_nrpedido').AsInteger := cdsPedidoi_nrpedido.AsInteger;
        cdsPedidoVenda.Execute;
      end;

      vMargemLucro := False;

      vPrc_Custo_Produto := eConfiguracao.Lines.values['PRC_CUSTO_PRODUTO'];

      cdsItemPed.Close;
      cdsItemPed.CommandText :=
        ' select ' +
        '   case ''' + vPrc_Custo_Produto + ''' when ''L'' then ' +
        '     prc.n_precocusto ' +
        '   else ' +
        '     prod.n_ultimoprecocompra ' +
        '   end as n_precocusto, ' +
        '   i_cditempedvenda, ' +
        '   n_prcvenda, ' +
        '   N_PercDesconto, ' +
        '   n_prcunitario ' +
        ' from ' +
        '   ItemPedVenda ' +
        ' join produto  prod using(i_cdproduto) ' +
        ' left join preco prc on prc.i_cdproduto = prod.i_cdproduto and ' +
        ' prc.i_cdlista_preco = itempedvenda.i_cdlista_preco ' +
        ' where ' +
        '   I_NrPedido = :I_NrPedido ';
      cdsItemPed.Params.ParamByName('I_NrPedido').AsInteger := cdsPedidoi_nrpedido.AsInteger;
      cdsItemPed.Open;

      while not cdsItemPed.Eof do begin
        // Caso o valor da venda � maior que pre�o unitario (acrescimo) e que n�o seja 100%
        vAcrescimo :=
          (cdsItemPed.FieldByName('n_prcvenda').AsFloat > cdsItemPed.FieldByName('n_prcunitario').AsFloat)
          and
          (eDesconto.Visible)
          and
          (StrToFloat(eDesconto.Text) < 100);

        if eConfiguracao.Lines.Values['MARGEMLUCRO'] = 'S' then begin
          cdsInsertDesc.Close;
          cdsInsertDesc.CommandText := sql;
          cdsInsertDesc.Params.ParamByName('i_cddesc_com').AsInteger:= cdsItemPed.FieldByName('i_cditempedvenda').AsInteger;
          cdsInsertDesc.Params.ParamByName('n_valor2').AsFloat := cdsItemPed.FieldByName('n_prcunitario').AsFloat;
          cdsInsertDesc.Execute;

          if eConfiguracao.Lines.Values['arredonda'] = 'S' then begin
            novounit := Round((cdsItemPed.FieldByName('n_prcunitario').AsFloat * ((100 - vlrdesconto) / 100)) * 100) / 100;
            novoprc := Round(novounit + (novounit * (cdsItemPed.FieldByName('N_PercDesconto').AsFloat / 100)));
          end
          else begin
            novounit := cdsItemPed.FieldByName('n_prcunitario').AsFloat * ((100 - vlrdesconto) / 100);
            novoprc := novounit + (novounit * (cdsItemPed.FieldByName('N_PercDesconto').AsFloat / 100));
          end;

          novodes := cdsItemPed.FieldByName('N_PercDesconto').AsFloat;
        end
        else
        begin
          cdsInsertDesc.Close;
          cdsInsertDesc.CommandText := sql;
          cdsInsertDesc.Params.ParamByName('i_cddesc_com').AsInteger := cdsItemPed.FieldByName('i_cditempedvenda').AsInteger;
          if vAcrescimo then
            cdsInsertDesc.Params.ParamByName('n_valor').AsFloat := StrToFloat(eDesconto.Text) * (-1)
          else
            cdsInsertDesc.Params.ParamByName('n_valor').AsFloat := cdsItemPed.FieldByName('N_PercDesconto').AsFloat;
          cdsInsertDesc.Execute;

          if
            (StrToIntDef(eConfiguracao.Lines.values['Margem_Min_Lucro'],0) > 0)
            and
            (cdsItemPed.FieldByName('n_precocusto').AsFloat * (1 + ((StrToIntDef(eConfiguracao.Lines.values['Margem_Min_Lucro'],0)/100))) > (cdsItemPed.FieldByName('n_prcvenda').AsFloat * (1 - (vlrdesconto/100))))
          then begin
            if eConfiguracao.Lines.Values['arredonda'] = 'S' then
              novoprc := Round((cdsItemPed.FieldByName('n_precocusto').AsFloat * (1 + (StrToInt(eConfiguracao.Lines.values['Margem_Min_Lucro']) / 100))) * 100) / 100
            else
              novoprc := (cdsItemPed.FieldByName('n_precocusto').AsFloat * (1 + (StrToInt(eConfiguracao.Lines.values['Margem_Min_Lucro']) / 100)));

            if novoprc > cdsItemPed.FieldByName('n_prcvenda').AsFloat then begin
              if eConfiguracao.Lines.Values['arredonda'] = 'S' then
                novoprc := Round((cdsItemPed.FieldByName('n_prcvenda').AsFloat) * 100) / 100
              else
                novoprc := cdsItemPed.FieldByName('n_prcvenda').AsFloat;
            end;

            novodes := ((cdsItemPed.FieldByName('n_prcunitario').AsFloat - novoprc) * 100) / cdsItemPed.FieldByName('n_prcunitario').AsFloat;
          end
          else begin
            if eConfiguracao.Lines.Values['arredonda'] = 'S' then
              novoprc := Round((cdsItemPed.FieldByName('n_prcvenda').AsFloat * ((100 - vlrdesconto) / 100)) * 100) / 100
            else
              novoprc := cdsItemPed.FieldByName('n_prcvenda').AsFloat * ((100 - vlrdesconto) / 100);

            if vAcrescimo then
              novodes := 100 - (novoprc * 100 / cdsItemPed.FieldByName('n_prcunitario').AsFloat)
            else
              novodes := 100 - ((100 - cdsItemPed.FieldByName('N_PercDesconto').AsFloat) * ((100 - vlrdesconto) / 100));

            if novodes < 0 then
              novodes := 0;
          end;

          novounit := cdsItemPed.FieldByName('n_prcunitario').AsFloat;
        end;

        cdsUpdateItem.Close;
        cdsUpdateItem.CommandText :=
          ' update ItemPedVenda set ' +
          '   n_prcvenda = :n_prcvenda, ' +
          '   n_percDesconto = :n_percDesconto, ' +
          '   n_prcunitario = :n_prcunitario ' +
          ' where ' +
          '   ItemPedVenda.i_cditempedvenda = :i_cditempedvenda ';
        cdsUpdateItem.Params.ParamByName('n_prcvenda').AsFloat := novoprc;
        cdsUpdateItem.Params.ParamByName('n_prcunitario').AsFloat := novounit;
        cdsUpdateItem.Params.ParamByName('n_percDesconto').AsFloat := novodes;
        cdsUpdateItem.Params.ParamByName('i_cditempedvenda').AsInteger := cdsItemPed.FieldByName('i_cditempedvenda').AsInteger;
        cdsUpdateItem.Execute;

        cdsItemPed.Next;
      end;
    end;

    cdsCalPedido.Close;
    cdsCalPedido.CommandText := ' select calculavalorespedido( ' + cdsPedidoi_nrpedido.AsString + ' ) ';
    cdsCalPedido.Open;
  end;
end;

procedure TForm1.ePedidoEnter(Sender: TObject);
begin
  Conectar;

  cdsCriaParametro.Close;
  cdsCriaParametro.CommandText := 'select CriaParametro() ';
  cdsCriaParametro.Open;

  cdsVerificaMes.Close;
  cdsVerificaMes.CommandText := ' select retparametro(''ATUALIZACAO_COMPLEMENTO'') as comp ';
  cdsVerificaMes.Open;

  if (Pos(cdsVerificaMes.FieldByName('comp').AsString, Self.Caption) = 0) then
  begin
    MessageDlg('Programa Desatualizado!', mtError, [mbOK], 0);
    Application.Terminate;
  end;

  cdsCriaParametro.Close;
  cdsCriaParametro.CommandText := 'select retparametro(''Cria_Comissao_PremioLiqui'')';
  cdsCriaParametro.Open;

  vCria_Comissao_PremioLiqui := cdsCriaParametro.Fields[0].AsString = 'S';

  cdsCriaParametro.Close;
  cdsCriaParametro.CommandText := 'select retparametro(''COMISSAO_LIQUIDEZ'')';
  cdsCriaParametro.Open;

  vComissao_Liquidez := cdsCriaParametro.Fields[0].AsString;

  cdsCriaParametro.Close;
  cdsCriaParametro.CommandText := 'select insparametro(''I_CdUsuarioAtual'', ' + QuotedStr(eConfiguracao.Lines.Values['usuario']) + ')';
  cdsCriaParametro.Open;

  if vExecuta_ExcluiDescCom = False then begin
    ExcluiDescCom;
    vExecuta_ExcluiDescCom := True;
  end;

  qTipoDoc.Close;
  qTipoDoc.SQL.Clear;
  qTipoDoc.SQL.Add('select i_cdtipotitulo, c_descricao, c_sigla from tipotitulo where f_tipo = ''R'' ');
  qTipoDoc.Open;
end;

procedure TForm1.bSalvaConfgClick(Sender: TObject);
begin
  eConfiguracao.Lines.SaveToFile('cpf.ini');
end;

procedure TForm1.bDescontoClick(Sender: TObject);
begin
  eDesconto.Visible := not eDesconto.Visible;
  ldesconto.Visible := not ldesconto.Visible;
  lperc.Visible := not lperc.Visible;
  cbGeraEntrada.Visible := not eDesconto.Visible;

  if eDesconto.Visible then
    bDesconto.Caption := 'Sem &Desconto'
  else
    bDesconto.Caption := 'Com &Desconto';

  Busca;
end;

procedure TForm1.bRetornaPedClick(Sender: TObject);
var
  _AutoMontagemKit: string;
begin
  cdsPedido.Close;
  cdsPedido.Params.ParamByName('i_nrpedido').AsInteger := StrToInt(ePedido.Text);
  cdsPedido.Open;

  if cdsPedido.RecordCount = 0 then
    Exit;

  //verifica se algum titulo esta pago total  ou parcial
  if not(cdsPedido.Active) then begin
    ShowMessage('Nenhum pedido selecionado!');
    Exit;
  end;

  if cdsPedidof_faturou.AsString = 'S' then begin
    MessageDlg('Aten��o, j� foi faturado!', mtError,  [mbOk], 0);
    Exit;
  end;

  cdsTitulo.First;
  while not(cdsTitulo.Eof) do begin
    if cdsTitulo.FieldByName('n_vracerto').AsFloat > 0 then begin
      ShowMessage('O titulo ' + cdsTitulo.FieldByName('i_cdtitulo').AsString + ' j� foi movimentado!, este n�o pode ser excluido!');
      Exit;
    end;

    cdsTitulo.Next;
  end;

  cdsHistoricoCNAB.Close;
  cdsHistoricoCNAB.Params.ParamByName('i_cdtitulo').AsInteger := cdsTitulo.FieldByName('i_cdtitulo').AsInteger;
  cdsHistoricoCNAB.Open;

  if cdsHistoricoCNAB.RecordCount > 0 then
    if MessageDlg('Existe(m) registro(s) de titulo(s) no historico CNAB. Deseja continuar?', mtInformation, [mbYes, mbNo], 0) = mrNo then
      Exit;

  if (cdsPedidof_cancelado.AsString = 'S') then begin
    if MessageDlg('O pedido est� cancelado! Corrige o estoque e exclui titulos?', mtConfirmation, [mbYes, mbNo], 0) = mrNo then
      Exit;
    DesCancelaPedido;
  end
  else begin
    if MessageDlg('Deseja exclui os titulos e acertar os itens do pedido?', mtConfirmation, [mbYes, mbNo], 0) = mrNo then
      Exit;
    CorrigeItens;
  end;

  // Corre��o para solicita��o 10043
  qSQL.Close;
  qSQL.SQL.Clear;
  qSQL.SQL.Add('select');
  qSQL.SQL.Add('  c_valor');
  qSQL.SQL.Add('from');
  qSQL.SQL.Add('  parametro');
  qSQL.SQL.Add('where');
  qSQL.SQL.Add('  c_nome = ''AUTOMONTAGEMKIT'' ');
  qSQL.Open;

  if qSQL.RecordCount > 0 then begin
    _AutoMontagemKit := qSQL.FieldByName('c_valor').AsString;

    if _AutoMontagemKit = 'S' then
      AutoMontarDesmontarKit(StrToInt(ePedido.Text), 'D'); // Desmontagem do Kit
  end;
  BaixarCaixas(StrToInt(ePedido.Text), 'R');
  //

  // Carrega novamente o pedido
  Busca;
end;

procedure TForm1.DesCancelaPedido;
begin
  if not(SQLConnection.InTransaction) then begin
    SQLConnection.StartTransaction;
    try
      ExcluiTitulos;

      // Baixa estoque
      cdsItemPed.Close;
      cdsItemPed.CommandText :=
        ' select ' +
        '   i_cdproduto, ' +
        '   n_qtdeseparada, ' +
        '   i_cditempedvenda, ' +
        '   n_prcunitario ' +
        ' from ' +
        '   ItemPedVenda ' +
        ' where ' +
        '   I_NrPedido = :I_NrPedido and ' +
        '   i_nrrequisicao is null ';
      cdsItemPed.Params.ParamByName('I_NrPedido').AsInteger := cdsPedidoi_nrpedido.AsInteger;
      cdsItemPed.Open;

      while not(cdsItemPed.Eof) do begin
        cdsUpdateItem.Close;
        cdsUpdateItem.CommandText :=
          ' update estoque set ' +
          '   n_qtdestoque = n_qtdestoque + :n_qtdestoque ' +
          ' where i_Cdproduto = :i_Cdproduto ' +
          '   and i_cdlocal_estoque = :i_cdlocal_estoque ' +
          '   and f_controla_estoque = ''S''';

        cdsUpdateItem.Params.ParamByName('n_qtdestoque').AsFloat := cdsItemPed.FieldByName('n_qtdeseparada').AsInteger;
        cdsUpdateItem.Params.ParamByName('i_Cdproduto').AsInteger := cdsItemPed.FieldByName('i_cdproduto').AsInteger;
        cdsUpdateItem.Params.ParamByName('i_cdlocal_estoque').AsInteger := cdsPedidoi_cdlocal_estoque.AsInteger;
        cdsUpdateItem.Execute;

        cdsItemPed.Next;
      end;

      cdsCriaParametro.Close;
      cdsCriaParametro.CommandText := 'select insparametro(''LIMPASEPARACAO'', ''N'')';
      cdsCriaParametro.Open;

      cdsCriaParametro.Close;
      cdsCriaParametro.CommandText := 'select insparametro(''CANCELAPEDIDOCPF'', ''S'')';
      cdsCriaParametro.Open;

      cdsPedidoVenda.Close;
      cdsPedidoVenda.CommandText :=
        ' update pedidovenda set ' +
        '   f_cancelado = ''N'' ' +
        ' where i_nrpedido  = :i_nrpedido ';
      cdsPedidoVenda.Params.ParamByName('i_nrpedido').AsInteger := cdsPedidoi_nrpedido.AsInteger;
      cdsPedidoVenda.Execute;

      cdsCriaParametro.Close;
      cdsCriaParametro.CommandText := 'select insparametro(''LIMPASEPARACAO'', ''S'')';
      cdsCriaParametro.Open;

      cdsCriaParametro.Close;
      cdsCriaParametro.CommandText := 'select insparametro(''CANCELAPEDIDOCPF'', ''N'')';
      cdsCriaParametro.Open;

      AbreRequisicao;

      SQLConnection.Commit;
    except
      on E: Exception do begin
        SQLConnection.Rollback;
        raise;
      end;
    end;
  end;
end;

procedure TForm1.CorrigeItens;
var
  sql1: string;

  novoprc: Double;
  novodes: Double;
  novounit: Double;
  vlrtotal: Double;
  vlrfatura: Double;
  vlrtitulos: Double;
  valorboleto: Double;

  vAcrescimo: Boolean;
begin
  if not(SQLConnection.InTransaction)then begin
    SQLConnection.StartTransaction;

    try
      cdsBuscaTitEnt.Close;
      cdsBuscaTitEnt.CommandText := ' select n_vrtitulo from titulo where c_documento = ''' + cdsPedidoi_nrpedido.AsString + '-0'' ';
      cdsBuscaTitEnt.Open;

      if cdsBuscaTitEnt.FieldByName('n_vrtitulo').AsFloat > 0 then begin
        cdsPedidoVenda.Close;
        cdsPedidoVenda.CommandText :=
          ' update pedidovenda set ' +
          '   n_vlrentrada = n_vlrentrada + :n_vlrentrada ' +
          ' where i_nrpedido = :i_nrpedido ';
        cdsPedidoVenda.Params.ParamByName('i_nrpedido').AsInteger := cdsPedidoi_nrpedido.AsInteger;
        cdsPedidoVenda.Params.ParamByName('n_vlrentrada').AsFloat := cdsBuscaTitEnt.FieldByName('n_vrtitulo').AsFloat;
        cdsPedidoVenda.Execute;
      end;

      ExcluiTitulos;

      if UpperCase(eConfiguracao.Lines.Values['TIPODESCONTO']) = 'PEDIDO' then begin
        cdsPedidoVenda.Close;
        cdsPedidoVenda.CommandText :=
          ' update pedidovenda set ' +
          '   n_percdescpedido = :n_percdescpedido ' +
          ' where i_nrpedido = :i_nrpedido ';
        cdsPedidoVenda.Params.ParamByName('n_percdescpedido').AsFloat := 100 - ((100 - cdsPedido.FieldByName('n_percdescpedido').AsFloat) / ((100 - vlrdesconto) / 100));
        cdsPedidoVenda.Params.ParamByName('i_nrpedido').AsInteger := cdsPedidoi_nrpedido.AsInteger;
        cdsPedidoVenda.Execute;
      end
      else begin
        if eConfiguracao.Lines.Values['MARGEMLUCRO'] = 'S' then begin
          sql1 :=
            ' select                      ' +
            '   it.i_cditempedvenda,      ' +
            '   it.n_prcvenda,            ' +
            '   it.N_PercDesconto,        ' +
            '   dc.n_valor,n_prcunitario, ' +
            '   dc.n_valor2               ' +
            ' from ItemPedVenda it        ' +
            ' join desc_com dc on dc.i_cddesc_com = it.i_cditempedvenda ' +
            ' where it.I_NrPedido = :I_NrPedido';
        end
        else begin
          sql1 :=
            ' select                     ' +
            '   it.i_cditempedvenda,     ' +
            '   it.n_prcvenda,           ' +
            '   it.N_PercDesconto,       ' +
            '   dc.n_valor,n_prcunitario ' +
            ' from ItemPedVenda it       ' +
            ' join desc_com dc on dc.i_cddesc_com = it.i_cditempedvenda ' +
            ' where it.I_NrPedido = :I_NrPedido';
        end;

        // Calculo, quando possui desconto no pedido e param. Margem_Min_Lucro
        cdsRcPedVenda.Close;
        cdsRcPedVenda.Params.ParamByName('pedido').AsInteger := StrToInt(ePedido.Text);
        cdsRcPedVenda.Open;

        if (StrToIntDef(eConfiguracao.Lines.values['Margem_Min_Lucro'],0) > 0) and (cdsRcPedVenda.FieldByName('n_percdescpedido').AsFloat > 0) then begin
          if cdsRcPedVenda.FieldByName('n_percdescpedido').AsFloat > 0 then begin
            //Recuperando valor do boleto (despesas acessorias)
            if cdsPedidof_cobraboleto.Value = 'S' then begin
              cdsValorPedido.Close;
              cdsValorPedido.CommandText := 'select N_ValorBoleto from ContaFinanceira where I_CdContaFin = :I_CdContaFin ';
              cdsValorPedido.Params.ParamByName('i_cdcontafin').AsInteger := StrToInt(eConfiguracao.Lines.Values['contafin']);
              cdsValorPedido.Open;

              valorboleto := cdsValorPedido.FieldByName('n_valorboleto').AsFloat;
            end
            else
              valorboleto := 0;

            vlrtitulos := 0;

            //Recuperando valor total dos titulos
            cdsTitulo.First;
            while not cdsTitulo.Eof do begin
              vlrtitulos := vlrtitulos + (cdsTitulo.FieldByName('n_vrtitulo').AsFloat - valorboleto);
              cdsTitulo.Next;
            end;

            //Calculando valor do pedido
            vlrfatura := cdsPedido.FieldByName('n_vlrfaturamento').AsFloat - cdsRcPedVenda.FieldByName('n_custofrete').AsFloat - cdsRcPedVenda.FieldByName('n_despacess').AsFloat;
            vlrtotal := vlrtitulos + vlrfatura + cdsRcPedVenda.FieldByName('n_vlrdesconto').AsFloat;

            //Acertando pedido
            cdsPedidoVenda.Close;
            cdsPedidoVenda.CommandText :=
              ' update pedidovenda set ' +
              '   n_vlrfaturamento = :n_vlrfaturamento, ' +
              '   n_percdescpedido = :n_percdescpedido ' +
              ' where i_nrpedido = :i_nrpedido ';
            cdsPedidoVenda.Params.ParamByName('n_vlrfaturamento').AsFloat := vlrtotal;
            cdsPedidoVenda.Params.ParamByName('n_percdescpedido').AsFloat := (cdsRcPedVenda.FieldByName('n_vlrdesconto').AsFloat * 100) / vlrtotal;
            cdsPedidoVenda.Params.ParamByName('i_nrpedido').AsInteger := cdsPedidoi_nrpedido.AsInteger;
            cdsPedidoVenda.Execute;
          end;
        end;

        cdsItemPed.Close;
        cdsItemPed.CommandText := sql1;
        cdsItemPed.Params.ParamByName('I_NrPedido').AsInteger := cdsPedidoi_nrpedido.AsInteger;
        cdsItemPed.Open;

        cdsItemPed.First;
        while not cdsItemPed.Eof do begin
          vAcrescimo := cdsItemPed.FieldByName('n_valor').AsFloat < 0;

          if eConfiguracao.Lines.Values['MARGEMLUCRO'] = 'S' then begin
            novounit := cdsItemPed.FieldByName('n_valor2').AsFloat;
            novodes := cdsItemPed.FieldByName('N_PercDesconto').AsFloat;

            if eConfiguracao.Lines.Values['arredonda'] = 'S' then
              novoprc := Round(novounit + (novounit * (novodes / 100)))
            else
              novoprc := novounit + (novounit * (novodes / 100));
          end
          else begin
            novounit := cdsItemPed.FieldByName('n_prcunitario').AsFloat;

            if vAcrescimo then begin
              novodes := 0;

              if eConfiguracao.Lines.Values['arredonda'] = 'S' then
                novoprc := Round(cdsItemPed.FieldByName('n_prcvenda').AsFloat * 100 / (100 - (cdsItemPed.FieldByName('n_valor').AsFloat * (-1))) * 100) / 100
              else
                novoprc := cdsItemPed.FieldByName('n_prcvenda').AsFloat * 100 / ( 100 - (cdsItemPed.FieldByName('n_valor').AsFloat * (-1)))
            end
            else begin
              novodes := cdsItemPed.FieldByName('n_valor').AsFloat;

              if eConfiguracao.Lines.Values['arredonda'] = 'S' then
                novoprc := Round((novounit * ((100 - cdsItemPed.FieldByName('n_valor').AsFloat) / 100)) * 100) / 100
              else
                novoprc := novounit * ((100 - cdsItemPed.FieldByName('n_valor').AsFloat) / 100);
            end;
          end;

          cdsDeleta.Close;
          cdsDeleta.Params.ParamByName('i_cddesc_com').AsInteger := cdsItemPed.FieldByName('i_cditempedvenda').AsInteger;
          cdsDeleta.Execute;

          cdsUpdateItem.Close;
          cdsUpdateItem.CommandText :=
            ' update ItemPedVenda set             ' +
            '   n_prcvenda = :n_prcvenda,         ' +
            '   n_percDesconto = :n_percDesconto, ' +
            '   n_prcunitario = :n_prcunitario    ' +
            ' where ItemPedVenda.i_cditempedvenda  = :i_cditempedvenda ';
          cdsUpdateItem.Params.ParamByName('n_prcvenda').AsFloat := novoprc;
          cdsUpdateItem.Params.ParamByName('n_prcunitario').AsFloat := novounit;
          cdsUpdateItem.Params.ParamByName('n_percDesconto').AsFloat := novodes;
          cdsUpdateItem.Params.ParamByName('i_cditempedvenda').AsInteger := cdsItemPed.FieldByName('i_cditempedvenda').AsInteger;
          cdsUpdateItem.Execute;

          cdsItemPed.Next;
        end;
      end;

      cdsCalPedido.Close;
      cdsCalPedido.CommandText := 'select calculavalorespedido( ' + cdsPedidoi_nrpedido.AsString + ' )';
      cdsCalPedido.Open;

      SQLConnection.Commit;
    except
      on E: Exception do begin
        SQLConnection.Rollback;
        raise;
      end;
    end;
  end;
end;

procedure TForm1.ExcluiTitulos;
begin
  cdsTitulo.First;
  while not cdsTitulo.Eof do begin
    cdsCriaTitulo.Close;
    cdsCriaTitulo.CommandText := ' Delete from historico_cnab where i_cdtitulo = :i_cdtitulo ';
    cdsCriaTitulo.Params.ParamByName('i_cdtitulo').AsInteger := cdsTitulo.FieldByName('i_cdtitulo').AsInteger;
    cdsCriaTitulo.Execute;

    cdsCriaTitulo.Close;
    cdsCriaTitulo.CommandText := 'Delete from movfinanceira where i_cdmovfinanceira in ( select i_cdmovfinanceira from movtitulo where i_cdtitulo = :i_cdtitulo)';
    cdsCriaTitulo.Params.ParamByName('i_cdtitulo').AsInteger := cdsTitulo.FieldByName('i_cdtitulo').AsInteger;
    cdsCriaTitulo.Execute;

    cdsCriaTitulo.Close;
    cdsCriaTitulo.CommandText := ' Delete from movtitulo where i_cdtitulo = :i_cdtitulo ';
    cdsCriaTitulo.Params.ParamByName('i_cdtitulo').AsInteger := cdsTitulo.FieldByName('i_cdtitulo').AsInteger;
    cdsCriaTitulo.Execute;

    cdsCriaTitulo.Close;
    cdsCriaTitulo.CommandText := ' Delete from contager_titulo where i_cdtitulo = :i_cdtitulo ';
    cdsCriaTitulo.Params.ParamByName('i_cdtitulo').AsInteger := cdsTitulo.FieldByName('i_cdtitulo').AsInteger;
    cdsCriaTitulo.Execute;

    cdsCriaTitulo.Close;
    cdsCriaTitulo.CommandText := ' update Titulo set f_validado = ''N'' where i_cdtitulo = :i_cdtitulo ';
    cdsCriaTitulo.Params.ParamByName('i_cdtitulo').AsInteger := cdsTitulo.FieldByName('i_cdtitulo').AsInteger;
    cdsCriaTitulo.Execute;

    cdsCriaTitulo.Close;
    cdsCriaTitulo.CommandText := ' Delete from Func_Lanc where i_cdtitulocomissao = :i_cdtitulo ';
    cdsCriaTitulo.Params.ParamByName('i_cdtitulo').AsInteger := cdsTitulo.FieldByName('i_cdtitulo').AsInteger;
    cdsCriaTitulo.Execute;

    cdsCriaTitulo.Close;
    cdsCriaTitulo.CommandText := ' Delete from Titulo where i_cdtitulo = :i_cdtitulo ';
    cdsCriaTitulo.Params.ParamByName('i_cdtitulo').AsInteger := cdsTitulo.FieldByName('i_cdtitulo').AsInteger;
    cdsCriaTitulo.Execute;

    cdsTitulo.Next;
  end;
end;

procedure TForm1.bImprimirReciboClick(Sender: TObject);
var
  select: string;
  vReport: string;
  totalproduto: Real;
  desconto: Real;
  boleto: Real;
  ipi: Real;
  icmssubst: Real;
  frete: Real;
  pTotalRec: Real;
  DirServer: string;
  pObs: string;
begin
  pObs := '';
  if ePedido.Text <> '' then begin
    DirServer := ExtractFilePath(Application.ExeName);

    cdsRcItemPedido.Close;

    select :=
      ' select ' +
      '   Pedidovenda.i_qtvolumes, ' +
      '   cast(Pedidovenda.n_despacess as numeric(12,4)) as n_despacess, ' +
      '   cast(Produto.c_codigo as character varying(20)) as c_codigo, ' +
      '   cast(Produto.c_descricao as character varying(50)) as c_descricao, ' +
      '   ItemPedVenda.i_cditempedvenda, ' +
      '   ItemPedVenda.i_cdproduto, ' +
      '   ItemPedVenda.i_ctitem, ' +
      '   cast(Unidade.c_cdunidade as character(2)) as c_cdunidade, ' +
      '   cast(buscaprecovenda(''V'', i_cditempedvenda) as numeric(12,4)) as n_prcunitario, ' +
      '   cast(ItemPedVenda.n_qtdeseparada as numeric(12,4)) as n_qtdeseparada,  ' +
      '   cast(ItemPedVenda.n_quantidade as numeric(12,4)) as n_quantidade, ' +
      '   cast(buscaprecovenda(''D'', i_cditempedvenda) as numeric(12,4)) as n_percdesconto, ' +

      '   cast( ' +
      '     ( ' +
      '       case itempedvenda.f_bonifica when ''N'' then ' +
      '         buscaprecovenda(''P'', i_cditempedvenda) ' +
      '       else ' +
      '         Round(0, 4) ' +
      '       end ' +
      '     ) as numeric(12,4) ' +
      '   ) as PrecoDesc, ' +

      '   cast( ' +
      '     ( ' +
      '       case itempedvenda.f_bonifica when ''N'' then ' +
      '         (buscaprecovenda(''P'', i_cditempedvenda) * ItemPedVenda.n_qtdeseparada) ' +
      '       else ' +
      '         Round(0, 4) ' +
      '       end ' +
      '     ) as numeric(12,4) ' +
      '   ) as total, ' +
      '   cast(itempedvenda.n_valoripi as numeric(12,4)) as n_valoripi, ' +
      '   pedidovenda.f_cobraboleto ' +
      ' from ' +
      '   ItemPedVenda ' +
      ' Join Produto on Produto.i_cdproduto = ItemPedVenda.i_cdProduto ' +
      ' Join Unidade on Unidade.i_cdunidade = Produto.i_cdunidade ' +
      ' Join pedidovenda using(i_nrpedido) ' +
      ' where ' +
      '   I_NrPedido = :pedido and ' +
      '   ItemPedVenda.n_qtdeseparada > 0 ' +
      ' order by ' +
      '   Produto.c_codigo';

    cdsRcItemPedido.CommandText := select;
    cdsRcItemPedido.Params.ParamByName('pedido').AsInteger := StrToInt(ePedido.Text);
    cdsRcItemPedido.Open;

    totalproduto := 0;

    cdsRcItemPedido.First;
    while not(cdsRcItemPedido.Eof) do begin
      totalproduto := totalproduto + cdsRcItemPedido.FieldByName('total').AsFloat;
      ipi := ipi + cdsRcItemPedido.FieldByName('n_valoripi').AsFloat;

      cdsRcItemPedido.Next;
    end;
    cdsRcItemPedido.First;

    cdsRcTipoVend.Close;
    cdsRcTipoVend.CommandText :=
      ' select i_cdvendedor, vendedor.c_nome from pedidovenda ' +
      ' join pedvenda_vendedor using(i_nrpedido) join vendedor using(i_cdvendedor) ' +
      ' where case when f_origem = ''T'' then ''I'' else ''R'' end = f_vendedor ' +
      ' and i_cdvendedor <> 1 and I_NrPedido = ' + Trim(ePedido.Text);
    cdsRcTipoVend.Open;

    cdsParam.Close;
    cdsParam.CommandText := ' select RetParametro(''C_MASCARAPRODUTO'') as c_valor ';
    cdsParam.Open;

    cdsRcItemPedido.FieldByName('c_codigo').EditMask := cdsParam.FieldByName('c_valor').AsString;
    vMascaraProduto := cdsParam.FieldByName('c_valor').AsString;

    cdsRcPedVenda.Close;
    cdsRcPedVenda.Params.ParamByName('pedido').AsInteger := StrToInt(ePedido.Text);
    cdsRcPedVenda.Open;

    desconto :=  RoundTo(totalproduto * (cdsRcPedVenda.FieldByName('n_percdescpedido').AsFloat/100),-2);
    icmssubst := cdsRcPedVenda.FieldByName('n_vlricmssubst').AsFloat;

    cdsDespesas.Close;
    cdsDespesas.CommandText :=
      ' select cast(coalesce(sum(n_valorboleto), 0) as numeric(12,4)) as despesas ' +
      ' from titulo ' +
      ' join contafinanceira using(i_cdcontafin) ' +
      ' where i_nrpedido is null and c_documento like :I_NrPedido ';
    cdsDespesas.Params.ParamByName('I_NrPedido').AsString := Trim(ePedido.Text) + '%';
    cdsDespesas.Open;

    cdsRcPedVenda.Edit;
    cdsRcPedVenda.FieldByName('c_prazo').EditMask := '000/000/000/000/000/000/000/;0;*';

    cdsRcCliente.Close;
    cdsRcCliente.Params.ParamByName('cliente').AsInteger := cdsRcPedVenda.FieldByName('i_cdcliente').AsInteger;
    cdsRcCliente.Open;

    if cdsRcCliente.FieldByName('c_cpf').AsString <> '' then
      cdsRcCliente.FieldByName('c_cpf').EditMask := '000.000.000-00;0;*'
    else
      cdsRcCliente.FieldByName('c_cnpj').EditMask := '00.000.000/0000-00;0;*';

    cdsRcTitulo.Close;
    cdsRcTitulo.CommandText :=
      ' SELECT Titulo.i_cdTitulo, ' +
      '        Titulo.c_documento, ' +
      '        Titulo.d_vencimento, ' +
      '        Titulo.n_vrtitulo, ' +
      '        TipoTitulo.c_descricao, ';

    if cdsRcItemPedido.FieldByName('f_cobraboleto').AsString = 'S' then begin
      cdsRcTitulo.CommandText := cdsRcTitulo.CommandText +
      '        contafinanceira.n_valorboleto ';
    end
    else begin
      cdsRcTitulo.CommandText := cdsRcTitulo.CommandText +
      '        cast(0 as numeric(5,2)) as n_valorboleto ';
    end;

    cdsRcTitulo.CommandText := cdsRcTitulo.CommandText +
      ' FROM Titulo ' +
      ' JOIN tipotitulo ON tipotitulo.i_cdtipotitulo = titulo.i_cdtipotitulo ' +
      ' JOIN contafinanceira using(i_cdcontafin) ' +
      ' WHERE f_validado = ''S'' ' +
      ' AND c_documento LIKE :c_documento ' +
      ' AND i_cdparceiro = :i_cdparceiro ' +
      ' AND i_cdempresa = :i_cdempresa ' +
      ' ORDER BY Titulo.d_vencimento ';

    if ProgPedVenda = 'CADPEDIDOVENDABALCAO' then begin
      if cdsPedidoi_fatura.AsInteger > 0 then
        cdsRcTitulo.Params.ParamByName('c_documento').AsString := Trim(cdsPedidoi_fatura.AsString) + '-%'
      else
        cdsRcTitulo.Params.ParamByName('c_documento').AsString := Trim(ePedido.Text) + '-%';
    end
    else
      cdsRcTitulo.Params.ParamByName('c_documento').AsString := Trim(ePedido.Text) + '-%';
    cdsRcTitulo.Params.ParamByName('i_cdparceiro').AsInteger := StrToInt(cdsRcPedVenda.FieldByName('i_cdcliente').AsString + '1');
    cdsRcTitulo.Params.ParamByName('i_cdempresa').AsInteger := cdsRcPedVenda.FieldByName('i_cdempresa').AsInteger;
    cdsRcTitulo.Open;

    boleto := 0;
    cdsRcTitulo.First;
    while not(cdsRcTitulo.Eof) do begin
      boleto := boleto + cdsRcTitulo.FieldByName('n_valorboleto').AsFloat;
      cdsRcTitulo.Next;
    end;

    boleto := boleto + cdsPedidon_despesasadd.Value;

    cdsRcTitulo.First;

    // Impress�o do relatorio via Fast report
    if MessageBox(0, PChar('Deseja exibir a observa��o do pedido?'), 'Confirma��o', MB_YESNO + MB_DEFBUTTON2 + MB_ICONQUESTION + MB_TASKMODAL) = IDYES then
      pObs := 'Obs.: ' + Trim(cdsRcPedVenda.FieldByName('c_observacao').AsString);

    if not(cbRelAntigo.Checked) then begin
      vFileReport := 'fr_relcpf.fr3';
      vClickMouse := False;

      if cdsRcPedVendaf_tpfrete.AsString = 'Incluso' then
        frete := cdsRcPedVendan_custofrete.AsFloat
      else if cdsRcPedVendaf_tpfrete.AsString = 'Pago' then
        frete := 0;

      pTotalRec := totalproduto - desconto + boleto + ipi + icmssubst + frete;
      // Verifica se existe relatorios proprios da empresa (prefixo "own_")
      if Pos('.fr3', vFileReport) > 0 then
        AtualizarArquivo(DirServer, 'own_' + vFileReport)
      else
        AtualizarArquivo(DirServer, 'own_' + vFileReport + '.fr3');

      if FileExists(ExtractFilePath(Application.ExeName) + 'own_' + vFileReport) then begin
        frxReport.Clear;

        if frxReport.LoadFromFile(ExtractFilePath(Application.ExeName) + 'own_' + vFileReport) then begin
          frxReport.Variables['pTotalRec'] := '''' + FormatFloat('0.00', pTotalRec) + '''';
          frxReport.Variables['pBoleto'] := '''' + FormatFloat('0.00', boleto) + '''';
          frxReport.Variables['pObs'] := '''' + pObs + '''';
          frxReport.Variables['pMascaraProduto'] := '''' + vMascaraProduto + '''';
        end;
      end
      else begin
        // Se n�o existir buscar relatorio normal
        if Pos('.fr3', vFileReport) > 0 then
          AtualizarArquivo(DirServer, vFileReport)
        else
          AtualizarArquivo(DirServer, vFileReport + '.fr3');

        frxReport.Clear;

        if frxReport.LoadFromFile(ExtractFilePath(Application.ExeName) + vFileReport) then begin
          frxReport.Variables['pTotalRec'] := '''' + FormatFloat('0.00', pTotalRec) + '''';
          frxReport.Variables['pBoleto'] := '''' + FormatFloat('0.00', boleto) + '''';
          frxReport.Variables['pObs'] := '''' + pObs + '''';
          frxReport.Variables['pMascaraProduto'] := '''' + vMascaraProduto + '''';
        end
        else begin
          ShowMessage('Arquivo '+ vFileReport + ' n�o foi encontrado na pasta do programa ');
          //+ getParametro(Application, 'DirServer'));
          Exit;
        end;
      end;

      if not(vClick) then begin
        if not(FileExists(ExtractFilePath(Application.ExeName) + 'own_' + vFileReport)) then begin
          frxReport.Clear;

          CopyFile(
            PChar(ExtractFilePath(Application.ExeName) + vFileReport),
            PChar(ExtractFilePath(Application.ExeName) + 'own_' + vFileReport),
            False
          );

          if frxReport.LoadFromFile(ExtractFilePath(Application.ExeName) + 'own_' + vFileReport) then begin
            frxReport.Variables['pTotalRec'] := '''' + FormatFloat('0.00', pTotalRec) + '''';
            frxReport.Variables['pBoleto'] := '''' + FormatFloat('0.00', boleto) + '''';
            frxReport.Variables['pObs'] := '''' + pObs + '''';
            frxReport.Variables['pMascaraProduto'] := '''' + vMascaraProduto + '''';
          end;
        end;

        frxReport.DesignReport(True, False);
       // AtualizarArquivoServer(DirServer, 'own_' + vFileReport);

        vClick := True;
      end;

      if frxReport.PrepareReport then
        frxReport.ShowPreparedReport
      else
        ShowMessage('Arquivo n�o pode ser impresso');
    end
    else begin
      RvSystem1.DefaultDest := rdPreview;
      RvSystem1.SystemSetups := RvSystem1.SystemSetups - [ssAllowSetup];

      if eConfiguracao.Lines.Values['RptRec'] <> '' then
        vReport := 'RptRec_' + eConfiguracao.Lines.Values['RptRec']
      else
        vReport := 'RptRec';

      RvProject1.SetParam('pObs', pObs);
      RvProject1.SetParam('pDespAcessorias', FormatFloat('#,##0.00', DespesasAcessorias));
      RvProject1.SetParam('pValorTotComIPI', eValor.Text);
      RvProject1.SetParam('pTotalRec', FormatFloat('#,##0.00', pTotalRec));

      RvProject1.SelectReport(vReport, True);
      RvSystem1.RenderObject := RvRenderPDF1;
      RvProject1.Execute;
    end;
  end
  else
    MessageDlg('Digite o c�digo para impress�o!', mtError, [mbOK], 0);
end;

procedure TForm1.bImprimirReciboMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if (ssLeft in Shift) and (ssCtrl in Shift) then
    vClick := False;
end;

function TForm1.DespesasAcessorias: Double;
var
  vlrBoleto: Double;
begin
  if cdsPedidof_cobraboleto.Value = 'S' then begin
    cdsValorPedido.Close;
    cdsValorPedido.CommandText :=
      ' select ' +
      '   N_ValorBoleto ' +
      ' from ' +
      '   ContaFinanceira ' +
      '	where ' +
      '   I_CdContaFin = :I_CdContaFin ';
    cdsValorPedido.Params.ParamByName('i_cdcontafin').AsInteger := StrToInt(eConfiguracao.Lines.Values['contafin']);
    cdsValorPedido.Open;

    vlrBoleto := cdsValorPedido.FieldByName('n_valorboleto').AsFloat;
  end
  else
    vlrBoleto := 0;

  cdsValorPedido.Close;
  cdsValorPedido.CommandText :=
    ' select ' +
    '   n_despacess ' +
    ' from ' +
    '   pedidovenda ' +
    ' where ' +
    '   i_nrpedido = :I_NrPedido ';
  cdsValorPedido.Params.ParamByName('I_NrPedido').AsInteger := StrToInt(ePedido.Text);
  cdsValorPedido.Open;

  if vlrBoleto > 0 then
    Result := (vlrBoleto * cdsTitulo.RecordCount) + cdsValorPedido.FieldByName('n_despacess').AsFloat
  else
    Result := 0;
end;

function TForm1.BuscaNovosValoreSPED: Double;
var
  valor: Double;
  calcprc: Double;
  vlrVenda: Double;

  vPrc_Custo_Produto: string;
begin
  if (StrToIntDef(eConfiguracao.Lines.values['Margem_Min_Lucro'],0) > 0) and (vlrdesconto <> 100) then begin
    vPrc_Custo_Produto := eConfiguracao.Lines.values['PRC_CUSTO_PRODUTO'];
    valor := 0;

    cdsItemPed.Close;
    cdsItemPed.CommandText :=
      ' select ' +
      '   case ''' + vPrc_Custo_Produto + ''' when ''L'' then ' +
      '     prc.n_precocusto ' +
      '   else ' +
      '     prod.n_ultimoprecocompra ' +
      '   end as n_precocusto, ' +
      '   ipv.n_prcvenda, ' +
      '   ipv.n_qtdeseparada ' +
      ' from ' +
      '   itempedvenda ipv ' +
      ' join produto prod using(i_cdproduto) ' +
      ' join preco prc on prc.i_cdproduto = prod.i_cdproduto and prc.i_cdlista_preco = ipv.i_cdlista_preco ' +
      ' where ipv.i_nrpedido = :I_NrPedido ' +
      ' and ipv.f_bonifica = ''N'' ';
    cdsItemPed.Params.parambyname('I_NrPedido').AsInteger := StrToInt(ePedido.Text);
    cdsItemPed.Open;

    cdsItemPed.First;
    while not cdsItemPed.Eof do begin
      vlrVenda := (cdsItemPed.FieldByName('n_prcvenda').AsFloat * cdsItemPed.FieldByName('n_qtdeseparada').AsFloat);

      if
        cdsItemPed.FieldByName('n_precocusto').AsFloat * (1 + (StrToInt(eConfiguracao.Lines.values['Margem_Min_Lucro']) / 100))
        >
        (cdsItemPed.FieldByName('n_prcvenda').AsFloat * (1 - (vlrdesconto/100)))
      then begin
        calcprc :=
          cdsItemPed.FieldByName('n_qtdeseparada').AsFloat *
          (cdsItemPed.FieldByName('n_precocusto').AsFloat * (1 + (StrToInt(eConfiguracao.Lines.values['Margem_Min_Lucro']) / 100)));

        if calcprc <= vlrVenda then
          valor := valor + (vlrVenda - calcprc);
      end
      else
        valor := valor + (cdsItemPed.FieldByName('n_qtdeseparada').AsFloat * cdsItemPed.FieldByName('n_prcvenda').AsFloat * (vlrdesconto / 100));

      cdsItemPed.Next;
    end;
  end
  else
  begin
   if (vlrdesconto <> 100) then
     valor := (cdsPedidon_vlrseparado.AsFloat - cdsPedidon_vlrdesconto.AsFloat) * (vlrdesconto / 100)
   else  // Caso seja 100 desconto
     valor := (cdsPedidon_vlrseparado.AsFloat - cdsPedidon_vlrdesconto.AsFloat) + cdsPedidon_freteincluso.AsFloat + cdsPedidon_despesasadd.AsFloat;
  end;



  if valor > 0 then
    Result := valor
  else begin
    MessageDlg('Nenhum item desse pedido foi separado.', mtInformation, [mbOK], 0);
    Abort;
  end;
end;

procedure TForm1.ExcluiDescCom;
begin
  cdsDiasVerificacao.Close;
  cdsDiasVerificacao.CommandText :=
    ' delete from desc_com where i_cddesc_com in ( ' +
    '   select ' +
    '     dc.i_cddesc_com ' +
    '   from ' +
    '     desc_com dc ' +
    '   join itempedvenda iv on iv.i_cditempedvenda = dc.i_cddesc_com ' +
	  '   join pedidovenda pv on pv.i_nrpedido = iv.i_nrpedido ' +
	  '   where date(pv.d_cadastro) < :data) ';
  cdsDiasVerificacao.Params.ParamByName('data').AsDate := Now - vDias_Verificacao;
  cdsDiasVerificacao.Execute;
end;

procedure TForm1.bFechaGerClick(Sender: TObject);
begin
  if (eDataGenInicial.Text = '') or (eDataGenFinal.Text = '') then begin
    MessageDlg('Informe as datas!', mtInformation, [mbOK], 0);
    Exit;
  end;

  cdsVerificaMes.Close;
  cdsVerificaMes.CommandText := ' select count(1) as tot from Fecha_gerencial where i_ano = :i_ano and i_mes = :i_mes ';
  cdsVerificaMes.Params.ParamByName('i_Mes').AsInteger := StrToInt(FormatDateTime('mm',StrToDate(eDataGenInicial.Text)));
  cdsVerificaMes.Params.ParamByName('i_Ano').AsInteger := StrToInt(FormatDateTime('yyyy',StrToDate(eDataGenInicial.Text)));
  cdsVerificaMes.Open;

  if cdsVerificaMes.FieldByName('tot').AsInteger > 0 then begin
    if MessageDlg('Este M�s j� foi fechado, Fechar novamente?', mtConfirmation, [mbYes, mbNo], 0) = mrNo then
      Exit;
  end;

  if (eDataGenInicial.Text <> '') and (eDataGenFinal.Text <> '') then begin
    cdsFechaGer.Close;
    cdsFechaGer.CommandText :=
      ' select FechaGerencial( ' +
        FormatDateTime('mm', StrToDate(eDataGenInicial.Text)) + ', ' +
        FormatDateTime('yyyy', StrToDate(eDataGenInicial.Text)) +
      ')';
    cdsFechaGer.Open;
  end;

  VerificaUltimoFechamento;
end;

procedure TForm1.Conectar;
var
  i: Integer;
begin
  if SQLConnection.Connected then
    SQLConnection.Connected := False;

  if not(SQLConnection.Connected) then begin
    if not FileExists('cpf.ini') or (eConfiguracao.Lines.Count = 0) then
      Self.Close;

    SQLConnection.Connected := False;

    SQLConnection.Params.Clear;
    SQLConnection.Params.Values['DataBase'] := eConfiguracao.Lines.Values['Database'];
    SQLConnection.Params.Values['User_Name'] := eConfiguracao.Lines.Values['User_Name'];
    SQLConnection.Params.Values['Password'] := eConfiguracao.Lines.Values['Password'];
    SQLConnection.Params.Values['Server'] := eConfiguracao.Lines.Values['HostName'];
    SQLConnection.Params.Values['DriverID'] := 'PG';
    SQLConnection.Params.Values['Port'] := eConfiguracao.Lines.Values['Port'];

    SQLConnection.Connected := True;
  end;
end;

procedure TForm1.ConfiguracaoChange(Sender: TObject);
begin
  if Configuracao.TabIndex = 2 then begin
    VerificaUltimoFechamento;

    cdsEmpresa.Close;
    cdsEmpresa.CommandText := ' select i_cdempresa from empresa order by i_cdempresa ';
    cdsEmpresa.Open;

    if cdsEmpresa.RecordCount = 1 then begin
      Label15.Visible := False;
      ComboBoxEmp.Visible := False;
    end
    else if cdsEmpresa.RecordCount > 1 then begin
      cdsEmpresa.First;

      while not cdsEmpresa.Eof do begin
        ComboBoxEmp.Items.Add(cdsEmpresa.FieldByName('i_cdempresa').AsString);
        cdsEmpresa.Next;
      end;

      Label15.Visible := True;
      ComboBoxEmp.Visible := True;
    end;
  end;

 if Configuracao.TabIndex = 1 then
   bSalvaConfg.Visible := True
 else
   bSalvaConfg.Visible := False;
end;

procedure TForm1.eDataGenInicialExit(Sender: TObject);
var
  dataFinal: TDate;
  dataInicial: TDate;

  dia: Word;
  mes: Word;
  ano: Word;
begin
  try
    StrToDate(eDataGenInicial.Text);
  except
    Exit;
  end;

  if eDataGenInicial.Text = '' then
    Exit;

  DecodeDate(StrToDate(eDataGenInicial.Text), ano, mes, dia);

  dataInicial := EncodeDate(ano, mes, 1);
  dataFinal := IncMonth(dataInicial, 1);
  dataFinal := IncDay(dataFinal, -1);
  eDataGenFinal.Text := DateToStr(dataFinal);
end;

procedure TForm1.ValidarMediaAtrasoCliente;
var
  _i_cdcliente: Integer;
  _i_media_atraso_cliente: Integer;
  _i_media_atraso_param: Integer;
  _s_classificacao: string;
begin
  qSQL.Close;
  qSQL.SQL.Clear;
  qSQL.SQL.Add('select');
  qSQL.SQL.Add('  i_cdcliente');
  qSQL.SQL.Add('from');
  qSQL.SQL.Add('  pedidovenda');
  qSQL.SQL.Add('where');
  qSQL.SQL.Add('  i_nrpedido = :i_nrpedido');
  qSQL.ParamByName('i_nrpedido').AsInteger := StrToInt(ePedido.Text);
  qSQL.Open;

  _i_cdcliente := qSQL.FieldByName('i_cdcliente').AsInteger;

  qSQL.Close;
  qSQL.SQL.Clear;
  qSQL.SQL.Add('select');
  qSQL.SQL.Add('  i_mediaatraso,');
  qSQL.SQL.Add('  f_classificacao');
  qSQL.SQL.Add('from');
  qSQL.SQL.Add('  infcomercial');
  qSQL.SQL.Add('where');
  qSQL.SQL.Add('  i_cdcliente = :i_cdcliente');
  qSQL.ParamByName('i_cdcliente').AsInteger := _i_cdcliente;
  qSQL.Open;

  _i_media_atraso_cliente := qSQL.FieldByName('i_mediaatraso').AsInteger;
  _s_classificacao := qSQL.FieldByName('f_classificacao').AsString;

  qSQL.Close;
  qSQL.SQL.Clear;
  qSQL.SQL.Add('select');
  qSQL.SQL.Add('  c_valor');
  qSQL.SQL.Add('from');
  qSQL.SQL.Add('  parametro');
  qSQL.SQL.Add('where');
  qSQL.SQL.Add('  c_nome = ''MEDIAATRASOCLIENTEBOM'' ');
  qSQL.Open;

  _i_media_atraso_param := StrToInt(qSQL.FieldByName('c_valor').AsString);

  if (_i_media_atraso_cliente > _i_media_atraso_param) and (_s_classificacao = 'N') then begin
    if MessageDlg('O cliente possui m�dia de atraso MAIOR que a m�dia de atraso parametrizada no sistema. Deseja continuar?', mtConfirmation, [mbYes, mbNo], 0) = mrNo then
      Abort;
  end;
end;

procedure TForm1.VerificaUltimoFechamento;
begin
  cdsVerificaMes.Close;
  cdsVerificaMes.CommandText := ' select Max((i_ano * 100) + (i_mes)) as Data from Fecha_gerencial ';
  cdsVerificaMes.Open;

  lData.Caption := Copy(cdsVerificaMes.FieldByName('Data').AsString, 5, 2) + '/' + Copy(cdsVerificaMes.FieldByName('Data').AsString,1,4);
end;

procedure TForm1.bRelGenrencialClick(Sender: TObject);
var
  sql: string;
  vAno: string;
  vReport: string;
begin
  if eAno.Text = '' then begin
    MessageDlg('Informe o ano!', mtInformation, [mbOK], 0);
    Exit;
  end;

  vAno := eAno.Text;

  sql :=
    ' select ' +
    '   C_CONTA, ' +
    '   C_TITULOCONTA, ' +
    '   f_analitico, ' +
    '   i_cdempresa, ' +
    '   c_nome, ' +

    '   sum(n_tot_custo_fatur1) as n_tot_custo_fatur1, ' +
    '   sum(n_tot_custo_fatur2) as n_tot_custo_fatur2, ' +
    '   sum(n_tot_custo_fatur3) as n_tot_custo_fatur3, ' +
    '   sum(n_tot_custo_fatur4) as n_tot_custo_fatur4, ' +
    '   sum(n_tot_custo_fatur5) as n_tot_custo_fatur5, ' +
    '   sum(n_tot_custo_fatur6) as n_tot_custo_fatur6, ' +
    '   sum(n_tot_custo_fatur7) as n_tot_custo_fatur7, ' +
    '   sum(n_tot_custo_fatur8) as n_tot_custo_fatur8, ' +
    '   sum(n_tot_custo_fatur9) as n_tot_custo_fatur9, ' +
    '   sum(n_tot_custo_fatur10) as n_tot_custo_fatur10, ' +
    '   sum(n_tot_custo_fatur11) as n_tot_custo_fatur11, ' +
    '   sum(n_tot_custo_fatur12) as n_tot_custo_fatur12, ' +

    '   sum(n_tot_comissao1) as n_tot_comissao1, ' +
    '   sum(n_tot_comissao2) as n_tot_comissao2, ' +
    '   sum(n_tot_comissao3) as n_tot_comissao3, ' +
    '   sum(n_tot_comissao4) as n_tot_comissao4, ' +
    '   sum(n_tot_comissao5) as n_tot_comissao5, ' +
    '   sum(n_tot_comissao6) as n_tot_comissao6, ' +
    '   sum(n_tot_comissao7) as n_tot_comissao7, ' +
    '   sum(n_tot_comissao8) as n_tot_comissao8, ' +
    '   sum(n_tot_comissao9) as n_tot_comissao9, ' +
    '   sum(n_tot_comissao10) as n_tot_comissao10, ' +
    '   sum(n_tot_comissao11) as n_tot_comissao11, ' +
    '   sum(n_tot_comissao12) as n_tot_comissao12, ' +

    '   sum(n_tot_faturado1) as n_tot_faturado1, ' +
    '   sum(n_tot_faturado2) as n_tot_faturado2, ' +
    '   sum(n_tot_faturado3) as n_tot_faturado3, ' +
    '   sum(n_tot_faturado4) as n_tot_faturado4, ' +
    '   sum(n_tot_faturado5) as n_tot_faturado5, ' +
    '   sum(n_tot_faturado6) as n_tot_faturado6, ' +
    '   sum(n_tot_faturado7) as n_tot_faturado7, ' +
    '   sum(n_tot_faturado8) as n_tot_faturado8, ' +
    '   sum(n_tot_faturado9) as n_tot_faturado9, ' +
    '   sum(n_tot_faturado10) as n_tot_faturado10, ' +
    '   sum(n_tot_faturado11) as n_tot_faturado11, ' +
    '   sum(n_tot_faturado12) as n_tot_faturado12, ' +

    '   sum(margemcont1) as margemcont1, ' +
    '   sum(margemcont2) as margemcont2, ' +
    '   sum(margemcont3) as margemcont3, ' +
    '   sum(margemcont4) as margemcont4, ' +
    '   sum(margemcont5) as margemcont5, ' +
    '   sum(margemcont6) as margemcont6, ' +
    '   sum(margemcont7) as margemcont7, ' +
    '   sum(margemcont8) as margemcont8, ' +
    '   sum(margemcont9) as margemcont9, ' +
    '   sum(margemcont10) as margemcont10, ' +
    '   sum(margemcont11) as margemcont11, ' +
    '   sum(margemcont12) as margemcont12, ' +

    '   sum(valor1) as valor1, ' +
    '   sum(valor2) as valor2, ' +
    '   sum(valor3) as valor3, ' +
    '   sum(valor4) as valor4, ' +
    '   sum(valor5) as valor5, ' +
    '   sum(valor6) as valor6, ' +
    '   sum(valor7) as valor7, ' +
    '   sum(valor8) as valor8, ' +
    '   sum(valor9) as valor9, ' +
    '   sum(valor10) as valor10, ' +
    '   sum(valor11) as valor11, ' +
    '   sum(valor12) as valor12 ' +
    ' from( ';

  sql := sql +  BuscaSQLMes('01', vAno) + ' union all ' ;
  sql := sql +  BuscaSQLMes('02', vAno) + ' union all ' ;
  sql := sql +  BuscaSQLMes('03', vAno) + ' union all ' ;
  sql := sql +  BuscaSQLMes('04', vAno) + ' union all ' ;
  sql := sql +  BuscaSQLMes('05', vAno) + ' union all ' ;
  sql := sql +  BuscaSQLMes('06', vAno) + ' union all ' ;
  sql := sql +  BuscaSQLMes('07', vAno) + ' union all ' ;
  sql := sql +  BuscaSQLMes('08', vAno) + ' union all ' ;
  sql := sql +  BuscaSQLMes('09', vAno) + ' union all ' ;
  sql := sql +  BuscaSQLMes('10', vAno) + ' union all ' ;
  sql := sql +  BuscaSQLMes('11', vAno) + ' union all ' ;
  sql := sql +  BuscaSQLMes('12', vAno);

  sql := sql +
    ' ) as tab '+
    ' group by c_nome, C_CONTA, C_TITULOCONTA, f_analitico, i_cdempresa '+
    ' order by tab.i_cdempresa, tab.c_conta ';

  cdsDespesas.Close;
  cdsDespesas.CommandText := sql;
  if ComboBoxEmp.ItemIndex > 0 then
    cdsDespesas.Params.ParamByName('I_CdEmpresa').AsInteger := StrToInt(ComboBoxEmp.Text);
  cdsDespesas.Open;

  vReport := 'RptFechaGer';
  RvSystem2.DefaultDest := rdPreview;
  RvSystem2.SystemSetups := RvSystem1.SystemSetups - [ssAllowSetup];
  RvProject2.SetParam('pAno', eAno.Text);
  RvProject2.SelectReport(vReport, True);
  RvProject2.Execute;
end;

function TForm1.BuscaSQLMes(mes,ano: string): string;
var
  sql: string;
  dataini: string;
  datafin: string;

  vdia: Word;
  vmes: Word;
  vano: Word;

  dataFinal: TDate;
  dataInicial: TDate;
begin
  dataini := '01/' + mes + '/' + ano;
  DecodeDate(StrToDate(dataini), vano, vmes, vdia);

  dataInicial := EncodeDate(vano,vmes,1);
  dataFinal := incmonth(dataInicial,1);
  dataFinal := IncDay(dataFinal,-1);

  datafin := DateToStr(dataFinal);
  datafin := FormatDateTime('yyyy', StrToDate(datafin)) + FormatDateTime('mm', StrToDate(datafin)) + FormatDateTime('dd', StrToDate(datafin));
  dataini := FormatDateTime('yyyy', StrToDate(dataini)) + FormatDateTime('mm', StrToDate(dataini)) + FormatDateTime('dd', StrToDate(dataini));

  sql := ' select pc.C_CONTA, pc.C_TITULOCONTA, pc.f_analitico, valores.i_cdempresa, empresa.c_nome, ';

  if mes = '01' then sql := sql + ' Sum(fecha_gerencial.n_tot_custo_fatur) as n_tot_custo_fatur1,' else sql := sql + ' 0 as n_tot_custo_fatur1,';
  if mes = '02' then sql := sql + ' Sum(fecha_gerencial.n_tot_custo_fatur) as n_tot_custo_fatur2,' else sql := sql + ' 0 as n_tot_custo_fatur2,';
  if mes = '03' then sql := sql + ' Sum(fecha_gerencial.n_tot_custo_fatur) as n_tot_custo_fatur3,' else sql := sql + ' 0 as n_tot_custo_fatur3,';
  if mes = '04' then sql := sql + ' Sum(fecha_gerencial.n_tot_custo_fatur) as n_tot_custo_fatur4,' else sql := sql + ' 0 as n_tot_custo_fatur4,';
  if mes = '05' then sql := sql + ' Sum(fecha_gerencial.n_tot_custo_fatur) as n_tot_custo_fatur5,' else sql := sql + ' 0 as n_tot_custo_fatur5,';
  if mes = '06' then sql := sql + ' Sum(fecha_gerencial.n_tot_custo_fatur) as n_tot_custo_fatur6,' else sql := sql + ' 0 as n_tot_custo_fatur6,';
  if mes = '07' then sql := sql + ' Sum(fecha_gerencial.n_tot_custo_fatur) as n_tot_custo_fatur7,' else sql := sql + ' 0 as n_tot_custo_fatur7,';
  if mes = '08' then sql := sql + ' Sum(fecha_gerencial.n_tot_custo_fatur) as n_tot_custo_fatur8,' else sql := sql + ' 0 as n_tot_custo_fatur8,';
  if mes = '09' then sql := sql + ' Sum(fecha_gerencial.n_tot_custo_fatur) as n_tot_custo_fatur9,' else sql := sql + ' 0 as n_tot_custo_fatur9,';
  if mes = '10' then sql := sql + ' Sum(fecha_gerencial.n_tot_custo_fatur) as n_tot_custo_fatur10,' else sql := sql + ' 0 as n_tot_custo_fatur10,';
  if mes = '11' then sql := sql + ' Sum(fecha_gerencial.n_tot_custo_fatur) as n_tot_custo_fatur11,' else sql := sql + ' 0 as n_tot_custo_fatur11,';
  if mes = '12' then sql := sql + ' Sum(fecha_gerencial.n_tot_custo_fatur) as n_tot_custo_fatur12,' else sql := sql + ' 0 as n_tot_custo_fatur12,';

  if mes = '01' then sql := sql + ' Sum(fecha_gerencial.n_tot_comissao) as n_tot_comissao1,' else sql := sql + ' 0 as n_tot_comissao1,';
  if mes = '02' then sql := sql + ' Sum(fecha_gerencial.n_tot_comissao) as n_tot_comissao2,' else sql := sql + ' 0 as n_tot_comissao2,';
  if mes = '03' then sql := sql + ' Sum(fecha_gerencial.n_tot_comissao) as n_tot_comissao3,' else sql := sql + ' 0 as n_tot_comissao3,';
  if mes = '04' then sql := sql + ' Sum(fecha_gerencial.n_tot_comissao) as n_tot_comissao4,' else sql := sql + ' 0 as n_tot_comissao4,';
  if mes = '05' then sql := sql + ' Sum(fecha_gerencial.n_tot_comissao) as n_tot_comissao5,' else sql := sql + ' 0 as n_tot_comissao5,';
  if mes = '06' then sql := sql + ' Sum(fecha_gerencial.n_tot_comissao) as n_tot_comissao6,' else sql := sql + ' 0 as n_tot_comissao6,';
  if mes = '07' then sql := sql + ' Sum(fecha_gerencial.n_tot_comissao) as n_tot_comissao7,' else sql := sql + ' 0 as n_tot_comissao7,';
  if mes = '08' then sql := sql + ' Sum(fecha_gerencial.n_tot_comissao) as n_tot_comissao8,' else sql := sql + ' 0 as n_tot_comissao8,';
  if mes = '09' then sql := sql + ' Sum(fecha_gerencial.n_tot_comissao) as n_tot_comissao9,' else sql := sql + ' 0 as n_tot_comissao9,';
  if mes = '10' then sql := sql + ' Sum(fecha_gerencial.n_tot_comissao) as n_tot_comissao10,' else sql := sql + ' 0 as n_tot_comissao10,';
  if mes = '11' then sql := sql + ' Sum(fecha_gerencial.n_tot_comissao) as n_tot_comissao11,' else sql := sql + ' 0 as n_tot_comissao11,';
  if mes = '12' then sql := sql + ' Sum(fecha_gerencial.n_tot_comissao) as n_tot_comissao12,' else sql := sql + ' 0 as n_tot_comissao12,';

  if mes = '01' then sql := sql + ' Sum(fecha_gerencial.n_tot_faturado) as n_tot_faturado1,' else sql := sql + ' 0 as n_tot_faturado1,';
  if mes = '02' then sql := sql + ' Sum(fecha_gerencial.n_tot_faturado) as n_tot_faturado2,' else sql := sql + ' 0 as n_tot_faturado2,';
  if mes = '03' then sql := sql + ' Sum(fecha_gerencial.n_tot_faturado) as n_tot_faturado3,' else sql := sql + ' 0 as n_tot_faturado3,';
  if mes = '04' then sql := sql + ' Sum(fecha_gerencial.n_tot_faturado) as n_tot_faturado4,' else sql := sql + ' 0 as n_tot_faturado4,';
  if mes = '05' then sql := sql + ' Sum(fecha_gerencial.n_tot_faturado) as n_tot_faturado5,' else sql := sql + ' 0 as n_tot_faturado5,';
  if mes = '06' then sql := sql + ' Sum(fecha_gerencial.n_tot_faturado) as n_tot_faturado6,' else sql := sql + ' 0 as n_tot_faturado6,';
  if mes = '07' then sql := sql + ' Sum(fecha_gerencial.n_tot_faturado) as n_tot_faturado7,' else sql := sql + ' 0 as n_tot_faturado7,';
  if mes = '08' then sql := sql + ' Sum(fecha_gerencial.n_tot_faturado) as n_tot_faturado8,' else sql := sql + ' 0 as n_tot_faturado8,';
  if mes = '09' then sql := sql + ' Sum(fecha_gerencial.n_tot_faturado) as n_tot_faturado9,' else sql := sql + ' 0 as n_tot_faturado9,';
  if mes = '10' then sql := sql + ' Sum(fecha_gerencial.n_tot_faturado) as n_tot_faturado10,' else sql := sql + ' 0 as n_tot_faturado10,';
  if mes = '11' then sql := sql + ' Sum(fecha_gerencial.n_tot_faturado) as n_tot_faturado11,' else sql := sql + ' 0 as n_tot_faturado11,';
  if mes = '12' then sql := sql + ' Sum(fecha_gerencial.n_tot_faturado) as n_tot_faturado12,' else sql := sql + ' 0 as n_tot_faturado12,';

  if mes = '01' then sql := sql + ' (sum(n_tot_faturado) - sum(n_tot_comissao) - sum(n_tot_custo_fatur)) as margemcont1,' else sql := sql + ' 0 as margemcont1,';
  if mes = '02' then sql := sql + ' (sum(n_tot_faturado) - sum(n_tot_comissao) - sum(n_tot_custo_fatur)) as margemcont2,' else sql := sql + ' 0 as margemcont2,';
  if mes = '03' then sql := sql + ' (sum(n_tot_faturado) - sum(n_tot_comissao) - sum(n_tot_custo_fatur)) as margemcont3,' else sql := sql + ' 0 as margemcont3,';
  if mes = '04' then sql := sql + ' (sum(n_tot_faturado) - sum(n_tot_comissao) - sum(n_tot_custo_fatur)) as margemcont4,' else sql := sql + ' 0 as margemcont4,';
  if mes = '05' then sql := sql + ' (sum(n_tot_faturado) - sum(n_tot_comissao) - sum(n_tot_custo_fatur)) as margemcont5,' else sql := sql + ' 0 as margemcont5,';
  if mes = '06' then sql := sql + ' (sum(n_tot_faturado) - sum(n_tot_comissao) - sum(n_tot_custo_fatur)) as margemcont6,' else sql := sql + ' 0 as margemcont6,';
  if mes = '07' then sql := sql + ' (sum(n_tot_faturado) - sum(n_tot_comissao) - sum(n_tot_custo_fatur)) as margemcont7,' else sql := sql + ' 0 as margemcont7,';
  if mes = '08' then sql := sql + ' (sum(n_tot_faturado) - sum(n_tot_comissao) - sum(n_tot_custo_fatur)) as margemcont8,' else sql := sql + ' 0 as margemcont8,';
  if mes = '09' then sql := sql + ' (sum(n_tot_faturado) - sum(n_tot_comissao) - sum(n_tot_custo_fatur)) as margemcont9,' else sql := sql + ' 0 as margemcont9,';
  if mes = '10' then sql := sql + ' (sum(n_tot_faturado) - sum(n_tot_comissao) - sum(n_tot_custo_fatur)) as margemcont10,' else sql := sql + ' 0 as margemcont10,';
  if mes = '11' then sql := sql + ' (sum(n_tot_faturado) - sum(n_tot_comissao) - sum(n_tot_custo_fatur)) as margemcont11,' else sql := sql + ' 0 as margemcont11,';
  if mes = '12' then sql := sql + ' (sum(n_tot_faturado) - sum(n_tot_comissao) - sum(n_tot_custo_fatur)) as margemcont12,' else sql := sql + ' 0 as margemcont12,';

  if mes = '01' then sql := sql + ' valor as valor1,' else sql := sql + ' 0 as valor1,';
  if mes = '02' then sql := sql + ' valor as valor2,' else sql := sql + ' 0 as valor2,';
  if mes = '03' then sql := sql + ' valor as valor3,' else sql := sql + ' 0 as valor3,';
  if mes = '04' then sql := sql + ' valor as valor4,' else sql := sql + ' 0 as valor4,';
  if mes = '05' then sql := sql + ' valor as valor5,' else sql := sql + ' 0 as valor5,';
  if mes = '06' then sql := sql + ' valor as valor6,' else sql := sql + ' 0 as valor6,';
  if mes = '07' then sql := sql + ' valor as valor7,' else sql := sql + ' 0 as valor7,';
  if mes = '08' then sql := sql + ' valor as valor8,' else sql := sql + ' 0 as valor8,';
  if mes = '09' then sql := sql + ' valor as valor9,' else sql := sql + ' 0 as valor9,';
  if mes = '10' then sql := sql + ' valor as valor10,' else sql := sql + ' 0 as valor10,';
  if mes = '11' then sql := sql + ' valor as valor11,' else sql := sql + ' 0 as valor11,';
  if mes = '12' then sql := sql + ' valor as valor12 ' else sql := sql + ' 0 as valor12 ';

 	sql := sql +
    ' from ( ' +
      ' select ct.i_cdplanoconta, sum(Trunc(ti.n_vracerto * Round((ct.n_valor/ti.n_vrtitulo),4),2)) as valor, ti.i_cdempresa ' +
		  ' from titulo ti ' +
      ' join contager_titulo ct on ct.i_cdtitulo = ti.i_cdtitulo ' +
      ' where ti.f_validado = ''S'' ' +
      ' and ti.f_status = ''Q'' ' +
      ' and date(ti.d_quitacao) >= date(''' + dataini + ''') ' +
      ' and date(ti.d_quitacao) <= date(''' + datafin + ''') ' +
      ' and ct.f_tipo = ''M'' ';

  if ComboBoxEmp.ItemIndex > 0 then
    sql := sql + ' and ti.i_cdempresa = :I_CdEmpresa ';

  sql := sql +
      ' group by ct.i_cdplanoconta,ti.i_cdempresa ' +
      ' order by i_cdempresa ' +
    ' ) as valores ' +
    ' inner join planoconta pc on pc.i_cdplanoconta = valores.i_cdplanoconta ' +
    ' join fecha_gerencial on fecha_gerencial.i_cdempresa = valores.i_cdempresa ' +
    ' join empresa on empresa.i_cdempresa = valores.i_cdempresa ' +
    ' where PC.F_ANALITICO = ''S'' AND PC.F_TPCONTA = ''D'' AND PC.F_TIPO = ''D'' ' +
    ' and to_date(''01-''||i_mes||''-''||i_ano, ''dd-mm-yyyy'') >= ''' + dataini + ''' ' +
    ' and to_date(''01-''||i_mes||''-''||i_ano, ''dd-mm-yyyy'') <= ''' + datafin + ''' ' +
    ' group by  empresa.c_nome, pc.C_CONTA, pc.C_TITULOCONTA, pc.f_analitico, valores.i_cdempresa, valor ';

  Result := sql;
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  cdsDiasVerificacao.Open;
end;

procedure TForm1.FechaRequisicao;
begin
  cdsRcOrcamento.Close;
  cdsRcOrcamento.CommandText :=
    ' select ' +
    '   i_cdorcamento ' +
    ' from ' +
    '   itempedvenda ' +
    ' join orcamento on orcamento.i_cdorcamento = itempedvenda.i_nrrequisicao ' +
    ' where ' +
    '   i_nrpedido = :I_NrPedido ' +
    ' and orcamento.f_status = ''R'' ';
  cdsRcOrcamento.Params.ParamByName('I_NrPedido').AsInteger := cdsPedidoi_nrpedido.AsInteger;
  cdsRcOrcamento.Open;

  cdsRcOrcamento.First;
  while not cdsRcOrcamento.Eof do begin
    cdsRcOpRequisicao.Close;
    cdsRcOpRequisicao.CommandText := ' update orcamento set f_status = ''F'' where i_cdorcamento = :I_CdOrcamento ';
    cdsRcOpRequisicao.Params.ParamByName('I_CdOrcamento').AsInteger := cdsRcOrcamento.FieldByName('i_cdorcamento').AsInteger;
    cdsRcOpRequisicao.Execute;

    cdsRcOrcamento.Next;
  end;

  cdsRcOpRequisicao.Close;
  cdsRcOpRequisicao.CommandText := ' update orcamento set i_nrpedidoFaturamento = :I_NrPedido where i_nrpedidovendarequisicao = :I_NrPedido ';
  cdsRcOpRequisicao.Params.ParamByName('I_NrPedido').AsInteger := cdsPedidoi_nrpedido.AsInteger;
  cdsRcOpRequisicao.Execute;
end;

procedure TForm1.AbreRequisicao;
begin
  cdsRcOrcamento.Close;
  cdsRcOrcamento.CommandText :=
    ' select ' +
    '   i_cdorcamento ' +
    ' from ' +
    '   itempedvenda ' +
    ' join orcamento on orcamento.i_cdorcamento = itempedvenda.i_nrrequisicao ' +
    ' where i_nrpedido = :I_NrPedido ';
  cdsRcOrcamento.Params.ParamByName('I_NrPedido').AsInteger := cdsPedidoi_nrpedido.AsInteger;
  cdsRcOrcamento.Open;

  cdsRcOrcamento.First;
  while not cdsRcOrcamento.Eof do begin
    cdsRcOpRequisicao.Close;
    cdsRcOpRequisicao.CommandText := ' update orcamento set f_status = ''R'' where i_cdorcamento = :I_CdOrcamento ';
    cdsRcOpRequisicao.Params.ParamByName('I_CdOrcamento').AsInteger := cdsRcOrcamento.FieldByName('i_cdorcamento').AsInteger;
    cdsRcOpRequisicao.Execute;

    cdsRcOrcamento.Next;
  end;

  cdsRcOpRequisicao.Close;
  cdsRcOpRequisicao.CommandText := ' update orcamento set i_nrpedidoFaturamento = null where i_nrpedidovendarequisicao = :I_NrPedido; ';
  cdsRcOpRequisicao.Params.ParamByName('I_NrPedido').AsInteger := cdsPedidoi_nrpedido.AsInteger;
  cdsRcOpRequisicao.Execute;
end;

function TForm1.Coalesce(vValue, vIfNull: Variant): Variant;
begin
  if vValue = Null then
    Result := vIfNull
  else
    Result := vValue;

end;

function TForm1.ComissaoBaseLucro: Double;
begin
  // Calcula comiss�o por margem de lucro
  if eConfiguracao.Lines.Values['MARGEMLUCRO'] = 'S' then begin
    cdsItemPed.Close;
    cdsItemPed.CommandText :=      ' select ' +      '   (sum(n_quantidade * n_prcvenda) - sum( n_quantidade * n_prcunitario)) as lucro ' +      ' from ' +      ' itempedvenda ' +      ' where ' +
      '   i_nrpedido = :i_nrpedido ' +
      ' and f_bonifica = ''N'' ';
    cdsItemPed.Params.ParamByName('i_nrpedido').AsInteger := StrToInt(ePedido.Text);
    cdsItemPed.Open;
    Result := (cdsItemPed.FieldByName('lucro').AsFloat * (vlrdesconto / 100)) / vTotalParcela;
  end;
end;

procedure TForm1.eEntradaExit(Sender: TObject);
begin
  try
    if StrToFloat(eEntrada.Text) < 0 then
      eEntrada.Text := '0,00';
  except
    eEntrada.Text := '0,00';
  end;
end;

procedure TForm1.btn5Click(Sender: TObject);
begin
  Conectar;

  if SQLConnection.Connected then begin
    BuscaVendaVarejo;
    SomaValores;
  end;
end;

procedure TForm1.BitBtn4Click(Sender: TObject);
begin
  if cdsVendaVarejo.IsEmpty then
    Exit;

  if cdsVendaVarejo.RecordCount > 0 then begin
    cdsVendaVarejo.DisableControls;

    try
      cdsVendaVarejo.First;
      while not cdsVendaVarejo.Eof do  begin
        if not (cdsVendaVarejo.State in [dsInsert,dsEdit]) then
          cdsVendaVarejo.Edit;

        if cdsVendaVarejomarcado.AsString = 'X' then
          cdsVendaVarejomarcado.AsString := ''
        else
          cdsVendaVarejomarcado.AsString := 'X';

        cdsVendaVarejo.Next;
      end;

    finally
      cdsVendaVarejo.EnableControls;
      cdsVendaVarejo.First;
      SomaValores;
    end;
  end;
end;
{
procedure TForm1.BitBtn5Click(Sender: TObject);
var
  vMarcado: Boolean;
begin
  if
    MessageBox(0,
      PChar('Aten��o ser� gerado cupom de todas as vendas marcadas, esta a��o n�o tem volta.' +#13+ ' Deseja continuar?'),
      'Confirma��o',
      MB_YESNO + MB_DEFBUTTON2 + MB_ICONQUESTION + MB_TASKMODAL
    ) = IDYES
  then begin
    if cdsVendaVarejo.IsEmpty then
      Exit;

    if cdsVendaVarejo.RecordCount > 0 then begin
      cdsVendaVarejo.DisableControls;

      try
        vMarcado := False;
        cdsVendaVarejo.First;

        while not cdsVendaVarejo.Eof do begin
          if cdsVendaVarejomarcado.AsString = 'X' then begin
            vTotOrcCupom := 0;
            GeraCupom;
            FinalizaCupom;
            vMarcado := True;
          end;
          cdsVendaVarejo.Next;
        end;

        if not vMarcado then
          MessageDlg('Marque Uma Venda!', mtError, [mbOK], 0);
      finally
        cdsVendaVarejo.EnableControls;
        cdsVendaVarejo.First;
        BuscaVendaVarejo;
        SomaValores;
      end;
    end;
  end;
end;
}
function TForm1.AjustString(s: string; Tamanho: Integer; Carac_subs: string; Alig_Right: Boolean): string;
begin
  Copy(s, 1, Tamanho);
  if Length(s) < Tamanho then
    while Length(s) < Tamanho do
      if Alig_Right then
        s := Carac_Subs + s
      else
        s := s + Carac_Subs;

  Result := s;
end;

procedure TForm1.AtualizarArquivo(DirServer, Arquivo: string);
var vPath: string;
  vIntDataServer, vIntDataLocal: Integer;
begin
  if Copy(DirServer, Length(DirServer), 1) <> '\' then
    DirServer := DirServer + '\';

  vPath := ExtractFilePath(Application.ExeName);
  vIntDataServer := FileAge(DirServer + Arquivo);
  vIntDataLocal := FileAge(vPath + Arquivo);
  //    Testando um Intervalo maior que 1 hora (2200)
  if abs(vIntDataLocal - vIntDataServer) > 2200 then
    if not CopyFile(PChar(DirServer + Arquivo), PChar(vPath + Arquivo), False) then
      Application.MessageBox(PChar('� necess�rio atualizar o arquivo ' + Arquivo + ' e eu n�o consegui atualizar automaticamente do servidor: ' + DirServer + '!'), 'Erro');

end;

procedure TForm1.AtualizaTituloProtesto(pTitulo: Integer);
begin
  cdsContaFin.requestClear;
  cdsContaFin.requestfindfield('i_cdcontafin', ftinteger, StrToIntDef(eConfiguracao.Lines.Values['contafin'], 0));
  if (eConfiguracao.Lines.Values['ProtestaBoleto'] = 'S') and (StrToIntDef(cdsContaFin.fieldbyname('c_codigodiasprotesto').asstring, 0) > 0) then
//  if StrToIntDef(eConfiguracao.Lines.Values['Dias_Protesto'], 0) > 0 then
  begin
    cdsAtualizaTitulo.RequestClear;
    cdsAtualizaTitulo.RequestFindField('i_cdtitulo', ftInteger, pTitulo);
    cdsAtualizaTitulo.AddValue('i_diasprotesto', StrToIntDef(cdsContaFin.fieldbyname('c_codigodiasprotesto').asstring, 0), ftInteger);
    cdsAtualizaTitulo.RequestSave;
  end;
end;

{
procedure TForm1.AtualizaOrcCupom(idOrcamento: Integer);
begin
  cdsCriaTitCanc.Params.Clear;

  cdsCriaTitCanc.Close;
  cdsCriaTitCanc.CommandText := 'update Orcamento set c_nrdocfiscal = ''0'', f_status = ''C'' where i_cdorcamento = :i_cdorcamento ';
  cdsCriaTitCanc.Params.ParamByName('i_cdorcamento').AsInteger := idOrcamento;
  cdsCriaTitCanc.Execute;
end;
}

procedure TForm1.AutoMontarDesmontarKit(numero_pedido: Integer; acao: string);
var
  iFuncAtual: Integer;
  iMontagKit: Integer;
  vQtdPedida: Boolean;
begin

  cdsCriaParametro.Close;
  cdsCriaParametro.CommandText := 'select retparametro(''FATURA_COM_QTD_PEDIDA'')';
  cdsCriaParametro.Open;

  vQtdPedida := (Coalesce(cdsCriaParametro.Fields[0].AsString, 'N') = 'S');
  iFuncAtual := StrToInt(eConfiguracao.Lines.Values['usuario']);

  qCItemPedido.Close;
  qCItemPedido.SQL.Clear;
  qCItemPedido.SQL.Add('select');
  qCItemPedido.SQL.Add('  produto.i_cdproduto,');

  if vQtdPedida then
    qCItemPedido.SQL.Add('itempedvenda.N_Quantidade as n_qtd,')
  else
    qCItemPedido.SQL.Add('itempedvenda.N_QtdeSeparada as n_qtd,');

  qCItemPedido.SQL.Add('  pedidovenda.i_cdlocal_estoque,');
  qCItemPedido.SQL.Add('  pedidovenda.i_cdempresa');
  qCItemPedido.SQL.Add('from');
  qCItemPedido.SQL.Add('  itempedvenda');
  qCItemPedido.SQL.Add('join pedidovenda on itempedvenda.i_nrpedido = pedidovenda.i_nrpedido');
  qCItemPedido.SQL.Add('join produto on produto.i_cdproduto = itempedvenda.i_cdproduto and produto.f_tpproduto = ''K'' ');
  qCItemPedido.SQL.Add('where');
  qCItemPedido.SQL.Add('  pedidovenda.i_nrpedido = ' + IntToStr(numero_pedido) + ' and');

  if vQtdPedida then
    qCItemPedido.SQL.Add('itempedvenda.N_Quantidade > 0')
  else
    qCItemPedido.SQL.Add('itempedvenda.N_QtdeSeparada > 0');
  qCItemPedido.Open;

  qCMD.Close;
  qCMD.SQL.Clear;
  qCMD.SQL.Add('insert into MontagemKit(');
  qCMD.SQL.Add('  i_cdfunc_execucao,');
  qCMD.SQL.Add('  i_cdfunc_solicitacao,');
  qCMD.SQL.Add('  i_cdproduto,');
  qCMD.SQL.Add('  n_quantidade,');
  qCMD.SQL.Add('  f_status,');
  qCMD.SQL.Add('  f_acao,');
  qCMD.SQL.Add('  d_solicitacao,');
  qCMD.SQL.Add('  i_cdempresa,');
  qCMD.SQL.Add('  i_cdlocal_estoque');
  qCMD.SQL.Add(') values (');
  qCMD.SQL.Add('  :i_cdfunc_execucao,');
  qCMD.SQL.Add('  :i_cdfunc_solicitacao,');
  qCMD.SQL.Add('  :i_cdproduto,');
  qCMD.SQL.Add('  :n_quantidade,');
  qCMD.SQL.Add('  :f_status,');
  qCMD.SQL.Add('  :f_acao,');
  qCMD.SQL.Add('  :d_solicitacao,');
  qCMD.SQL.Add('  :i_cdempresa,');
  qCMD.SQL.Add('  :i_cdlocal_estoque');
  qCMD.SQL.Add(')');

  while not(qCItemPedido.Eof) do begin
    qCMD.ParamByName('i_cdfunc_execucao').AsInteger := iFuncAtual;
    qCMD.ParamByName('i_cdfunc_solicitacao').AsInteger := iFuncAtual;
    qCMD.ParamByName('i_cdproduto').AsInteger := qCItemPedido.FieldByName('i_cdproduto').AsInteger;
    qCMD.ParamByName('n_quantidade').AsFloat := qCItemPedido.FieldByName('n_qtd').AsFloat;
    qCMD.ParamByName('f_status').AsString := 'B';
    qCMD.ParamByName('f_acao').AsString := acao;
    qCMD.ParamByName('d_solicitacao').AsDateTime := Now;
    qCMD.ParamByName('i_cdempresa').AsInteger := qCItemPedido.FieldByName('i_cdempresa').AsInteger;
    qCMD.ParamByName('i_cdlocal_estoque').AsInteger := qCItemPedido.FieldByName('i_cdlocal_estoque').AsInteger;
    qCMD.ExecSQL;

    qSQL.Close;
    qSQL.SQL.Clear;
    qSQL.SQL.Add('select max(i_cdmontagemkit) as i_cdmontagemkit from montagemkit');
    qSQL.Open;

    iMontagKit := qSQL.FieldByName('i_cdmontagemkit').AsInteger;

    // Montagem de Kit
    if acao = 'M' then begin
      // Entra a quantidade no Estoque do KIT
      cdsMovimentaEstoque.Close;
      cdsMovimentaEstoque.CommandText :=
        ' select movimentaestoque( ' +
        '   ''MontagemKit'', ' +
            IntToStr(iMontagKit) + ', ' +
        '   ''montagemkit'', ' +
            IntToStr(iMontagKit) + ', ' +
        '   ''Montagem de Kit Aut.'', ' +
            qCItemPedido.FieldByName('i_cdproduto').AsString + ', ' +
        '   ''E'', ' +
            StringReplace(qCItemPedido.FieldByName('n_qtd').AsString, ',', '.', [rfReplaceAll]) + ', ' +
            qCItemPedido.FieldByName('i_cdlocal_estoque').AsString + ', ' +
        '   0, ' +
        '   0' +
        ' ); ';
      cdsMovimentaEstoque.Open;
//      qMovimentaEstoque.Close;
//      qMovimentaEstoque.SQL.Clear;
//      qMovimentaEstoque.SQL.Add('select movimentaestoque(');
//      qMovimentaEstoque.SQL.Add('''MontagemKit'',');
//      qMovimentaEstoque.SQL.Add(IntToStr(iMontagKit) + ',');
//      qMovimentaEstoque.SQL.Add('''montagemkit'',');
//      qMovimentaEstoque.SQL.Add(IntToStr(iMontagKit) + ',');
//      qMovimentaEstoque.SQL.Add('''Montagem de Kit Aut.'',');
//      qMovimentaEstoque.SQL.Add(qCItemPedido.FieldByName('i_cdproduto').AsString + ',');
//      qMovimentaEstoque.SQL.Add('''E'','); // Entrada
//      qMovimentaEstoque.SQL.Add(vNValor + ',');
//      qMovimentaEstoque.SQL.Add(qCItemPedido.FieldByName('i_cdlocal_estoque').AsString + ',');
//      qMovimentaEstoque.SQL.Add('0,');
//      qMovimentaEstoque.SQL.Add('0');
//      qMovimentaEstoque.SQL.Add(');');
//      qMovimentaEstoque.Open;

      qRKit.Close;
      qRKit.SQL.Clear;
      qRKit.SQL.Add('select');
      qRKit.SQL.Add('  n_quantidade,');
      qRKit.SQL.Add('  i_cdprodutokit');
      qRKit.SQL.Add('from');
      qRKit.SQL.Add('  composicaokit');
      qRKit.SQL.Add('where');
      qRKit.SQL.Add('  i_cdproduto = ' + qCItemPedido.FieldByName('i_cdproduto').AsString);
      qRKit.Open;

      while not(qRKit.Eof) do begin
        // Sai quantidade no Estoque dos componentes do kit
        cdsMovimentaEstoque.Close;
        cdsMovimentaEstoque.CommandText :=
          ' select movimentaestoque( ' +
          '   ''MontagemKit'', ' +
              IntToStr(iMontagKit) + ', ' +
          '   ''montagemkit'', ' +
              IntToStr(iMontagKit) + ', ' +
          '   ''Montagem de Kit Aut.'', ' +
              qRKit.FieldByName('i_cdprodutokit').AsString + ', ' +
          '   ''S'', ' +
              StringReplace(FloatToStr(qCItemPedido.FieldByName('n_qtd').AsFloat * qRKit.FieldByName('n_quantidade').AsFloat), ',', '.', [rfReplaceAll]) + ', ' +
              qCItemPedido.FieldByName('i_cdlocal_estoque').AsString + ', ' +
          '   0, ' +
          '   0' +
          ' ); ';
        cdsMovimentaEstoque.Open;

//        vNValor := StringReplace(FloatToStr(qCItemPedido.FieldByName('n_qtd').AsFloat * qRKit.FieldByName('n_quantidade').AsFloat), ',', '.', [rfReplaceAll]);
//        qMovimentaEstoque.Close;
//        qMovimentaEstoque.SQL.Clear;
//        qMovimentaEstoque.SQL.Add('select movimentaestoque(');
//        qMovimentaEstoque.SQL.Add('''MontagemKit'',');
//        qMovimentaEstoque.SQL.Add(IntToStr(iMontagKit) + ',');
//        qMovimentaEstoque.SQL.Add('''montagemkit'',');
//        qMovimentaEstoque.SQL.Add(IntToStr(iMontagKit) + ',');
//        qMovimentaEstoque.SQL.Add('''Montagem de Kit Aut.'',');
//        qMovimentaEstoque.SQL.Add(qRKit.FieldByName('i_cdprodutokit').AsString + ',');
//        qMovimentaEstoque.SQL.Add('''S'','); // Saida
//        qMovimentaEstoque.SQL.Add(vNValor + ',');
//        qMovimentaEstoque.SQL.Add(qCItemPedido.FieldByName('i_cdlocal_estoque').AsString + ',');
//        qMovimentaEstoque.SQL.Add('0,');
//        qMovimentaEstoque.SQL.Add('0');
//        qMovimentaEstoque.SQL.Add(');');
//        qMovimentaEstoque.Open;

        qRKit.Next;
      end;
    end
    else begin
      // Desmontagem de kit

      // Sai a quantidade no Estoque do KIT
      cdsMovimentaEstoque.Close;
      cdsMovimentaEstoque.CommandText :=
        ' select movimentaestoque( ' +
        '   ''MontagemKit'', ' +
            IntToStr(iMontagKit) + ', ' +
        '   ''montagemkit'', ' +
            IntToStr(iMontagKit) + ', ' +
        '   ''Desmontagem Kit Aut.'', ' +
            qCItemPedido.FieldByName('i_cdproduto').AsString + ', ' +
        '   ''S'', ' +
            StringReplace(qCItemPedido.FieldByName('n_qtd').AsString, ',', '.', [rfReplaceAll]) + ', ' +
            qCItemPedido.FieldByName('i_cdlocal_estoque').AsString + ', ' +
        '   0, ' +
        '   0' +
        ' ); ';
      cdsMovimentaEstoque.Open;

//      vNValor := StringReplace(qCItemPedido.FieldByName('n_qtd').AsString, ',', '.', [rfReplaceAll]);
//      qMovimentaEstoque.Close;
//      qMovimentaEstoque.SQL.Clear;
//      qMovimentaEstoque.SQL.Add('select movimentaestoque(');
//      qMovimentaEstoque.SQL.Add('''MontagemKit'',');
//      qMovimentaEstoque.SQL.Add(IntToStr(iMontagKit) + ',');
//      qMovimentaEstoque.SQL.Add('''montagemkit'',');
//      qMovimentaEstoque.SQL.Add(IntToStr(iMontagKit) + ',');
//      qMovimentaEstoque.SQL.Add('''Desmontagem Kit Aut.'',');
//      qMovimentaEstoque.SQL.Add(qCItemPedido.FieldByName('i_cdproduto').AsString + ',');
//      qMovimentaEstoque.SQL.Add('''S'','); // Saida
//      qMovimentaEstoque.SQL.Add(vNValor + ',');
//      qMovimentaEstoque.SQL.Add(qCItemPedido.FieldByName('i_cdlocal_estoque').AsString + ',');
//      qMovimentaEstoque.SQL.Add('0,');
//      qMovimentaEstoque.SQL.Add('0');
//      qMovimentaEstoque.SQL.Add(');');
//      qMovimentaEstoque.Open;

      qRKit.Close;
      qRKit.SQL.Clear;
      qRKit.SQL.Add('select');
      qRKit.SQL.Add('  n_quantidade,');
      qRKit.SQL.Add('  i_cdprodutokit');
      qRKit.SQL.Add('from');
      qRKit.SQL.Add('  composicaokit');
      qRKit.SQL.Add('where');
      qRKit.SQL.Add('  i_cdproduto = ' + qCItemPedido.FieldByName('i_cdproduto').AsString);
      qRKit.Open;

      while not(qRKit.Eof) do begin
        // Entra quantidade no Estoque dos componentes do kit
//        vNValor := StringReplace(FloatToStr(qCItemPedido.FieldByName('n_qtd').AsFloat * qRKit.FieldByName('n_quantidade').AsFloat), ',', '.', [rfReplaceAll]);
//        qMovimentaEstoque.Close;
//        qMovimentaEstoque.SQL.Clear;
//        qMovimentaEstoque.SQL.Add('select movimentaestoque(');
//        qMovimentaEstoque.SQL.Add('''MontagemKit'',');
//        qMovimentaEstoque.SQL.Add(IntToStr(iMontagKit) + ',');
//        qMovimentaEstoque.SQL.Add('''montagemkit'',');
//        qMovimentaEstoque.SQL.Add(IntToStr(iMontagKit) + ',');
//        qMovimentaEstoque.SQL.Add('''Desmontagem Kit Aut.'',');
//        qMovimentaEstoque.SQL.Add(qRKit.FieldByName('i_cdprodutokit').AsString + ',');
//        qMovimentaEstoque.SQL.Add('''E'','); // Entrada
//        qMovimentaEstoque.SQL.Add(vNValor + ',');
//        qMovimentaEstoque.SQL.Add(qCItemPedido.FieldByName('i_cdlocal_estoque').AsString + ',');
//        qMovimentaEstoque.SQL.Add('0,');
//        qMovimentaEstoque.SQL.Add('0');
//        qMovimentaEstoque.SQL.Add(');');
//        qMovimentaEstoque.Open;
        cdsMovimentaEstoque.Close;
        cdsMovimentaEstoque.CommandText :=
          ' select movimentaestoque( ' +
          '   ''MontagemKit'', ' +
              IntToStr(iMontagKit) + ', ' +
          '   ''montagemkit'', ' +
              IntToStr(iMontagKit) + ', ' +
          '   ''Desmontagem Kit Aut.'', ' +
              qRKit.FieldByName('i_cdprodutokit').AsString + ', ' +
          '   ''E'', ' +
              StringReplace(FloatToStr(qCItemPedido.FieldByName('n_qtd').AsFloat * qRKit.FieldByName('n_quantidade').AsFloat), ',', '.', [rfReplaceAll]) + ', ' +
              qCItemPedido.FieldByName('i_cdlocal_estoque').AsString + ', ' +
          '   0, ' +
          '   0' +
          ' ); ';
        cdsMovimentaEstoque.Open;

        qRKit.Next;
      end;
    end;
    qCItemPedido.Next;
  end;
end;

procedure TForm1.BaixarCaixas(numero_pedido: Integer; acao: string);
begin
  // acao -> B = Baixar; R = Retornar
  qSQL.Close;
  qSQL.SQL.Clear;
  qSQL.SQL.Add('select');
  qSQL.SQL.Add('  sr.i_cditempedvenda');
  qSQL.SQL.Add('from');
  qSQL.SQL.Add('  itempedvenda ipv');
  qSQL.SQL.Add('inner join serie sr on ipv.i_cdproduto = sr.i_cdproduto');
  qSQL.SQL.Add('where');
  qSQL.SQL.Add('  ipv.i_nrpedido = :i_nrpedido and');
  qSQL.SQL.Add('  ipv.i_cditempedvenda = sr.i_cditempedvenda and');
  qSQL.SQL.Add('  sr.c_caixa = ''S''');
  qSQL.ParamByName('i_nrpedido').AsInteger := numero_pedido;
  qSQL.Open;

  while not(qSQL.Eof) do begin
    qCMD.Close;
    qCMD.SQL.Clear;
    qCMD.SQL.Add('update serie set');
    qCMD.SQL.Add('  f_status = :f_status');
    qCMD.SQL.Add('where i_cditempedvenda = :i_cditempedvenda');

    if acao = 'B' then
      qCMD.ParamByName('f_status').AsString := 'V'
    else if acao = 'R' then
      qCMD.ParamByName('f_status').AsString := 'R';

    qCMD.ParamByName('i_cditempedvenda').AsInteger := qSQL.FieldByName('i_cditempedvenda').AsInteger;
    qCMD.ExecSQL;

    qSQL.Next;
  end;
end;

procedure TForm1.BloquearAcaoUsuario(Bloquea: Boolean);
var
  xBlockInput: function(Block: BOOL): BOOL; stdcall;
begin
  //Vini desabilitar o bloqueo de controles temporariamente...
  EnableCTRLALTDEL(not (Bloquea));

  if FunctionDetect('USER32.DLL', 'BlockInput', @xBlockInput) then
    xBlockInput(Bloquea);
end;

procedure TForm1.BuscaVendaVarejo;
var
  vTotalOrc: Double;
begin
  vTotalOrc := 0;

  cdsVendaVarejo.Close;
  cdsVendaVarejo.CommandText :=
    ' select ' +
    '   cast('' '' as char) as marcado, ' +
    '   date(d_lancamento) as d_lancamento, ' +
    '   n_totliquido, ' +
    '   i_cdfatura, ' +
    '   i_cdorcamento, ' +
    '   i_cdparccliente ' +
    ' from ' +
    '   orcamento ' +
    ' where ' +
    '   date(d_lancamento) = :data and ' +
    '   f_status = ''F'' and ' +
    '   trim(coalesce(c_nrdocfiscal, '''')) = '''' ';
  cdsVendaVarejo.Params.ParamByName('data').AsDate := dtpLancamento.Date;
  cdsVendaVarejo.Open;

  cdsVendaVarejo.DisableControls;

  try
    cdsVendaVarejo.First;
    while not cdsVendaVarejo.Eof do begin
      vTotalOrc := vTotalOrc + cdsVendaVarejon_totliquido.AsFloat;
      cdsVendaVarejo.Next;
    end;

  finally
    eTotOrcamento.Text := FormatFloat('#,##0.00', vTotalOrc);

    cdsVendaVarejo.EnableControls;
    cdsVendaVarejo.First;
  end;
end;

{
procedure TForm1.CriaParametro;
begin
  cdsCriaParametro.Close;
  cdsCriaParametro.CommandText := 'select CriaParametro() ';
  cdsCriaParametro.Open;
end;
 }
 {
procedure TForm1.CriaTituloCancelado(vlrTit: Double; idparceiro: Integer;
  cDoc: string);
var
  vSql: string;
begin
  vSql :=
    ' insert into titulo ( ' +
    '   i_cdparceiro, ' +
    '   i_cdtipotitulo, ' +
    '   d_vencimento, ' +
    '   n_vrtitulo, ' +
    '   d_lancamento, ' +
    '   c_documento, ' +
    '   d_mov, ' +
    '   f_tpmov, ' +
    '   f_status ' +
    ' ) values ( ' +
    '   :i_cdparccliente, ' +
    '   1, ' +
    '   now(), ' +
    '   :VlrTit, ' +
    '   now(), ' +
    '   :cDoc, ' +
    '   now(), ' +
    '   ''C'', ' +
    '   ''C'' ' +
    ' ) ';

  cdsCriaTitCanc.Close;
  cdsCriaTitCanc.CommandText := vSql;
  cdsCriaTitCanc.Params.ParamByName('i_cdparccliente').AsInteger := idparceiro;
  cdsCriaTitCanc.Params.ParamByName('VlrTit').AsFloat := VlrTit;
  cdsCriaTitCanc.Params.ParamByName('cDoc').AsString := cDoc;
  cdsCriaTitCanc.Execute;
end;
 }
procedure TForm1.EnableCTRLALTDEL(YesNo: Boolean);
const
  sRegPolicies = '\Software\Microsoft\Windows\CurrentVersion\Policies';
begin
  with TRegistry.Create do
  try
    RootKey := HKEY_CURRENT_USER;
    if OpenKey(sRegPolicies + '\System\', True) then begin
      case YesNo of
        True: WriteInteger('DisableTaskMgr', 0);
        False: WriteInteger('DisableTaskMgr', 1);
      end;
    end;

    CloseKey;

    if OpenKey(sRegPolicies + '\Explorer\', True) then begin
      case YesNo of
        False: begin
          WriteInteger('NoChangeStartMenu', 1);
          WriteInteger('NoClose', 1);
          WriteInteger('NoLogOff', 1);
        end;

        True: begin
          WriteInteger('NoChangeStartMenu', 0);
          WriteInteger('NoClose', 0);
          WriteInteger('NoLogOff', 0);
        end;
      end;
    end;

    CloseKey;
  finally
    Free;
  end;
end;
{
procedure TForm1.FinalizaCupom;
var
  vAcrescimo: Double;
  vNrCupom: Integer;
  vsMens, vNr_Orca: string;
begin
  ErroTef := not ECF.NumeroCupom(Parametros);

  if ErroTef then begin
    while (ErroTef) and (MessageDlg('Problemas com a impressora. Tentar Novamente?', mtConfirmation, [mbYes, mbNo], 0) = mrYes) do begin
      BloquearAcaoUsuario(True);
      ErroTef := not ECF.NumeroCupom(Parametros);
      BloquearAcaoUsuario(False);
    end;
  end;

  if Trim(Parametros[0]) <> '' then
    vNrCupom := Parametros[0];

  try
    if not ErroTef then begin
      Parametros[0] := 'D';
      Parametros[1] := '$';
      Parametros[2] := 0;

      ErroTef := not Ecf.IniciaFechamentoCupom(Parametros);

      if (ErroTef) then begin
        BloquearAcaoUsuario(False);

        while (ErroTef) and (MessageDlg('Problemas com a impressora. Tentar Novamente?', mtConfirmation, [mbYes, mbNo], 0) = mrYes) do begin
          BloquearAcaoUsuario(True);
          ErroTef := not Ecf.IniciaFechamentoCupom(Parametros);
          BloquearAcaoUsuario(False);
        end;
      end;
    end;

    vsMens := 'Cliente = CONSUMIDOR';
    vNr_Orca := vNr_Orca + AjustString(vsMens, 48, ' ', False);

    vsMens := '------------------ Procon-GO -------------------';
    vNr_Orca := vNr_Orca + AjustString(vsMens, 48, ' ', False);
    vsMens := 'Rua 2, nº 24, Centro, Goiânia-GO - Fone: 151';
    vNr_Orca := vNr_Orca + AjustString(vsMens, 48, ' ', False);

    if not ErroTef then begin
      Parametros[0] := vNr_Orca;
      Ecf.TerminaFechamentoCupom(Parametros);
      ecf.NumeroCupom(Parametros);
      CriaParametro;
      CriaTituloCancelado(
        vTotOrcCupom,
        cdsVendaVarejo.FieldByName('i_cdparccliente').AsInteger,
        '01-' + Parametros[0]
      );

      AtualizaOrcCupom(cdsVendaVarejo.FieldByName('i_cdorcamento').AsInteger);
      LimpaFatura(cdsVendaVarejoi_cdfatura.AsInteger);
    end;

  except
    raise;
  end;
end;
}
function TForm1.FunctionDetect(LibName, FuncName: string;
  var LibPointer: Pointer): Boolean;
var
  LibHandle: THandle;
begin
  Result := False;
  LibPointer := nil;

  if LoadLibrary(PChar(LibName)) = 0 then
    Exit;

  LibHandle := GetModuleHandle(PChar(LibName));

  if LibHandle <> 0 then begin
    LibPointer := GetProcAddress(LibHandle, PChar(FuncName));

    if LibPointer <> nil then
      Result := True;
  end;
end;
{
procedure TForm1.GeraCupom;
var
  Total: Double;
  Truncado: Double;
  PrecoVenda: Double;
  vN_AliqIcms: Double;

  vsMens: string;
  vNr_Orca: string;

  vNrCupom: Integer;
begin
  if cdsVendaVarejo.RecordCount = 0 then
    Abort;

  vb_CupomAberto := Ecf.CupomFiscalAberto;

  cdsItemOrcamento.Close;
  cdsItemOrcamento.CommandText :=
    ' select ' +
    '   item.i_cdsittrib, ' +
    '   item.N_aliqicms, ' +
    '   Produto.c_codigo, ' +
    '   Produto.c_descricao, ' +
    '   item.n_precovenda, ' +
    '   item.n_quantidade, ' +
    '   item.i_cdproduto, ' +
    '   Produto.c_cdbarras, ' +
    '   item.c_codbarras ' +
    ' from ' +
    '   Item_Orcamento item ' +
    ' join produto using(i_cdproduto) ' +
    ' where ' +
    '   item.i_cdorcamento = :I_CdOrcamento and ' +
    ' produto.f_servico = ''N'' ' +
    ' order by ' +
    '   item.i_ctitem ';
  cdsItemOrcamento.Params.ParamByName('I_CdOrcamento').AsInteger := cdsVendaVarejo.FieldByName('I_CdOrcamento').AsInteger;
  cdsItemOrcamento.Open;

  cdsItemOrcamento.First;
  while not cdsItemOrcamento.Eof do begin
    if cdsItemOrcamento.FieldByName('n_quantidade').AsFloat > 9999 then begin
      ShowMessage('N�o � poss�vel imprimir Cupom Fiscal com quantidade maior que 9.999,99.');
      Exit;
    end;

    cdsItemOrcamento.Next;
  end;

  if not Ecf.VerificaEstadoImpressora(Param) then
    raise Exception.Create('Problema de comunicac�o com a impressora fiscal!');

  if (vb_CupomAberto) Or (cdsItemOrcamento.RecordCount = 0) then begin
    ECF.NumeroCupom(Param);

    if (vb_CupomAberto) and (trim(Param[0]) = cdsVendaVarejo.FieldByName('C_NRDOCFISCAL').AsString) then
      Exit;

    Ecf.CancelaCupom(Param);
    vb_CupomAberto := False;
  end;

  cdsItemOrcamento.First;
  while not cdsItemOrcamento.Eof do begin
    Total := RoundTo(cdsItemOrcamento.FieldByName('n_quantidade').AsFloat * cdsItemOrcamento.FieldByName('n_precovenda').AsFloat, -3);
    Truncado := RoundTo(cdsItemOrcamento.FieldByName('n_quantidade').AsFloat * cdsItemOrcamento.FieldByName('n_precovenda').AsFloat, -3);
    vN_AliqIcms :=  RoundTo(cdsItemOrcamento.FieldByName('N_aliqicms').AsFloat, -2);

    If Truncado < Total then
      PrecoVenda := RoundTo(Trunc(cdsItemOrcamento.FieldByName('n_precovenda').AsFloat * 1000) / 1000, -3)
    else
      PrecoVenda := cdsItemOrcamento.FieldByName('n_precovenda').AsFloat;

    PrecoVenda := PrecoVenda * (1 - ( StrToFloatDef(eDescVarejo.Text,0)/100));

    vTotOrcCupom := vTotOrcCupom + RoundTo(cdsItemOrcamento.FieldByName('n_quantidade').AsFloat * PrecoVenda,-2);

    Param[0] := cdsItemOrcamento.FieldByName('c_codigo').AsString;
    Param[1] := cdsItemOrcamento.FieldByName('c_descricao').AsString;

    if (cdsItemOrcamento.FieldByName('i_cdsittrib').AsInteger = 60) then
      Param[2] := 'FF'
    else begin
      if (cdsItemOrcamento.FieldByName('i_cdsittrib').AsInteger = 40) then
        Param[2] := 'II'
      else if vN_AliqIcms = 0 then
        Param[2] := 'FF'
      else
        Param[2] := FloatToStr(vN_AliqIcms);
    end;

    Param[3] := 'F'; //Todos fracionária
    Param[4] := cdsItemOrcamento.FieldByName('n_quantidade').AsFloat;
    Param[5] := '3';
    Param[6] := PrecoVenda;
    Param[7] := '$';
    Param[8] := 0;

    ECF.VendeItem(Param);
    cdsItemOrcamento.Next;
  end;
end;
}
{
procedure TForm1.LimpaFatura(idFatura: Integer);
var
  vSql: string;
begin
  vSql := 'update titulo set i_cdfatura = null where i_cdfatura = :i_cdfatura';

  cdsCriaTitCanc.Params.Clear;
  cdsCriaTitCanc.Close;
  cdsCriaTitCanc.CommandText := vSql;
  cdsCriaTitCanc.Params.ParamByName('i_cdfatura').AsInteger := idFatura;
  cdsCriaTitCanc.Execute;
end;
 }
procedure TForm1.SomaValores;
var
  bt: TBookmark;
  vTotalOrc, vTotalMarc: Double;
begin
  vTotalOrc := 0;
  vTotalMarc := 0;

  cdsVendaVarejo.DisableControls;
  bt := cdsVendaVarejo.GetBookmark;

  try
    cdsVendaVarejo.First;
    while not cdsVendaVarejo.Eof do begin
      if cdsVendaVarejomarcado.AsString = 'X' then
        vTotalMarc := vTotalMarc + cdsVendaVarejon_totliquido.AsFloat;
      vTotalOrc := vTotalOrc + cdsVendaVarejon_totliquido.AsFloat;
      cdsVendaVarejo.Next;
    end;
    cdsVendaVarejo.GotoBookmark(bt);
  finally
    eTotOrcamento.Text := FormatFloat('#,##0.00', vTotalOrc);
    eTotTitMarc.Text := FormatFloat('#,##0.00', vTotalMarc);
    cdsVendaVarejo.EnableControls;
    cdsVendaVarejo.FreeBookmark(bt);
  end;
end;

procedure TForm1.dbg2DrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
  R: TRect;
  Check: Integer;
begin
  if cdsVendaVarejomarcado.AsString = 'X' then begin
    dbg2.Canvas.Brush.Color := clMoneyGreen;
    dbg2.Canvas.FillRect(Rect);
    dbg2.DefaultDrawColumnCell(Rect, DataCol, Column, State);
  end;

  if Column.FieldName = 'marcado' then begin
    dbg2.Canvas.FillRect(Rect);
    Check := 0;

    if cdsVendaVarejomarcado.AsString = 'X' then
      Check := DFCS_CHECKED
    else
      Check := 0;

    R := Rect;
    InflateRect(R, -1, -1); // Diminiu o tamanho do CheckBox no Grid

    DrawFrameControl(dbg2.Canvas.Handle, R, DFC_BUTTON, DFCS_BUTTONCHECK or Check);
  end;
end;

procedure TForm1.dbg2CellClick(Column: TColumn);
begin
  if not (cdsVendaVarejo.State in [dsInsert, dsEdit]) then
    cdsVendaVarejo.Edit;

  with cdsVendaVarejo do begin
    if FieldByName('marcado').AsString = 'X' then
      FieldByName('marcado').AsString := ' '
    else
      FieldByName('marcado').AsString := 'X';
  end;

  vAtualiza := True;
  SomaValores;
end;

procedure TForm1.SpeedButton3Click(Sender: TObject);
begin
//  if ecf.VerificaEstadoImpressora(parametros) then
//    MessageDlg('Comunica��o com a impressora com sucesso!',mtInformation,[mbOK],0)
end;

procedure TForm1.FormShow(Sender: TObject);
begin
  if eConfiguracao.Lines.Values['Desconto'] <> '' then
  begin
    eDesconto.Text := eConfiguracao.Lines.Values['Desconto'];
    if eDesconto.Text = '100' then
    begin
      eDesconto.Visible := False;
      ldesconto.Visible := False;
      lperc.Visible := False;
      cbGeraEntrada.Visible := True;
      bDesconto.Caption := 'Com &Desconto';
    end
    else
    begin
      eDesconto.Visible := True;
      ldesconto.Visible := True;
      lperc.Visible := True;
      cbGeraEntrada.Visible := False;
      bDesconto.Caption := 'Sem &Desconto';
    end;

  end;
end;

end.

//0.43 acrescentando f_controla_estoque = 'S' nos update de estoque.




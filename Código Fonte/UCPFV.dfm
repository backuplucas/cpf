�
 TFORM1 0Y6  TPF0TForm1Form1Left�TopBWidthyHeightVActiveControl
eOrcamentoCaptionControle geralColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
KeyPreview	OldCreateOrderPositionpoDesktopCenterOnClose	FormCloseOnCreate
FormCreate	OnKeyDownFormKeyDownPixelsPerInch`
TextHeight TPanelPanel1Left Top WidthqHeight� AlignalTop	PopupMenu
PopupMenu1TabOrder  TLabelLabel1Left!TopWidth!HeightCaption   Código  TLabellpercLeft�TopWidthHeightCaption%Font.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabel	ldescontoLeft4TopWidth.HeightCaptionDesconto  TLabelLabel8LeftTop� WidthAHeightCaptionValor Gerado:  TLabelLabel10Left� Top� WidthTHeightCaptionOutras Despesas:  TSpeedButtonSpeedButton1Left� TopWidthHeightCaption...OnClickSpeedButton1Click  TLabelLabel4LeftTop@Width$HeightCaptionParcela  TBitBtnBitBtn1LeftTop� WidthyHeightCaption&Gera complementoModalResultTabOrderOnClickBitBtn1Click
Glyph.Data
�  �  BM�      v   (   $            h                       �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� 333333333333333333  333333333333�33333  334C33333338�33333  33B$3333333�8�3333  34""C33333833�3333  3B""$33333�338�333  4"*""C3338�8�3�333  2"��"C3338�3�333  :*3:"$3338�38�8�33  3�33�"C333�33�3�33  3333:"$3333338�8�3  33333�"C333333�3�3  33333:"$3333338�8�  333333�"C333333�3�  333333:"C3333338�  3333333�#3333333��  3333333:3333333383  333333333333333333  	NumGlyphs  TEdit
eOrcamentoLeftITopWidthyHeightTabOrder OnEntereOrcamentoEnter  TEdit	eDescontoLefthTopWidth9HeightTabStopFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderText50  TEditeValorLeftXTop� WidthqHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclBlueFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderText0,00  TBitBtnbSalvaConfgLeft�Top� Width{HeightCaption   &Salva configuraçãoTabOrderVisibleOnClickbSalvaConfgClickKindbkAll  TDBMemoDBMemo1LeftTop]WidthEHeight9	DataFieldc_observacao
DataSource
dOrcamentoTabOrder  TBitBtnBitBtn2Left� Top� WidthKHeightCaption
Limpa TelaTabOrderOnClickLimpa1Click  TBitBtnbretornapedLeft� Top� WidthiHeightCaptionRetorna VendaTabOrderOnClickbretornapedClick  TEditeOutrasLeft8Top� WidthqHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclBlueFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderText0,00  TEditP1LeftITop=WidthHeightTabOrderOnExitP1Exit  TEditP2LeftbTop=WidthHeightTabOrderOnExitP1Exit  TEditP3Left}Top=WidthHeightTabOrderOnExitP1Exit  TEditP4Left� Top=WidthHeightTabOrderOnExitP1Exit  TEditP5Left� Top=WidthHeightTabOrderOnExitP1Exit  TEditP6Left� Top=WidthHeightTabOrderOnExitP1Exit  TEditP7Left� Top=WidthHeightTabOrderOnExitP1Exit  TEditP8LeftTop=WidthHeightTabOrder	OnExitP1Exit  TEditP9LeftTop=WidthHeightTabOrder
OnExitP1Exit  TEditP10Left:Top=WidthHeightTabOrderOnExitP1Exit  TEditP11LeftUTop=WidthHeightTabOrderOnExitP1Exit  TEditP12LeftpTop=WidthHeightTabOrderOnExitP1Exit  TBitBtnBitBtn3Left�Top� Width{HeightCaption   &Salva configuraçãoTabOrderVisibleOnClickbSalvaConfgClickKindbkAll   TPanelPanel2Left Top� WidthqHeightPAlignalClientTabOrder TPageControlConfiguracaoLeftTopWidthoHeightN
ActivePage	TabSheet1AlignalClientTabOrder OnChangeConfiguracaoChange
OnChangingConfiguracaoChanging 	TTabSheet	TabSheet1CaptionTitulos TDBGridDBGrid1Left Top WidthgHeight2AlignalClient
DataSourcedtituloTabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style ColumnsExpanded	FieldName
i_cdtituloTitle.CaptionCodigoVisible	 Expanded	FieldNamec_documentoTitle.Caption	DocumentoWidth� Visible	 Expanded	FieldName	d_emissaoTitle.Caption   EmissãoWidthPVisible	 Expanded	FieldNamed_vencimentoTitle.Caption
VencimentoWidthPVisible	 Expanded	FieldName
n_vrtituloTitle.CaptionValorVisible	 Expanded	FieldNamef_statusTitle.CaptionStatusVisible	     	TTabSheet	TabSheet2Caption   Configuração
ImageIndex TMemoconfLeft Top WidthgHeight2AlignalClientLines.Strings  TabOrder      TClientDataSet
Corcamento
Aggregates CommandText<select * from orcamento where i_cdorcamento = :i_cdorcamentoPacketRecordsParamsDataType	ftIntegerNamei_cdorcamento	ParamTypeptInputValue   ProviderName
PorcamentoLeft�Top�  TDataSource
dOrcamentoDataSet
CorcamentoLeft�Top�  TSQLDataSet
SorcamentoCommandText<select * from orcamento where i_cdorcamento = :i_cdorcamentoMaxBlobSize ParamsDataType	ftIntegerNamei_cdorcamento	ParamTypeptInputValue   SQLConnectionSQLConnectionLeft8Top�  TDataSetProvider
PorcamentoDataSet
SorcamentoOptionspoAllowCommandText LeftTop�  TSQLConnectionSQLConnectionConnectionNamePGEConnection
DriverName
PostgreSQLGetDriverFuncgetSQLDriverPOSTGRESQLLibraryNamedbexppge.dllLoginPromptParams.Strings  	VendorLib	LIBPQ.DLLLeft8TopX  TSQLDataSetscriatituloMaxBlobSize Params SQLConnectionSQLConnectionLeft�Toph  TDataSetProviderdcriatituloDataSetscriatituloOptionspoAllowCommandText Left�Toph  TClientDataSet
criatitulo
Aggregates FetchOnDemandParams ProviderNamedcriatituloLeftpToph  
TPopupMenu
PopupMenu1LeftTopX 	TMenuItemLimpa1CaptionLimpaShortCutrOnClickLimpa1Click  	TMenuItemProcura1CaptionProcuraShortCutqOnClickProcura1Click  	TMenuItemGera1CaptionGeraShortCutyOnClickBitBtn1Click  	TMenuItem	Imprimir1CaptionImprimirHintImprimir ReciboShortCutP@OnClickBitBtn3Click  	TMenuItemSair1CaptionSairShortCutzOnClick
Sair1Click   TSQLDataSetsTituloCommandText�Select i_cdcliente, i_cdtitulo, c_documento, d_emissao,d_vencimento, n_vrtitulo, f_status from titulo where i_nrpedido is null and c_documento like :c_documento MaxBlobSize ParamsDataTypeftStringNamec_documento	ParamTypeptInput  SQLConnectionSQLConnectionLeft8Topa  TDataSetProviderpTituloDataSetsTituloOptionspoAllowCommandText LeftTop`  TClientDataSetcTitulo
Aggregates Params ProviderNamepTituloLeft�Topa  TDataSourcedtituloDataSetcTituloLeft�Topa  TClientDataSet	cvendedor
Aggregates Params ProviderName	dvendedorLeft�Top�  TDataSetProvider	dvendedorDataSet	svendedorOptionspoAllowCommandText LeftTop�  TSQLDataSet	svendedorMaxBlobSize Params SQLConnectionSQLConnectionLeft8Top�  TSQLDataSetscriaparametroMaxBlobSize Params SQLConnectionSQLConnectionLeft� TopQ  TSQLDataSetsLoteCommandTextLSelect N_ValorBoleto From ContaFinanceira	Where I_CdContaFin = :I_CdContaFinMaxBlobSize ParamsDataType	ftIntegerNameI_CdContaFin	ParamTypeptInputOutput  SQLConnectionSQLConnectionLeft�Top�  TDataSetProviderdLoteDataSetsLoteOptionspoAllowCommandText Left�Top�  TClientDataSetcLote
Aggregates PacketRecordsParams ProviderNamedLoteLeftpTop�  TSQLDataSetsLoteRecCommandTextLSelect N_ValorBoleto From ContaFinanceira	Where I_CdContaFin = :I_CdContaFinMaxBlobSize ParamsDataType	ftIntegerNameI_CdContaFin	ParamTypeptInputOutput  SQLConnectionSQLConnectionLeft�Top�  TDataSetProviderdLoteRecDataSetsLoteRecOptionspoAllowCommandText Left�Top�  TClientDataSetcLoteRec
Aggregates CommandText�INSERT INTO loterecebimento(d_lancamento, i_cdvendedor, c_documento,i_cdparceiro, i_cdcaixa, i_cdbanco)   VALUES (:d_lancamento,:i_cdvendedor, :c_documento, :i_cdparceiro,:i_cdcaixa,:i_cdbanco)ParamsDataType
ftDateTimeNamed_lancamento	ParamTypeptInputValue         DataType	ftIntegerNamei_cdvendedor	ParamTypeptInputValue  DataTypeftStringNamec_documento	ParamTypeptInputValue  DataType	ftIntegerNamei_cdparceiro	ParamTypeptInputValue  DataType	ftUnknownName	i_cdcaixa	ParamType	ptUnknown DataType	ftUnknownName	i_cdbanco	ParamType	ptUnknown  ProviderNamedLoteRecLeftqTop�  TSQLDataSet	sfunclancMaxBlobSize Params SQLConnectionSQLConnectionLeft8Top`  TDataSetProvider	dfunclancDataSet	sfunclancOptionspoAllowCommandText LeftTop`  TClientDataSet	cfunclanc
Aggregates FetchOnDemandParams ProviderName	dfunclancLeft� Top`  TSQLDataSetsitemMaxBlobSize Params SQLConnectionSQLConnectionLeft8Top�  TDataSetProviderditemDataSetsitemOptionspoAllowCommandText LeftTop�  TClientDataSetcitem
Aggregates FetchOnDemandParams ProviderName	dfunclancLeft� Top�  TSQLDataSetsupdateitemMaxBlobSize Params SQLConnectionSQLConnectionLeft8Top�  TDataSetProviderdupdateitemDataSetsupdateitemOptionspoAllowCommandText LeftTop�  TClientDataSetcupdateitem
Aggregates FetchOnDemandParams ProviderNamedupdateitemLeft� Top�  TSQLDataSet
scalpedidoMaxBlobSize Params SQLConnectionSQLConnectionLeft8Top�  TDataSetProviderdcalcpedidoDataSet
scalpedidoOptionspoAllowCommandText LeftTop�  TClientDataSet
ccalpedido
Aggregates FetchOnDemandParams ProviderNamedcalcpedidoLeft� Top�  TSQLDataSet	slocalestMaxBlobSize Params SQLConnectionSQLConnectionLeft� Topp  TDataSetProviderdlocestDataSet	slocalestOptionspoAllowCommandText Left� Topp  TClientDataSet	clocalest
Aggregates FetchOnDemandParams ProviderNamedlocestLeft`Topp  TDataSetProviderdcriaparametroDataSetscriaparametroOptionspoAllowCommandText Left� TopP  TClientDataSetccriaparametro
Aggregates FetchOnDemandParams ProviderNamedcriaparametroLeft`TopP  TSQLDataSetsorcamentovendaMaxBlobSize Params SQLConnectionSQLConnectionLeft� Top�  TDataSetProviderdorcamentovendaDataSetsorcamentovendaOptionspoAllowCommandText Left� Top�  TClientDataSetcorcamentovenda
Aggregates FetchOnDemandParams ProviderNamedorcamentovendaLeft`Top�  TSQLDataSetsInsertDescCommandTextLinsert into desc_com (i_cddesc_com, n_valor) values (:i_cddesc_com,:n_valor)MaxBlobSize�ParamsDataType	ftUnknownNamei_cddesc_com	ParamTypeptInput DataType	ftUnknownNamen_valor	ParamTypeptInput  SQLConnectionSQLConnectionLeft� Top�  TClientDataSetcInsertDesc
Aggregates CommandTextOinsert into desc_comv (i_cddesc_comv, n_valor) values (:i_cddesc_comv,:n_valor)ParamsDataType	ftUnknownNamei_cddesc_comv	ParamType	ptUnknown DataTypeftFloatNamen_valor	ParamTypeptInputValue            ProviderNamepInsertDescLeft`Top�  TDataSetProviderpInsertDescDataSetsInsertDescOptionspoAllowCommandText Left� Top�  TClientDataSetcDeleta
Aggregates CommandText:delete from desc_comv where i_cddesc_comv = :i_cddesc_comvParamsDataType	ftIntegerNamei_cddesc_comv	ParamTypeptInputValue   ProviderNamepDeletaLeft`Top�  TDataSetProviderpDeletaDataSetsDeletaOptionspoAllowCommandText Left� Top�  TSQLDataSetsDeletaCommandTextLinsert into desc_com (i_cddesc_com, n_valor) values (:i_cddesc_com,:n_valor)MaxBlobSize�ParamsDataType	ftUnknownNamei_cddesc_com	ParamTypeptInput DataType	ftUnknownNamen_valor	ParamTypeptInput  SQLConnectionSQLConnectionLeft� Top�  TClientDataSetcParamTag
Aggregates CommandText5select * from tparametro where c_nome = upper(:param)ParamsDataTypeftStringNameparam	ParamTypeptInputValue   ProviderNamepParamLeft�Top�   TDataSetProviderpParamDataSetsParamOptionspoAllowCommandText Left�Top�   TSQLDataSetsParamMaxBlobSize�Params SQLConnectionSQLConnectionLeft Top�   TClientDataSet
cMovTitulo
Aggregates CommandTextS  insert into movtitulo (i_cdloterecebimento,f_status,d_movtitulo,i_cdfuncionario,i_cdtitulo,n_valpago,n_vrdesconto,i_cdcontafin,c_documento,c_obs,i_cdformapagto,f_fechado) values (:i_cdloterecebimento,:f_status,:d_movtitulo,:i_cdfuncionario,:i_cdtitulo,:n_valpago,:n_vrdesconto,:i_cdcontafin,:c_documento,:c_obs,:i_cdformapagto,:f_fechado) ParamsDataType	ftIntegerNamei_cdloterecebimento	ParamTypeptInputValue  DataTypeftStringNamef_status	ParamType	ptUnknownValue  DataTypeftDateNamed_movtitulo	ParamTypeptInputValue         DataType	ftIntegerNamei_cdfuncionario	ParamTypeptInputValue  DataType	ftIntegerName
i_cdtitulo	ParamTypeptInputValue  DataTypeftFloatName	n_valpago	ParamTypeptInputValue           DataTypeftFloatNamen_vrdesconto	ParamTypeptInputValue           DataType	ftUnknownNamei_cdcontafin	ParamType	ptUnknown DataTypeftStringNamec_documento	ParamType	ptUnknownValue  DataTypeftStringNamec_obs	ParamType	ptUnknownValue  DataType	ftIntegerNamei_cdformapagto	ParamTypeptInputValue  DataTypeftStringName	f_fechado	ParamType	ptUnknownValue   ProviderName
dMovTituloLeft�TopC  TDataSetProvider
dMovTituloDataSet
sMovTituloOptionspoAllowCommandText LeftTopC  TSQLDataSet
sMovTituloMaxBlobSize�Params SQLConnectionSQLConnectionLeft:TopC   